/**
 * Initializes webGL to canvas for 2D translation.
 */
function init() {
    let graphics = initCanvas("commonRasterCanvas");
    let letterF = new LetterFShape("LetterF" + createUID(112), 0, 0);
    graphics.addShape(letterF);

    setUIGadgets(graphics, letterF);
    graphics.update();
}


function setUIGadgets(state, obj) {
    let upd = new UpdateFunctions(obj, state);
    let shearXValueId = "shearXValue";
    let shearYValueId = "shearYValue";

    let shearXOpts = {
        uiPrecision: 1,
        min: -4,
        max: 4,
        step: 1,
        value: 0,
        functionCallback: upd.updatePosition(0)

    };

    let shearYOpts = {
        uiPrecision: 1,
        min: -4,
        max: 4,
        step: 1,
        value: 0,
        functionCallback: upd.updatePosition(1)
    };

    let sliderX = setupSlider(shearXValueId, shearXOpts);
    let sliderY = setupSlider(shearYValueId, shearYOpts);
    state.shearSx = sliderX.slider;
    state.shearSy = sliderY.slider;
}


function UpdateFunctions(obj, state) {
    function updatePosition(index) {
        return function (event, ui) {
            let shearX = state.shearSx.value;
            let shearY = state.shearSy.value;

            obj.setTransform($m3.identity().mul($m3.shear2(shearX, shearY)));

            // Set UI
            let fixedForUI = obj.transform.toFixed(3);
            let m = $m3.asArrayByRows(fixedForUI);
            setUIMatrix(m);
            state.update();
        }
    }


    return {
        updatePosition: updatePosition
    };
}


init();