/**
 * Creates "span" tag with text and color.
 *
 * @param text text to set innerHTML to
 * @param color color to style span with
 * @returns {Element} span element
 */
function createSpanWithColor(text, color) {

    let span = document.createElement("span");
    span.style.color = color;
    span.innerHTML = text;
    return span;
}


/**
 * Creates "span" tag and sets className attribute and its inner HTML text.
 *
 * @param className class name to set
 * @param text text to set
 */
function createOuterSpanNode(className, text) {
    let span = document.createElement("span");
    span.className = className;
    span.innerHTML = text;
    return span;
}


/**
 * Creates "span" tag and sets className attribute and its inner HTML text.
 *
 * @param className class name to set
 * @param text text to set
 * @returns {string}
 */
function createOuterSpan(className, text) {
    let span = document.createElement("span");
    span.className = className;
    span.innerHTML = text;
    return span.outerHTML;
}


/**
 * Helper function for creating needed "span" tag and adding text to it.
 *
 * @param token operator token to highlight
 * @returns {string}
 */
function highlightOperators(token) {
    if (token === "<=") {
        token = "&lt;=";
    }
    return createOuterSpan("operatorClass", token);
}


/**
 * Helper function for creating needed "span" tag and adding text to it.
 *
 * @param token digit token to highlight
 * @returns {string}
 */
function highlightDigit(token) {
    return createOuterSpan("digitClass", token);
}


/**
 * Helper function for creating needed "span" tag and adding text to it.
 *
 * @param token keyword token to highlight
 * @returns {string}
 */
function highlightKeyword(token) {
    return createOuterSpan("keywordClass", token);
}


/**
 * Helper function for creating needed "span" tag and adding text to it.
 *
 * @param token label token to highlight
 * @returns {string}
 */
function highlightLabels(token) {
    return createOuterSpan("labelClass", token);
}


/**
 * Helper function for creating needed "span" tag and adding text to it.
 *
 * @param value constant token to highlight
 * @returns {string}
 */
function colorCodeConstant(value) {
    return highlightDigit(value);
}


/**
 * Color codes the given command.
 *
 * @param command command to color
 * @returns {string}
 */
function colorCodeCommand(command) {

    let pu = new ParserUtil();
    let coloredCommand = "";

    let tokens = command.split(" ");
    if (tokens.length === 0) {
        return colorCommand(command);
    }

    let first = true;
    tokens.forEach((token) => {
            if (first) {
                coloredCommand += colorCommand(token);
                first = false;
            } else {
                coloredCommand += (" " + colorCommand(token));
            }

        }
    );


    function colorCommand(token) {
        if (pu.isKeyword(token)) {
            return (highlightKeyword(token));
        } else if (pu.isDigit(token)) {
            return (highlightDigit(token));
        } else if (pu.isOperChar(token)) {
            return (highlightOperators(token))
        } else if (pu.isId(token)) {
            return (highlightLabels(token) );
        }
        else {
            return (token);
        }
    }


    return coloredCommand;
}