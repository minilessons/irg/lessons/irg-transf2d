/**
 * Environment for executing user commands.
 *
 * @param parent parent scope
 * @constructor
 */
function Environment(parent) {
    this.vars = Object.create(parent ? parent.vars : null);
    this.parent = parent;
}


/**
 * Environment prototype. Offers setting, looking up, extending and getting environment variables from environment.
 *
 * @type {{extend: Environment.extend, lookup: Environment.lookup, get: Environment.get, set: Environment.set, def: Environment.def}}
 */
Environment.prototype = {
    u: new ParserUtil(),
    extend: function () {
        return new Environment(this);
    },

    lookup: function (name) {
        let scope = this;
        while (scope) {
            if (Object.prototype.hasOwnProperty.call(scope.vars, name))
                return scope;
            scope = scope.parent;
        }
    },

    get: function (name) {
        if (name in this.vars) {
            return this.vars[name];
        }

        let error = "Undefined variable " + name;
        throw new Error(error);
    },

    set: function (name, value) {
        let scope = this.lookup(name);
        if (!scope && this.parent) {
            let error = "Undefined variable " + name;
            throw new Error(error);
        }

        let retVal = (scope || this).vars[name] = value;
        updateEnvironmentConsole(name, value);

        return retVal;
    },

    def: function (name, value) {
        return this.vars[name] = value;
    }
};


/**
 * Evaluator for abstract syntax tree created by parser.
 *
 * @param exp expression to evaluate
 * @param userEnv environment to get variables from
 * @returns {*}
 */
function evaluate(exp, userEnv) {
    let u = new ParserUtil();
    switch (exp.type) {
        case "num":
            let precision = getConstants().getNumeric("mathPrecision");
            return Number((exp.value.toFixed(precision)));
        case "str":
        case "bool":
        case "pointType":
        case "hexColor":
            return exp.value;

        case "clearScreen":
            return clearConsole();

        case "var":
            // printToConsole(exp.value);
            return userEnv.get(exp.value);

        case "assign":
            if (exp.left.type !== "var") {
                let error = "Cannot assign to " + JSON.stringify(exp.left);
                printToConsole(error);
                throw new Error(error);
            }

            let setTo = evaluate(exp.right, userEnv);
            if (exp.operator === "<=") {
                // Check if it is assigned to our active matrix!
                if (exp.left.value !== "matrix") {
                    let err = "Assigning with '&lt;=' is supported only if left side is predefined variable \"matrix\"";
                    throw new Error(err);
                }

                // Push active matrix to stack
                pushToStack(userEnv, userEnv.get("matrix"));
            }

            let variableName = exp.left.value;
            let forbiddenNames = ["traceStack", "tracePoints", "pointPrecision", "matrixStack"];
            if (forbiddenNames.includes(variableName)) {
                setErrorToConsole("Cannot assign to predefined variable: " + colorCodeConstant(variableName) + ".");
                throw new Error("Use another variable name for new variables.");
            }

            if (!(setTo instanceof $m3) && (variableName === "matrix")) {
                let err = "Right side must be matrix when assigning to 'matrix' variable.";
                throw new Error(err);

            } else if (exp.left.value === "matrix") {
                setTransformToF(setTo, userEnv);
            }

            return userEnv.set(exp.left.value, setTo);

        case "inv":
            return exp;

        case "matrixVar":
            let m = userEnv.get(exp.value);

            if (exp.inverse) {
                m = $m3.inv(m);
            }

            return m;

        case "matrix":
            let mat = [];

            exp.value.forEach((row, rowIdx) => {
                let r = [];
                row.forEach((e, colIdx) => {
                    let evaluated = (evaluate(e, userEnv));
                    if (!u.isNum(evaluated)) {
                        throw new Error("Expected number for matrix element [" + rowIdx + ", " + colIdx + "].");
                    }
                    r.push(evaluated);
                });
                mat.push(r);
            });


            let m3 = $m3(mat);
            if (exp.inverse) {
                return $m3.inv(m3);
            }

            createStringRepresentation(m3, true);
            return m3;

        case "print":
            return printToConsole(evaluate(exp.value, userEnv), userEnv);

        case "pop":
            return popFromStack(userEnv);


        case "clearStack":
            return clearStack(userEnv);

        case "printPoints":
            return printPointsToConsole(userEnv);

        case "points":
            sendMessageToConsole("Setting points visibility to: " + exp.value);
            getStateEasy().tracePoints = exp.value;
            return userEnv.set("tracePoints", exp.value);

        case "trace":
            sendMessageToConsole("Setting trace visibility to: " + exp.value);
            getStateEasy().traceStack = exp.value;
            return userEnv.set("traceStack", exp.value);

        case "unary":
            let operand = evaluate(exp.operand, userEnv);
            return applyUnary(exp.value, operand);

        case "binary":
            return applyOper(exp.operator,
                evaluate(exp.left, userEnv),
                evaluate(exp.right, userEnv));

        case "prog":
            let val = false;
            exp.prog.forEach(function (exp) {
                val = evaluate(exp, userEnv)
            });
            return val;

        case "stack":
            // Called stack function
            return function (idx) {
                let stack = userEnv.get("matrixStack");
                if (idx < 0 || idx >= stack.length) {
                    let err = "Stack index out of range for index: " + idx;
                    throw Error(err);
                }
                return stack[idx];
            };

        case "identity":
            return function () {
                return $m3.identity();
            };

        case "translate":
            return function (tx, ty) {
                return $m3.translation(tx, ty);
            };

        case "rotate":
            return function (angleInDegrees) {
                return $m3.rotation(degToRad(angleInDegrees));
            };

        case "scale":
            return function (sx, sy) {
                return $m3.scale(sx, sy);
            };

        case "shear":
            return function (angleAlpha, angleBeta) {
                return $m3.shear(angleAlpha, angleBeta);
            };

        case "cos":
            return function (angleInDegrees) {
                let precision = getConstants().getNumeric("mathPrecision");
                return Number((Math.cos(degToRad(angleInDegrees))).toFixed(precision));
            };

        case "sin":
            return function (angleInDegrees) {
                let precision = getConstants().getNumeric("mathPrecision");
                return Number((Math.sin(degToRad(angleInDegrees))).toFixed(precision));

            };

        case "tan":
            return function (angleInDegrees) {
                let precision = getConstants().getNumeric("mathPrecision");
                return Number((Math.tan(degToRad(angleInDegrees))).toFixed(precision));

            };

        case "precision":
            return function (n) {
                if (Number.isInteger(n)) {
                    if (n < 0) {
                        throw new Error("Argument for precision must be positive.");
                    }
                    if (n >= 20) {
                        throw new Error("Argument for precision must be less than 20");
                    }
                    userEnv.set("printPrecision", n);
                    sendMessageToConsole("Setting print precision to: " + n);


                    // update stack precision
                    updateAllVars(userEnv);

                    return getStateEasy().constants.getString("noRetValue");

                } else {
                    throw new Error("Argument for precision must be integer type.");
                }

            };

        case "printStack":
            return printStackToConsole(userEnv);

        case "removePoint":
            if (exp.removeAll === true) {
                return removePoint(null, userEnv, exp.removeAll);
            }

            let id = evaluate(exp.value, userEnv);
            return removePoint(id, userEnv, exp.removeAll);

        case "args":
            return exp.args.map(function (arg) {
                return evaluate(arg, userEnv)
            });

        case "newPoint":
            let evaluatedArgs = evaluate({type: "args", args: exp.args}, userEnv);
            return addNewPoint(evaluatedArgs, userEnv);

        case "pushToStack":
            let shouldBeMatrix = evaluate(exp.value, userEnv);
            if (u.isMatrix(shouldBeMatrix)) {
                pushToStack(userEnv, shouldBeMatrix);
            } else {
                throw new Error("Cannot push '" + shouldBeMatrix + "' to stack. Please push only matrices.");
            }

            setToConsole(getLogMessage("Pushed matrix to stack."));
            return;

        case "setRotateStep":
            let rotStep = evaluate(exp.value, userEnv);
            if (!u.isNum(rotStep)) {
                throw new Error("Rotate step must be a number.");
            } else {
                let range = new u.InRange(-360, 360);
                if (!range.checkRange(rotStep)) {
                    throw new Error("Please keep rotate step in range: " + range.toString());
                }
                userEnv.set("rotateStep", rotStep);
                getStateEasy().events.rotStep = rotStep;
                return setToConsole(getLogMessage("Rotate step set to: " + rotStep));
            }


        case "setScaleXStep":
            let scaleX = evaluate(exp.value, userEnv);
            if (!u.isNum(scaleX)) {
                throw new Error("Scale step must be a number.");
            } else {
                let range = new u.InRange(-5, 5);
                if (!range.checkRange(scaleX)) {
                    throw new Error("Please keep horizontal scale step in range: " + range.toString());
                }
                userEnv.set("scaleXStep", scaleX);
                getStateEasy().events.scaleStepX = scaleX;
                return setToConsole(getLogMessage("Horizontal scale step set to: " + scaleX));
            }

        case "setScaleYStep":
            let scaleY = evaluate(exp.value, userEnv);
            if (!u.isNum(scaleY)) {
                throw new Error("Scale step must be a number.");
            } else {
                let range = new u.InRange(-5, 5);
                if (!range.checkRange(scaleY)) {
                    throw new Error("Please keep vertical scale step in range: " + range.toString());
                }
                userEnv.set("scaleXStep", scaleY);
                getStateEasy().events.scaleStepY = scaleY;
                return setToConsole(getLogMessage("Vertical scale step set to: " + scaleY));
            }


        case "selectionList":
            let callList = exp.shapes;
            let state = getStateEasy();
            state.objects.clear();
            callList.forEach((call) => {
                let sh = evaluate(call, userEnv);
                let optArgs = call.optionalArgs.map((arg) => {
                    return evaluate(arg, userEnv);
                });

                applyOptionalArgs(sh, optArgs[0], optArgs[1]);


                state.objects.addDynamicShape(sh);
                state.drawSelectionList = true;
            });

            return;

        case "call":
            let funcName = exp.func.value;
            if (u.isFunc(funcName)) {
                exp.func.type = funcName;
                let func = evaluate(exp.func, userEnv);
                let args = evaluate({type: "args", args: exp.args}, userEnv);

                if (exp.checkArgsCount === true || exp.checkArgsCount === undefined) {
                    checkCallParams(args.length, func.length, funcName);
                }

                return func.apply(null, args);
            } else {
                throw new Error("Undefined function: '" + funcName + "'");
            }

        case "defaultOutlineColor":
            userEnv.set(exp.type, exp.value);
            setToConsole(getLogMessage("Successfully changed outline color to: " + exp.value));
            return;
        case "defaultFillColor":
            userEnv.set(exp.type, exp.value);
            setToConsole(getLogMessage("Successfully changed fill color to: " + exp.value));
            return;

        case "square":
            return createAndRenderSquare;

        case "point":
            return createAndRenderPoint;

        case "polyline":
            return createAndRenderPolyline;
        case "rectangle":
            return createAndRenderRectangle;
        case "letterF":
            return createAndRenderLetterF();

        case "polygon":
            return createAndRenderPolygon;

        case "line":
            return createAndRenderLine;

        default:
            let error = "I don't know how to evaluate " + exp.type;
            printToConsole(error);
            throw new Error(error);
    }
}


function getGLColorFromHex(c) {
    return convertHexToRGBA(c).asNormalizedArray();
}


function applyOptionalArgs(sh, strokeColor, fillColor) {
    if (sh.fillStyle !== undefined) {
        sh.fillStyle = getGLColorFromHex(fillColor || globalUserEnv.get("defaultFillColor"));
    }
    sh.strokeStyle = getGLColorFromHex(strokeColor || globalUserEnv.get("defaultOutlineColor"));
}


/**
 * Checks number of params before calling a function in evaluator.
 *
 * @param given given number of parameters
 * @param expected expected number of parameters
 * @param funcName name of function which is checked
 */
function checkCallParams(given, expected, funcName) {
    if (given !== expected) {
        let message = "Expected " + expected + " but found " + given + " parameters for function: '" + funcName + "'.";
        throw new Error(message);
    }
}


/**
 * Helper function to apply unary operator to operand.
 *
 * @param op operator to apply
 * @param a operand
 * @returns {*} operand after applying operator
 */
function applyUnary(op, a) {
    let u = new ParserUtil();
    let num = u.num;


    switch (op) {
        case "+":
            return op;
        case "-":
            if (u.isMatrix(a)) {
                return $m3.multiplyWithScalar(a, -1);
            }

            if (num(a)) {
                return -a;
            }
            break;
        case "!":
            return !a;
    }

    let msg = "Can't apply unary operator " + op + " on operand: " + a;
    throw new Error(msg);
}


/**
 * Helper function to apply matrix operations on matrices.
 *
 * @param op operator to apply
 * @param a first matrix
 * @param b second matrix
 * @returns {*}
 */
function applyMatrixOperator(op, a, b) {

    switch (op) {
        case "+":
            return a.add(b);
        case "-":
            return a.add(b);
        case "*":
            return a.mul(b);
        default:
            let err = ("Can't apply operator " + op + " on matrices.");
            setToConsole(err);
            throw new Error(err);
    }
}


/**
 * Helper function to apply binary operator.
 *
 * @param op operator
 * @param a first operand (left side)
 * @param b second operand (right side)
 * @returns {*} result after operator is being applied
 *
 * @throws Error: In case of unsupported operand or wrong operand types
 */
function applyOper(op, a, b) {
    let u = new ParserUtil();
    let num = u.num;
    let isNum = u.isNum;
    let isMatrix = u.isMatrix;


    function div(x) {
        if (num(x) === 0) {
            let error = "Cannot divide by zero. Canceling operation.";
            throw new Error(error);
        }
        return x;
    }


    if (isMatrix(a) && b.value === "inv") {
        // Apply inverse
        if (true === b.apply) {
            return $m3.inv(a);
        } else {
            return a;
        }

    }


    if (isMatrix(a) && isMatrix(b)) {
        // Both matrices
        return applyMatrixOperator(op, a, b);
    }

    if (isMatrix(a) && isNum(b) && op === "*") {
        return $m3.multiplyWithScalar(a, b);
    }

    if (isMatrix(b) && isNum(a) && op === "*") {
        return $m3.multiplyWithScalar(b, a);
    }


    switch (op) {
        case ".":
            if (isMatrix(a)) {
                return ($m3.inv(a));
            } else {
                let err = ("Inverse operator can be applied only on matrices.");
                throw new Error(err);
            }
            break;
        case "+":
            return num(a) + num(b);
        case "-":
            return num(a) - num(b);
        case "*":
            return num(a) * num(b);
        case "/":
            return num(a) / div(b);
        case "%":
            return num(a) % div(b);
        case "&&":
            return a !== false && b;
        case "||":
            return a !== false ? a : b;
        case "<":
            return num(a) < num(b);
        case ">":
            return num(a) > num(b);
        case "<=":
            return num(a) <= num(b);
        case ">=":
            return num(a) >= num(b);
        case "==":
            return a === b;
        case "!=":
            return a !== b;
    }

    let err = "Can't apply operator '" + op + "' on operands: " + a + " and " + b + ".";
    throw new Error(err);
}

