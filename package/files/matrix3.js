// Matrices
function $m3(data) {
    if (!(this instanceof $m3)) {
        return new $m3(data);
    }

    if (data instanceof $m3) {
        this.elements = [
            [].concat(data.elements[0]),
            [].concat(data.elements[1]),
            [].concat(data.elements[2]),
        ];
    } else if (Array.isArray(data)) {
        $m3.check(data, 3, 3);
        this.elements = data;
    } else {
        throw "Expected 3x3 array or $m3 object."
    }
}


$m3.prototype.getElements = function () {
    return this.elements;
};


$m3.copy = function (data) {
    let arr = null;
    if (data instanceof $m3) {
        arr = data.elements;
    } else if (Array.isArray(data)) {
        arr = data;
    } else {
        throw 'Expected 3x3 array or $m3 object.';
    }
    let res = [];
    let n = arr.length;
    for (let i = 0; i < n; i++) {
        res.push([].concat(arr[i]));
    }
    return res;
};

$m3.check = function (m, r, c) {
    if (m.length !== r) throw "Expected " + r + "-row matrix; got " + m.length + ".";
    for (let i = 0; i < r; i++) {
        if (m[i].length !== c) throw "Expected " + r + "x" + c + " matrix; found col with " + m[i].length + " elements.";
    }
};

$m3.elems = function (data) {
    if (Array.isArray(data)) {
        $m3.check(data, 3, 3);
        return data;
    }
    if (data instanceof $m3) {
        return data.elements;
    }
    throw "Expected 3x3 array or $m3 object.";
};


$m3.prototype.elems = function () {
    return this.elements;
};

$m3.add = function (m1, m2) {
    m1 = $m3.elems(m1);
    m2 = $m3.elems(m2);
    $m3.check(m1, 3, 3);
    $m3.check(m2, 3, 3);
    return [
        [m1[0][0] + m2[0][0], m1[0][1] + m2[0][1], m1[0][2] + m2[0][2]],
        [m1[1][0] + m2[1][0], m1[1][1] + m2[1][1], m1[1][2] + m2[1][2]],
        [m1[2][0] + m2[2][0], m1[2][1] + m2[2][1], m1[2][2] + m2[2][2]]
    ];
};

$m3.prototype.add = function (m) {
    return $m3($m3.add(this.elements, $m3.elems(m)));
};

$m3.sub = function (m1, m2) {
    m1 = $m3.elems(m1);
    m2 = $m3.elems(m2);
    $m3.check(m1, 3, 3);
    $m3.check(m2, 3, 3);
    return [
        [m1[0][0] - m2[0][0], m1[0][1] - m2[0][1], m1[0][2] - m2[0][2]],
        [m1[1][0] - m2[1][0], m1[1][1] - m2[1][1], m1[1][2] - m2[1][2]],
        [m1[2][0] - m2[2][0], m1[2][1] - m2[2][1], m1[2][2] - m2[2][2]]
    ];
};

$m3.prototype.sub = function (m) {
    return $m3($m3.sub(this.elements, $m3.elems(m)));
};

$m3.transpose = function (m) {
    m = $m3.elems(m);
    $m3.check(m, 3, 3);
    return [
        [m[0][0], m[1][0], m[2][0]],
        [m[0][1], m[1][1], m[2][1]],
        [m[0][2], m[1][2], m[2][2]]

    ];
};

$m3.prototype.transpose = function () {
    return $m3($m3.transpose(this.elements));
};

$m3.multiplyWithScalar = function (m, s) {
    m = $m3.elems(m);
    $m3.check(m, 3, 3);
    return [
        [m[0] * s, m[1] * s, m[2] * s],
        [m[3] * s, m[4] * s, m[5] * s],
        [m[6] * s, m[7] * s, m[8] * s]
    ];
};

$m3.prototype.multiplyWithScalar = function (s) {
    return $m3.multiplyWithScalar(this.elements, s);
};

$m3.mul = function (m1, m2) {
    m1 = $m3.elems(m1);
    m2 = $m3.elems(m2);
    $m3.check(m1, 3, 3);
    $m3.check(m2, 3, 3);
    let sum = 0;
    let res = [[0, 0, 0], [0, 0, 0], [0, 0, 0]];
    for (let r = 0; r < 3; r++) {
        for (let c = 0; c < 3; c++) {
            sum = 0;
            for (let k = 0; k < 3; k++) {
                sum += m1[r][k] * m2[k][c];
            }
            res[r][c] = sum;
        }
    }
    return res;
};

$m3.prototype.mul = function (m) {
    return $m3($m3.mul(this.elements, $m3.elems(m)));
};

/**
 * Saves elements from first row, then second...
 */
$m3.asArrayByRows = function (m) {
    m = $m3.elems(m);
    $m3.check(m, 3, 3);
    return [m[0][0], m[0][1], m[0][2], m[1][0], m[1][1], m[1][2], m[2][0], m[2][1], m[2][2]];
};

$m3.prototype.asArrayByRows = function () {
    return $m3.asArrayByRows(this.elements);
};

/**
 * Saves elements from first column then second... uniformMatrix4fv are expecting this way of matrix array.
 */
$m3.asArrayByColumns = function (m) {
    m = $m3.elems(m);
    $m3.check(m, 3, 3);
    return [m[0][0], m[1][0], m[2][0], m[0][1], m[1][1], m[2][1], m[0][2], m[1][2], m[2][2]];
};

$m3.prototype.asArrayByColumns = function () {
    return $m3.asArrayByColumns(this.elements);
};

$m3.prototype.toString = function () {
    return '[' + $vec(this.elements[0]) + ', ' + $vec(this.elements[1]) + ', ' + $vec(this.elements[2]) + ']';
};

$m3.prototype.float32array = function () {
    return new Float32Array(this.asArrayByColumns());
};

$m3.float32array = function (m) {
    return new Float32Array($m3.asArrayByColumns(m));
};

$m3.prototype.postMul = function (v) {
    return $vec($m3.postMul(this.elements, $vec.elems(v)));
};

/**
 * Multiplies vector * matrix. Vector must be 3 dimensional.
 * @param m matrix
 * @param v vector
 *
 */
$m3.postMul = function (m, v) {
    m = $m3.elems(m);
    v = $vec.elems(v);
    $m3.check(m, 3, 3);
    if (v.length === 3) {
        return [
            m[0][0] * v[0] + m[1][0] * v[1] + m[2][0] * v[2],
            m[0][1] * v[0] + m[1][1] * v[1] + m[2][1] * v[2],
            m[0][2] * v[0] + m[1][2] * v[1] + m[2][2] * v[2]
        ];
    } else {
        throw "Expected 3 or 4-dim vector.";
    }
};


$m3.inv = function (m) {
    function mkeye(n) {
        let res = [];
        for (let i = 0; i < n; i++) {
            let row = [];
            for (let j = 0; j < n; j++) row.push(i === j ? 1 : 0);
            res.push(row);
        }
        return res;
    }


    function findMaxi(m1, c) {
        let max = Math.abs(m1[c][c]), maxi = c;
        let n = m1.length;
        for (let k = c + 1; k < n; k++) {
            let maxcand = Math.abs(m1[k][c]);
            if (maxcand > max) {
                maxi = k;
                maxcand = max;
            }
        }
        return maxi;
    }


    function addRow(m1, r1, r2) {
        let n = m1.length;
        for (let k = 0; k < n; k++) {
            m1[r1][k] += m1[r2][k];
        }
    }


    function addScaledRow(m1, r1, r2, s) {
        let n = m1.length;
        for (let k = 0; k < n; k++) {
            m1[r1][k] += m1[r2][k] * s;
        }
    }


    function scaleRow(m1, r1, s) {
        let n = m1.length;
        for (let k = 0; k < n; k++) {
            m1[r1][k] *= s;
        }
    }


    m = $m3.copy($m3.elems(m));
    let d = m.length;
    let res = mkeye(d);
    for (let r = 0; r < d; r++) {
        let mx = findMaxi(m, r);
        if (mx !== r) {
            addRow(m, r, mx);
            addRow(res, r, mx);
        }
        let sc = m[r][r];
        scaleRow(m, r, 1.0 / sc);
        scaleRow(res, r, 1.0 / sc);
        for (let rr = 0; rr < d; rr++) {
            if (rr === r) continue;
            sc = m[rr][r];
            addScaledRow(m, rr, r, -sc);
            addScaledRow(res, rr, r, -sc);
        }
    }

    return $m3(res);
};


$m3.identity = function () {
    return $m3(
        [
            [1, 0, 0],
            [0, 1, 0],
            [0, 0, 1]
        ]
    );

};

$m3.scale = function (sx, sy) {
    return $m3(
        [
            [sx, 0, 0],
            [0, sy, 0],
            [0, 0, 1],
        ]
    );
};


$m3.translation = function (tx, ty) {
    return $m3(
        [
            [1, 0, 0],
            [0, 1, 0],
            [tx, ty, 1]
        ]
    );
};


$m3.rotation = function (angleInRadians) {
    let s = Math.sin(angleInRadians);
    let c = Math.cos(angleInRadians);
    return $m3(
        [
            [c, s, 0],
            [-s, c, 0],
            [0, 0, 1],
        ]
    );
};

$m3.shear2 = function (a, b) {
    return new $m3(
        [
            [1, b, 0],
            [a, 1, 0],
            [0, 0, 1]

        ]
    );
}

$m3.shear = function (alpha, beta) {
    let tga = Math.tan(degToRad(alpha));
    let tgb = Math.tan(degToRad(beta));
    return new $m3(
        [
            [1, tga, 0],
            [tgb, 1, 0],
            [0, 0, 1]

        ]
    );
};

/*$m3.prototype.apply = function(callback){
    this.data = this.asArrayByRows().map(callback);
};*/

$m3.prototype.toFixedString = function (prec) {
    return $m3.toFixedString(this.elements, prec);
};


$m3.toFixedString = function (data, precision) {
    let prec;
    if (precision === 0 || undefined) {
        prec = 0;
    } else {
        prec = precision;
    }

    let res = [];
    data.forEach((x) => {
        let row = [];
        x.forEach((elem) => {
            let fixedVal = (elem.toFixed(prec));
            row.push(fixedVal);
        });
        res.push(row);
    });

    return (res);
};

$m3.prototype.toFixed = function (prec) {
    return $m3.toFixed(this.elements, prec);
};

$m3.toFixed = function (data, precision) {
    let prec = precision || 6;
    let res = [];
    data.forEach((x) => {
        let row = [];
        x.forEach((elem) => {
            let fixedVal = (Number(elem.toFixed(prec)));
            row.push(fixedVal);
        });
        res.push(row);
    });

    return $m3(res);
};

