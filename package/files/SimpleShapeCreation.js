function CreatorLib() {

    function dispatchOnEventType(type, shape, state, m) {

        let switchType = state.events.optionPane.activeCreating;
        let types = state.constants.getString("shapeTypes").map((prefix) => {
            return (prefix + "Shape");
        });

        let c = 0;
        let actions = getShapesEventActions(state);
        if (type === "mousemove") {
            let tp = transformSinglePoint(m.x, m.y);
            switch (switchType) {
                case types[c]:
                    // Line creator
                    state.objects.doOnShape(actions.changeLastVertex, shape.id, [tp.x, tp.y]);
                    state.objects.refreshShapeData(shape);
                    break;
                case types[c + 1]:
                    // Square creator
                    shape.changeSide(tp.x, tp.y);
                    state.objects.refreshShapeData(shape);
                    break;
                case types[c + 2]:
                    // Rectangle creator
                    shape.changeSides(tp.x, tp.y);
                    state.objects.refreshShapeData(shape);
                    break;
                case types[c + 3]:
                    // LetterF creator
                    shape.moveOrigin(tp.x, tp.y);
                    state.objects.refreshShapeData(shape);
                    break;
                case types[c + 4]:
                    // Point creator
                    shape.moveOrigin(tp.x, tp.y);
                    state.objects.refreshShapeData(shape);
                    break;
                case types[c + 5]:
                    // Polyline Shape
                    state.objects.doOnShape(actions.changeLastVertex, shape.id, [tp.x, tp.y]);
                    state.objects.refreshShapeData(shape);
                    break;

                case types[c + 6]:
                    // Polygon Shape
                    state.objects.doOnShape(actions.changeLastVertex, shape.id, [tp.x, tp.y]);
                    state.objects.refreshShapeData(shape);
                    break;
                default:
                    throw new Error("Unsupported shape type for:", type + ".");
            }
        }

        if (type === "click") {
            switch (switchType) {
                case types[c]:
                case types[c + 1]:
                case types[c + 2]:
                case types[c + 3]:
                case types[c + 4]:
                    clearShapeCreation(state.events.optionPane);
                    state.objects.refreshShapeData(shape);
                    break;
                case types[c + 5]:
                    // Polyline creator
                    state.objects.doOnShape(actions.addVertex, shape.id, [m.x, m.y]);
                    state.objects.refreshShapeData(shape);
                    break;

                case types[c + 6]:
                    // Polygon creator
                    state.objects.doOnShape(actions.addVertex, shape.id, [m.x, m.y]);
                    state.objects.refreshShapeData(shape);
                    break;

                default:
                    throw new Error("Unsupported shape type for:", type + ".");

            }
        }

        if (type === "rightClick") {
            switch (switchType) {
                case types[c]:
                case types[c + 1]:
                case types[c + 2]:
                case types[c + 3]:
                case types[c + 4]:
                    state.objects.refreshShapeData(shape);
                    break;
                case types[c + 5]:
                    // Polyline creator
                    state.objects.doOnShape(actions.removeLastVertex, shape.id, []);
                    state.objects.refreshShapeData(shape);
                    break;
                case types[c + 6]:
                    // Polygon creator
                    state.objects.doOnShape(actions.removeLastVertex, shape.id, []);
                    state.objects.refreshShapeData(shape);

                    break;

                default:
                    alert("No good for:" + switchType + types);
            }
        }
    }


    function createLineShape(x, y, x1, y1) {
        let shape = createAndRenderLine(x, y, x1, y1);

        let s = getStateEasy();
        s.events.optionPane.activeCreating = "LineShape";
        s.events.optionPane.activeShape = shape;

        return shape;

    }


    function createSquareShape(x, y, side) {
        let shape = createAndRenderSquare(x, y, side);

        let s = getStateEasy();
        s.events.optionPane.activeCreating = "SquareShape";
        s.events.optionPane.activeShape = shape;

        return shape;
    }


    function createRectangleShape(x, y, w, h) {
        let shape = createAndRenderRectangle(x, y, w, h);

        let s = getStateEasy();
        s.events.optionPane.activeCreating = "RectangleShape";
        s.events.optionPane.activeShape = shape;

        return shape;
    }


    function createLetterFShape(x, y) {
        let shape = createAndRenderLetterFAtPos(x, y);

        let s = getStateEasy();
        s.events.optionPane.activeCreating = "LetterFShape";
        s.events.optionPane.activeShape = shape;

        return shape;
    }


    function createPointShape(x, y) {
        let shape = createAndRenderPoint(x, y);

        let s = getStateEasy();
        s.events.optionPane.activeCreating = "PointShape";
        s.events.optionPane.activeShape = shape;

        return shape;
    }


    function createPolylineShape(x, y, x1, y1) {
        let shape = createAndRenderPolyline(x, y, x1, y1);

        let s = getStateEasy();
        s.events.optionPane.activeCreating = "PolylineShape";
        s.events.optionPane.activeShape = shape;

        return shape;
    }


    function createPolygonShape(x, y, x1, y1) {
        let shape = createAndRenderPolygonAnyVertices(x, y, x1, y1);

        let s = getStateEasy();
        s.events.optionPane.activeCreating = "PolygonShape";
        s.events.optionPane.activeShape = shape;

        return shape;

    }


    function dispatchCreateDialog(e) {
        let shapeId = e.target.id.split("Shape")[0];
        let shapeNames = getConstants().getString("shapeTypes");

        let s = getStateEasy();
        let pane = s.events.optionPane;
        let tp = transformSinglePoint(pane.lastX, pane.lastY);
        let mx = Math.floor(tp.x);
        let my = Math.floor(tp.y);

        let funcCallbacks = {
            LineShape: {func: createLineShape, args: [mx, my, mx + 2, my + 2]},
            SquareShape: {func: createSquareShape, args: [mx, my, 1]},
            RectangleShape: {func: createRectangleShape, args: [mx, my, 1, 3]},
            LetterFShape: {func: createLetterFShape, args: [mx, my]},
            PointShape: {func: createPointShape, args: [mx, my]},
            PolylineShape: {func: createPolylineShape, args: [mx, my]},
            PolygonShape: {func: createPolygonShape, args: [mx, my, mx + 2, my + 2]},
        };


        let size = shapeNames.length;

        for (let c = 0; c < size; c++) {
            if (shapeId === shapeNames[c]) {
                let funcObj = funcCallbacks[shapeId + "Shape"];
                let func = funcObj.func;
                let args = funcObj.args;
                let createdShape = func.apply(null, args);

                // Add modifiable shape to objects list for rendering
                applyOptionalArgs(createdShape, globalUserEnv.get("defaultOutlineColor"), globalUserEnv.get("defaultFillColor"));
                s.objects.addDynamicShape(createdShape);
                s.update();
                togglePaneVisibility(document.getElementById("optionPane"), s);
                s.events.optionPane.paneOpen = false;
                break;
            }
        }
    }


    return {
        dispatchOnEventType: dispatchOnEventType,
        dispatchCreateDialog: dispatchCreateDialog,
        /* createLetterFShape: createLetterFShape,
         createRectangleShape: createRectangleShape,
         createPointShape: createPointShape,
         createPolylineShape: createPolylineShape,
         createPolygonShape: createPolygonShape,
         createLineShape: createLineShape,
         createSquareShape: createSquareShape*/
    }
}