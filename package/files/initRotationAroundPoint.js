function init() {
    let graphics = initCanvas("letterFRotationAroundPoint");
    graphics.fixedFPointX = 3;
    graphics.fixedFPointY = 2;
    let letterF = new LetterFShape("LetterF" + createUID(112), 3, 2);
    graphics.addShape(letterF);

    setUIGadgets(graphics, letterF);
    graphics.update();
}


function setUIGadgets(state, obj) {
    let upd = new UpdateFunctions(obj, state);
    let rotationId = "rotationValue4";
    let rotateOpts = {
        functionCallback: upd.updateAngle,
        min: -360,
        max: 360,
        /*name: "RotationSlider: " Inherits from parent HTML */
    };

    state.angleDegrees = setupSlider(rotationId, rotateOpts);
}


function UpdateFunctions(obj, state) {
    function updateAngle(event, ui) {
        let angleInDegrees = ui.value;

        let translationToOrigin = $m3.translation(-state.fixedFPointX, -state.fixedFPointY);
        let finalTransformation =
            $m3.identity()
                .mul(translationToOrigin)
                .mul($m3.rotation(degToRad(angleInDegrees)))
                .mul($m3.inv(translationToOrigin));
        state.userMatrix = finalTransformation;

        // Set UI
        let fixedForUI = finalTransformation.toFixed(3);
        let m = $m3.asArrayByRows(fixedForUI);
        setUIMatrix(m, "m3");
        setUIMatrix(m, "m4");
        state.update();
    }


    return {
        updateAngle: updateAngle
    };
}


init();