/**
 * Main function for creating 2D grid and bind whole state to webGl and canvas.
 *
 * @param canvas canvas to bind drawings to
 * @returns {RenderState} whole canvas state
 * @constructor
 */
function RenderState(canvas) {
    let gl = initWebGL(canvas);
    if (gl) {
    } else {
        return null;
    }

    this.constants = getConstants();
    this.step = this.constants.numeric.gridStep;
    this.rotStep = this.constants.numeric.rotStep;
    this.canvas = canvas;

    setCanvasProperties(canvas, this.constants);

    // Points for creating additional shapes
    this.listOfPoints = [];

    // List that holds all shapes that are to be drawn in that specific order.
    this.objects = new ObjectsToDraw();

    // Transform matrices sent from stack
    this.matrixStack = [];

    // Tracing on/off switches
    this.traceStack = true;
    this.tracePoints = true;

    // User matrix: global matrix which is set to represent shapes position transformations
    this.userMatrix = $m3.identity();

    // Keep reference to this state
    let myState = this;

    // Set clear color to black, fully opaque
    let backgroundColor = new RGBA(0.0, 0.0, 0.0, 1.0);
    gl.clearColor(backgroundColor.r, backgroundColor.g, backgroundColor.b, backgroundColor.a);

    // Clear the color as well as the depth buffer.
    gl.clear(gl.COLOR_BUFFER_BIT);


    let utils = new WebGLUtils();

    let grid = getCoordinateArray(gl, myState.step);
    let gridArrays = {
        position: {numComponents: 2, data: grid.values, normalize: false},
        vertexColor: {numComponents: 4, data: grid.colors, normalize: false}
    };

    // New structure
    let programInfoGrid = utils.createProgramInfo(gl, ["vertex-grid-shader", "fragment-grid-shader"]);
    let programInfoShapes = utils.createProgramInfo(gl, ["vertex-shape-shader", "fragment-shape-shader"]);
    let shapeProjection = getShapeProjection(gl.canvas.clientWidth, gl.canvas.clientHeight, myState.step);

    let bufferInfoGrid = utils.createBufferInfoFromArrays(gl, gridArrays);


    // Add grid for drawing
    this.objects.addGrid("GridObject",
        programInfoGrid,
        gridProjection(gl.canvas.clientWidth, gl.canvas.clientHeight),
        bufferInfoGrid
    );


    this.addShape = function (shape) {
        myState.objects.addDynamicShape(shape);
    };


    function constructTransparent(color, num, transparencyDecreaseFactor) {

        let colors = [];

        let varyingAlpha = 1.0;
        for (let c = 0; c < num; c++) {
            colors[c] = [];
            colors[c].push(color[0]);
            colors[c].push(color[1]);
            colors[c].push(color[2]);
            colors[c].push(varyingAlpha);

            varyingAlpha -= transparencyDecreaseFactor;
        }

        return colors;
    }


    function gridProjection(w, h) {
        return new $m3([
            [2 / w, 0, 0],
            [0, -2 / h, 0],
            [-1, 1, 1],
        ]);
    }


    function getShapeProjection(w, h, step) {
        let m = $m3.identity();
        m = m.mul($m3.scale((step * 2) / w, ( step * 2) / h));

        return m;
    }


    function ObjectsToDraw() {
        this.shapesToDraw = [];
        this.points = [];
        this.gridObj = null;

        this.clear = function () {
            this.shapesToDraw = [];
        };


        this.setTransformMatrixForShape = function (action, id) {
            this.shapesToDraw.forEach((sh) => {
                if (sh.id === id) {
                    if (sh.fillProp !== undefined) {
                        sh.fillProp.transform = action(sh);
                    }
                    sh.strokeProp.transform = action(sh);
                }
            });
        };

        this.doOnShape = function (predicate, id, args) {
            let size = this.shapesToDraw.length;
            for (let c = 0; c < size; c++) {
                let sh = this.shapesToDraw[c];
                if (sh.id === id) {
                    predicate.apply(null, [sh].concat(args));
                    break;
                }
            }

        };

        this.addPoint = function (sh) {
            this.points.push(this.getDrawableShape(sh, sh.fillData, sh.fillStyle, sh.primitiveFillType)
            );
        };

        this.addGrid = function (id, program, projection, bufferInfo) {
            this.gridObj = {
                id: id,
                programInfo: program,
                projection: projection,
                bufferInfo: bufferInfo,

                transform: $m3.identity(),

                primitiveType: gl.LINES
            };
        };

        this.refreshShapeData = function (sh) {
            if (sh.fillStyle !== undefined) {
                sh.fillProp = (this.getDrawableShape(sh, sh.fillData, sh.fillStyle, sh.primitiveFillType));
            }

            sh.strokeProp = (this.getDrawableShape(sh, sh.strokeData, sh.strokeStyle, sh.primitiveStrokeType));
        };

        this.addDynamicShape = function (sh) {
            this.refreshShapeData(sh);
            this.shapesToDraw.push(sh);
        };

        this.getDrawableShape = function (sh, data, color, primitiveType, transform) {
            let arrays = {
                position: {numComponents: 2, data: data, normalize: false},
            };

            if (sh.colors !== undefined) {
                arrays.vertexColor = {numComponents: 4, data: sh.colors, normalize: false}
            }

            let bufferInfo = utils.createBufferInfoFromArrays(gl, arrays);

            return {
                id: sh.id || "undefinedShapeID",
                programInfo: programInfoShapes,
                projection: shapeProjection,
                bufferInfo: bufferInfo,

                transform: transform || sh.transform,

                primitiveType: getGLTypesFromShapeType(primitiveType),
                uniforms: {
                    u_color: color
                }
            };
        };


        this.combineCustomTransformWithObj = function (obj, m) {
            return obj.transform.mul(m);
        };


        this.drawObj = function (obj, initialMatrix) {
            gl.useProgram(obj.programInfo.program);

            let finalMatrix = initialMatrix.mul(obj.projection);
            let finalMatrixUniform = {
                u_matrix: $m3.asArrayByColumns(finalMatrix)
            };

            utils.setBuffersAndAttributes(gl, obj.programInfo, obj.bufferInfo);
            utils.setUniforms(obj.programInfo.uniformSetters, finalMatrixUniform);

            // Set other uniforms: such as color and textures...
            if (obj.uniforms !== undefined) {
                utils.setUniforms(obj.programInfo.uniformSetters, obj.uniforms);
            }

            gl.drawArrays(obj.primitiveType, 0, obj.bufferInfo.numElements);


        };

        this.drawObjects = function () {
            let restoreColors = false;
            this.drawGrid();

            if (myState.tracePoints) {
                this.drawPoints();
            }

            if (myState.traceStack === true) {
                restoreColors = true;
                this.drawStack();
            }


            this.shapesToDraw.forEach((shape) => {
                myState.objects.refreshShapeData(shape);
                if (shape.fillProp !== undefined) {
                    if (restoreColors) shape.fillProp.uniforms.u_color = shape.fillStyle;
                    this.drawObj(shape.fillProp, this.combineCustomTransformWithObj(shape.fillProp, myState.userMatrix));
                }
                if (restoreColors) shape.strokeProp.uniforms.u_color = shape.strokeStyle;
                this.drawObj(shape.strokeProp, this.combineCustomTransformWithObj(shape.strokeProp, myState.userMatrix));
            });

        };
        this.drawPoints = function () {
            this.points.forEach((obj) => {
                this.drawObj(obj, obj.transform);
            });
        };

        this.drawGrid = function () {
            this.drawObj(this.gridObj, this.gridObj.transform);
        };

        this.drawStack = function () {
            let stackSize = myState.matrixStack.length;
            let stackFactor = 1 / (stackSize + stackSize);
            this.shapesToDraw.forEach((obj) => {
                let shapeFill = obj.fillProp;
                if (shapeFill !== undefined) {
                    let colors = constructTransparent(shapeFill.uniforms.u_color, stackSize, stackFactor);
                    myState.matrixStack.forEach((transform, stackIdx) => {
                        shapeFill.uniforms.u_color = colors[stackIdx];

                        this.drawObj(shapeFill, this.combineCustomTransformWithObj(shapeFill, transform));
                    });
                }

                let shapeStroke = obj.strokeProp;
                let colors = constructTransparent(shapeStroke.uniforms.u_color, stackSize, stackFactor);
                myState.matrixStack.forEach((transform, stackIdx) => {
                    shapeStroke.uniforms.u_color = colors[stackIdx];
                    this.drawObj(shapeStroke, this.combineCustomTransformWithObj(shapeStroke, transform));
                });
            })
        };
    }


    function drawScene() {
        gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
        gl.clearColor(backgroundColor.r, backgroundColor.g, backgroundColor.b, backgroundColor.a);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        myState.objects.drawObjects();
    }


    function getGLTypesFromShapeType(type) {
        switch (type) {
            case "TRIANGLES":
                return gl.TRIANGLES;
            case "TRIANGLE_FAN":
                return gl.TRIANGLE_FAN;
            case "TRIANGLE_STRIP":
                return gl.TRIANGLE_STRIP;
            case "LINE_STRIP":
                return gl.LINE_STRIP;
            case "LINE_LOOP":
                return gl.LINE_LOOP;
            case "LINES":
                return gl.LINES;
            default:
                throw new Error("Got not supported GL type:" + type);
                break;
        }
    }


    this.pointIdIndex = 1;
    this.getNextPointIndex = function () {
        let oldId = this.pointIdIndex;
        this.pointIdIndex += 1;
        return oldId;
    };


    // Animation and events
    addEvents(myState);
    addEventObject(myState);


    this.update = drawScene;
    return myState;
}


function addEvents(state) {
    let c = state.canvas;
    /* c.addEventListener("mouseup", function (e) {
         webGLMouseUpCallback(e, state);
     }, false);
     c.addEventListener("mousemove", function (e) {
         webGLMouseMoveCallback(e, state);
     }, false);
     c.addEventListener("mousedown", function (e) {
         webGLMouseDownCallback(e, state);
     }, false);

     c.addEventListener("keydown", function (e) {
         webGLKeyDownEvent(e, state);
     }, false);
     c.addEventListener("keypress", function (e) {
         webGLKeyPressEvent(e, state);
     }, false);
 */
    // Set to canvas later!
    c.addEventListener("contextmenu", webGLContextMenuCallback, false);
    c.addEventListener("selectstart", webGLSelectStartCallback, false);
    c.addEventListener("contextmenu", function (e) {
        webGLContextMenuCallback(e, state);
    }, false);

}


function addEventObject(state) {
    let actions = new SimpleShapeActions();
    let creations = new CreatorLib();
    state.events = {
        dragX: 0,
        dragY: 0,
        dragging: false,
        savedColors: {},
        selectedShape: null,
        selectedObjects: {},
        selected: false,

        rotStep: 45,
        scaleStepX: 0.1,
        scaleStepY: 0.1,
        optionPane: {
            lastX: 0,
            lastY: 0,
            activeCreating: null,
            activeShape: null,
            paneOpen: false,

            creation: creations
        },

        shapes: {
            actions: actions
        }

    }
}


/**
 * Initializes webGL at specified canvas.
 *
 * @param canvas canvas to set webGL to
 * @returns {CanvasRenderingContext2D|WebGLRenderingContext}
 */
function initWebGL(canvas) {
    let gl = canvas.getContext('webgl') || canvas.getContext('experimental-webgl');

    if (!gl) {
        alert("Cannot initialize WebGL. Your browser may not support it.");
    }

    return gl;
}