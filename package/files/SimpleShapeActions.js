function SimpleShapeActions() {
    function changeLastVertex(shape, x, y) {
        let length = shape.vertices.length;
        shape.vertices[length - 2] = x;
        shape.vertices[length - 1] = y;
        shape.refreshShapeData();
    }


    /**
     * Helper action function to add vertex to a shape.
     *
     * @param shape shape to add vertex to
     * @param x coordinate of a vertex
     * @param y coordinate of a vertex
     */
    function addVertex(shape, x, y) {
        let tp = transformSinglePoint(x, y);
        shape.vertices.push(tp.x);
        shape.vertices.push(tp.y);

        // Set vertex data and refresh render data
        shape.refreshShapeData();

    }


    /**
     * Helper function to remove last vertex from shape while in creation mode.
     *
     * @param sh shape to remove last vertex from
     */
    function removeLastVertex(sh) {
        sh.vertices = sh.vertices.slice(0, sh.vertices.length - 2);
        sh.refreshShapeData();
    }


    return {
        addVertex: addVertex,
        removeLastVertex: removeLastVertex,
        changeLastVertex: changeLastVertex
    };
}