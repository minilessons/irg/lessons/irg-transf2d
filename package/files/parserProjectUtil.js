/**
 * Renders given matrix to UI.
 *
 * @param matrix matrix to set UI to
 * @env environment in which state is defined
 */
function setTransformToF(matrix) {
    getStateEasy().userMatrix = matrix;

    // Set UI
    let fixedForUI = matrix.toFixed(3);
    let m = $m3.asArrayByRows(fixedForUI);

    let size = 3;
    for (let c = 0; c < size; c++) {
        for (let k = 0; k < size; k++) {
            let constructedString = "m" + c + k;
            let div = document.getElementById(constructedString);
            div.innerHTML = (m[k + c * size]);
        }
    }
}


/**
 * Adds blank "span class=selector" node to div element.
 *
 * @param elementId id of an element to add blank rows to
 * @param numOfRows number of rows to add
 */
function addBlankRows(elementId, numOfRows) {

    let element = document.getElementById(elementId);

    if (!element) {
        throw "No such element for ID::" + elementId;
    }

    for (let c = 0; c < numOfRows; c++) {
        let newDiv = createEmptyAreaElement();
        element.appendChild(newDiv);
    }
}


function createEmptyAreaElement() {
    let newDiv = document.createElement("div");
    newDiv.className = "selector";
    newDiv.innerHTML = "&nbsp;";

    return newDiv;
}


/**
 * Creates description and usage examples for this example.
 */
function createDescription() {

    let div = document.getElementById("keywordsDesc");
    let printColor = "#e96226";
    let keywordColor = "#2196F3";
    let sectionColor = "#30ff85";
    let eventColor = "#51bcff";
    let tabDivider = "&nbsp;&nbsp;&nbsp;&nbsp;";
    let usageColor = "#f0970e";
    let importantColor = "#f02a23";


    function getTextSpan(text) {
        return createSpanWithColor(text, "#FFFFFF");
    }


    function addNewline() {
        div.appendChild(document.createElement("br"));
    }


    function add(child) {
        div.appendChild(child);
        addNewline();
    }


    function addFuncSpan(func) {
        let fSpan = createSpanWithColor(tabDivider, printColor);
        fSpan.appendChild(func);
        add(fSpan);
    }


    function addSection(name, color, desc, descColor) {
        let SSpan = createSpanWithColor(name, color);
        let descSpan = createSpanWithColor(desc, descColor || headerColor);
        SSpan.innerHTML += " :: ";
        SSpan.appendChild(descSpan);
        add(SSpan);
    }


    function addEventSpan(name, color, desc, descColor, optionalSee) {
        let ESpan = createSpanWithColor(tabDivider + name, color);
        let descSpan = createSpanWithColor(desc, descColor || printColor);
        ESpan.innerHTML += " @";
        ESpan.appendChild(descSpan);

        if (optionalSee) {
            let optionalSpan = createSpanWithColor(" @see::" + optionalSee, keywordColor);
            ESpan.appendChild(optionalSpan);
        }
        add(ESpan);
    }


    /*function addKeywordWithUsage(keyword, usage) {
        let keywordColor = "#2196F3";
        let kSpan = createSpanWithColor(tabDivider, keywordColor);
        keyword.innerHTML += ": ";
        keyword.style.color = keywordColor;
        kSpan.appendChild(keyword);
        kSpan.appendChild(usage);
        add(kSpan);
    }*/


    function addUsage(usage) {
        let USpan = createSpanWithColor(tabDivider, printColor);
        USpan.appendChild(usage);
        add(USpan);
    }


    function addKeywordSpan(keyword, desc) {
        let keywordColor = "#2196F3";
        let kSpan = createSpanWithColor(tabDivider, keywordColor);
        keyword.innerHTML += ": ";
        keyword.style.color = keywordColor;
        kSpan.appendChild(keyword);
        kSpan.appendChild(desc);
        add(kSpan);
    }


    function createUsageDesc(desc, other, color) {
        let USpan = createSpanWithColor("usage: ", color || usageColor);
        let usageSpan = getTextSpan(colorCodeCommand(desc));
        let otherSpan = createSpanWithColor(" " + other, color || printColor);
        USpan.appendChild(usageSpan);
        USpan.appendChild(otherSpan);
        return USpan;
    }


    if (!div) {
        return;
    }

    let headerColor = "#ee194b";
    let headerSpan = createSpanWithColor("List of available commands", headerColor);
    div.appendChild(headerSpan);
    addNewline();
    addNewline();


    // Matrix predefined variable
    let predefinedMatrix = createSpanWithColor("matrix", printColor);
    let predefinedDesc = getTextSpan(": variable shown left as '\\(M_{matrix} \\)'.");

    predefinedMatrix.appendChild(predefinedDesc);
    add(predefinedMatrix);

    let matrix = getTextSpan("anyMatrix");
    let invOperator = createSpanWithColor(".inv", printColor);

    matrix.appendChild(invOperator);

    let inverseDesc = getTextSpan(": Gets inverse of matrix.");

    matrix.appendChild(inverseDesc);

    add(matrix);


    // matrix syntax:
    let matrixShowcase = "[a b c | d e f | g h i]";
    let matrixSyntax = createSpanWithColor("Syntax for creating 3x3 matrix from elements: ", printColor);

    let matrixShowcaseDesc = getTextSpan(matrixShowcase);
    matrixSyntax.appendChild(matrixShowcaseDesc);

    add(matrixSyntax);

    //addNewline();
    /*addSection("canvas events", sectionColor, "list of available actions (does not work well with stack/bugs)", headerColor);
    addEventSpan("left click", eventColor, "selects a shape when it's not already selected.");
    addEventSpan("ctrl + left click", eventColor, "toggles shape selection.");
    addEventSpan("click and drag", eventColor, "moves object around in a provided grid.");
    addEventSpan("right click", eventColor, "if any number of shapes is selected, deselects them all.");
    addEventSpan("right click", eventColor, "if nothing is selected offers a dialog for creating shapes.");
    addNewline();
    addEventSpan("keyboard arrows", eventColor, "if any number of shapes is selected, moves them in a grid respectively.");
    addEventSpan("key '+' and '-'", eventColor, "rotates any number of selected shapes by rotate step (default: 45).", printColor, "setRotateStep");
    addEventSpan("key '1' and '2'", eventColor,
        "scales all selected shapes horizontally. (not yet fully supported/bugs)", printColor, "setScaleStepX");
    addEventSpan("key '3' and '4'", eventColor,
        "scales all selected shapes vertically. (not yet fully supported/bugs)", printColor, "setScaleStepY");
    addEventSpan("key p'", eventColor,
        "toggles option panel for creating additional shapes.", printColor);
    addEventSpan("key 'escape'", eventColor,
        "removes option panel for creating shapes if displayed.", printColor);*/

    let pop = createSpanWithColor("pop", printColor);
    let popDesc = getTextSpan("Removes first element from stack.");
    let traceOn = createSpanWithColor("trace on", printColor);
    let traceOnDesc = getTextSpan("Enables tracing stack matrices.");
    let traceOff = createSpanWithColor("trace off", printColor);
    let traceOffDesc = getTextSpan("Disables tracing stack matrices.");

    // Points tracing
    let pointsTraceOn = createSpanWithColor("points on", printColor);
    let pointsTraceOnDesc = getTextSpan("Shows tracing points.");
    let pointsTraceOff = createSpanWithColor("points off", printColor);
    let pointsTraceOffDesc = getTextSpan("Hides tracing points.");

    // Printing points
    let printPoints = createSpanWithColor("printPoints", printColor);
    let printPointsDesc = getTextSpan("Prints all points to console.");

    // Adding and removing points
    let addPoint = createSpanWithColor("addPoint", printColor);
    let addPointUsage = createUsageDesc('addPoint x y #hexColor optional[cross, square, rhombus, circle]', "");

    let removePoint = createSpanWithColor("removePoint", printColor);
    let removePointUsage = createUsageDesc("removePoint {id or all}", "(removes point with provided id or all points)");

    let printStack = createSpanWithColor("printStack", printColor);
    let printStackDesc = getTextSpan("Prints whole stack to console with stack indexes.");

    let clearStack = createSpanWithColor("clearStack", printColor);
    let clearStackDesc = getTextSpan("Removes all elements from stack.");

    let clearScreen = createSpanWithColor("clearScreen", printColor);
    let clearScreenDesc = getTextSpan("Clears console and command history.");

    let keywordPrint = createSpanWithColor("print", printColor);
    let keywordDesc = getTextSpan("Prints its only argument to console.");

    /*let setRotateStep = createSpanWithColor("setRotateStep", printColor);
    let setRotateStepDesc = createUsageDesc("setRotateStep number ", "(sets rotate step, allowed values are &lt;-360, 360&gt;)");

    let setScaleXStep = createSpanWithColor("setScaleStepX", printColor);
    let setScaleXStepDesc = createUsageDesc("setScaleStepX number", "(sets horizontal scale step, allowed values are &lt;-5, 5&gt;)");

    let setScaleYStep = createSpanWithColor("setScaleStepY", printColor);
    let setScaleYStepDesc = createUsageDesc("setScaleStepY number", "(sets vertical scale step, allowed values are &lt;-5, 5&gt;)");
*/
    let trueKeyword = createSpanWithColor("true", printColor);
    let trueKeywordDesc = createSpanWithColor("gets boolean with value true", printColor);

    let falseKeyword = createSpanWithColor("false", printColor);
    let falseKeywordDesc = createSpanWithColor("gets boolean with value false", printColor);

    let printOutline = createSpanWithColor("printOutlineColor", printColor);
    let printOutlineDesc = getTextSpan("Prints current outline color.");
    let printFill = createSpanWithColor("printFillColor", printColor);
    let printFilLDesc = getTextSpan("Prints current fill color.");

    addNewline();
    addSection("keywords", sectionColor, "list of available keywords", headerColor);
    addKeywordSpan(keywordPrint, keywordDesc);

    addKeywordSpan(printPoints, printPointsDesc);
    addKeywordSpan(printStack, printStackDesc);
    addKeywordSpan(pop, popDesc);
    addKeywordSpan(traceOn, traceOnDesc);
    addKeywordSpan(traceOff, traceOffDesc);
    addKeywordSpan(pointsTraceOn, pointsTraceOnDesc);
    addKeywordSpan(pointsTraceOff, pointsTraceOffDesc);
    addKeywordSpan(clearStack, clearStackDesc);
    addKeywordSpan(clearScreen, clearScreenDesc);
    addKeywordSpan(trueKeyword, trueKeywordDesc);
    addKeywordSpan(falseKeyword, falseKeywordDesc);
    addKeywordSpan(printOutline, printOutlineDesc);
    addKeywordSpan(printFill, printFilLDesc);
    addNewline();

    // With Usages
    let defaultOutline = createSpanWithColor("defaultOutlineColor", printColor);
    let defaultOutlineDesc = createUsageDesc("defaultOutlineColor #hexColor", "(sets default outline color for all following shapes)");

    let defaultFill = createSpanWithColor("defaultFillColor", printColor);
    let defaultFillDesc = createUsageDesc("defaultFillColor #hexColor", "(sets default fill color for all following shapes which can be filled)");


    addSection("usage descriptions", sectionColor, "list of examples how to use keywords and other expressions", printColor);
    addKeywordSpan(addPoint, addPointUsage);
    addKeywordSpan(removePoint, removePointUsage);
    /* addKeywordSpan(setRotateStep, setRotateStepDesc);
     addKeywordSpan(setScaleXStep, setScaleXStepDesc);
     addKeywordSpan(setScaleYStep, setScaleYStepDesc);*/
    addKeywordSpan(defaultOutline, defaultOutlineDesc);
    addKeywordSpan(defaultFill, defaultFillDesc);
    addNewline();

    let selection = createSpanWithColor("select", printColor);
    let selectionUsage = createUsageDesc("select shape with #outlineColor and #fillColor, shape..., lastShape", "(renders provided shapes, can be zero or more)");
    let selectionExample = createUsageDesc("select polygon(0, 0, 2, 0, 2, 2, 2, 4), line(-5, -5, 0, 0);", "");
    addSection("Select statement", sectionColor, "selects certain shapes from provided list", headerColor);
    addKeywordSpan(selection, selectionUsage);
    addUsage(selectionExample);


    // Shape constructor
    /*["Line", "Square", "Rectangle", "LetterF", "Point", "Polyline", "Polygon"]*/
    let lineConst = createSpanWithColor("Line", printColor);
    let lineConstDesc = createUsageDesc("select line(xs ys, xe, xe)", "(startPoint(xs, ys), endPoint(xe, ye)");

    let squareConst = createSpanWithColor("Square", printColor);
    let squareConstDesc = createUsageDesc("select square(lowerLeftX, lowerLeftY, sideLength)", "(startPoint(x, y) and side length)");

    let rectangleConst = createSpanWithColor("Rectangle", printColor);
    let rectangleConstDesc = createUsageDesc("select rectangle(lowerLeftX, lowerLeftY, width, height)", "(startPoint(x, y) with width and height)");

    let letterF = createSpanWithColor("LetterF", printColor);
    let letterFDesc = createUsageDesc("select letterF", "(shows letterF)");

    let pointShape = createSpanWithColor("Point", printColor);
    let pointShapeDesc = createUsageDesc("select point(x, y)", "(single point(x, y), at provided grid coordinates)");

    let polylineShape = createSpanWithColor("Polyline", printColor);
    let polylineShapeDesc = createUsageDesc("select polyline(...args)", "(arguments for constructing polyline. Must be even number)");

    let polygonShape = createSpanWithColor("Polygon", printColor);
    let polygonShapeDecs = createUsageDesc("select polygon(...args)", "(arguments for constructing polygon. Minimum of 6 coordinates and must be even number of them)");


    addNewline();
    addSection("Shape constructors", sectionColor, "list of shapes that can be used for select statement.", printColor);
    addKeywordSpan(lineConst, lineConstDesc);
    addKeywordSpan(squareConst, squareConstDesc);
    addKeywordSpan(rectangleConst, rectangleConstDesc);
    addKeywordSpan(letterF, letterFDesc);
    addKeywordSpan(pointShape, pointShapeDesc);
    addKeywordSpan(polylineShape, polylineShapeDesc);
    addKeywordSpan(polygonShape, polygonShapeDecs);

    // Section outline and fill optional parameters

    let optionalOutline = createSpanWithColor("outlineColor", printColor);
    let optionalOutlineDesc = createUsageDesc("select anyShape(shapeArguments) with #00FF00", "renders shape with outline color #00FF00");
    let optionalFill = createSpanWithColor("fillColor", printColor);
    let optionalFillDesc = createUsageDesc("select someShapes(shapeArguments) with #00FF00 and #FF0000", "renders shape with fill color #FF0000");
    let textDescForFill = createSpanWithColor(tabDivider + "Fill color is applicable only on closed shapes: [Square, Rectangle, Polygon and LetterF].", importantColor);

    addNewline();
    addSection("Additional parameters (optional)", sectionColor, "outline and fill colors for rendering shapes. ", headerColor);
    add(textDescForFill);
    addKeywordSpan(optionalOutline, optionalOutlineDesc);
    addKeywordSpan(optionalFill, optionalFillDesc);

    addNewline();


    let funcIdentity = createSpanWithColor("identity", keywordColor);
    let argsIdentity = getTextSpan("()");

    /* let funcSquareShape = createSpanWithColor("square", keywordColor);
     let argsFuncSquareShape = getTextSpan("(x, y, sideLength)");
    */
    let funcCos = createSpanWithColor("cos", keywordColor);
    let argsCos = getTextSpan("(angleInDegrees)");
    let funcSin = createSpanWithColor("sin", keywordColor);
    let argsSin = getTextSpan("(angleInDegrees)");
    let funcTan = createSpanWithColor("tan", keywordColor);
    let argsTan = getTextSpan("(angleInDegrees)");
    let funcStack = createSpanWithColor("stack", keywordColor);
    let argsStack = getTextSpan("(index)");
    let funcShear = createSpanWithColor("shear", keywordColor);
    let argsShear = getTextSpan("(alpha, beta)");
    let funcTranslate = createSpanWithColor("translate", keywordColor);
    let argsTranslate = getTextSpan("(tx, ty)");
    let funcRotate = createSpanWithColor("rotate", keywordColor);
    let argsRotate = getTextSpan("(angleInDegrees)");
    let funcScale = createSpanWithColor("scale", keywordColor);
    let argsScale = getTextSpan("(sx, sy)");
    let funcPrecision = createSpanWithColor("precision", keywordColor);
    let argsPrecision = getTextSpan("(number)");


    //funcSquareShape.appendChild(argsFuncSquareShape);
    funcIdentity.appendChild(argsIdentity);
    funcPrecision.appendChild(argsPrecision);
    funcCos.appendChild(argsCos);
    funcSin.appendChild(argsSin);
    funcTan.appendChild(argsTan);
    funcStack.appendChild(argsStack);
    funcShear.appendChild(argsShear);
    funcTranslate.appendChild(argsTranslate);
    funcRotate.appendChild(argsRotate);
    funcScale.appendChild(argsScale);

    addSection("functions", sectionColor, "list of available functions", printColor);
    addFuncSpan(funcIdentity);
    //addFuncSpan(funcSquareShape);
    addFuncSpan(funcPrecision);
    addFuncSpan(funcCos);
    addFuncSpan(funcSin);
    addFuncSpan(funcTan);
    addFuncSpan(funcShear);
    addFuncSpan(funcTranslate);
    addFuncSpan(funcRotate);
    addFuncSpan(funcScale);
    addFuncSpan(funcStack);


}


/**
 * Parses array to nice string output.
 *
 * @param arr array to transform to string
 * @returns {string} nice string of given array
 */
function beautifyArray(arr) {
    let strBuilder = "[";

    for (let c = 0, size = arr.length; c < size; c++) {
        let x = arr[c];
        strBuilder += ((c !== 0) ? (", " + x) : (x));
    }

    strBuilder += "]";
    return strBuilder;
}


/**
 * Returns list of string lines for printing matrix to console.
 *
 * @param matrix matrix to construct strings from
 * @returns {Array} array of strings
 */
function getMatrixString(matrix) {
    let precision = globalUserEnv.get("printPrecision");

    // Get plain array of fixed matrix strings only if precision is different from actual data
    let m = matrix.toFixedString(precision);

    let lines = [];

    let firstSpaces = ((Number(m[0][0]) < 0 ) ? "&nbsp;" : "&nbsp;&nbsp;");
    let str = "[" + firstSpaces;

    let rows = m.length;
    let cols = m[0].length;
    for (let c = 0; c < rows; c++) {
        for (let k = 0; k < cols; k++) {
            let x = m[c][k];
            let nextX = m[c][k + 1];

            let spaces = (Number(nextX) < 0) ? "&nbsp;&nbsp;" : "&nbsp;&nbsp;&nbsp;";
            if (c === 2 && k === 2) {
                spaces = "&nbsp;";
            }

            str += (x + spaces);

        }


        if (c === 2) {
            str += "]";
            lines.push(str);
        } else {
            lines.push(str);
            str = (Number(m[c + 1][0]) < 0) ? "&nbsp;&nbsp;" : "&nbsp;&nbsp;&nbsp;";
        }

    }

    return lines;
}


/**
 * Gets a tab component. Readability purpose.
 *
 * @param id tab identification
 * @returns {Element} tab element
 */
function getTabEasy(id) {
    return document.getElementById(id);
}


/**
 * Gets console tab component. Readability purpose.
 *
 * @returns {Element} console tab element
 */
function getConsoleEasy() {
    return document.getElementById("console");
}


/**
 * Helper function to append div child to parent.
 *
 * @param str string which newly created div will hold.
 * @param parent parent to append to
 */
function addLineToParent(str, parent) {
    let newElem = document.createElement("div");
    newElem.className = "envValue";
    newElem.innerHTML = str;

    parent.appendChild(newElem);

}


/**
 * Creates simple arrow in a "span" tag.
 *
 * @returns {Element} span tag with arrow specified style and innerHTML of an arrow.
 */
function createArrow() {
    let elem = document.createElement("span");

    elem.className = "expandArrow";
    elem.innerHTML = getStateEasy().constants.getString("collapsedSymbol");

    return elem;
}


/**
 * Creates new environment variable.
 *
 * @param envConsole console to put variable to
 * @param numOfDefinedVars current number of defined variables
 * @param numVarsId id for saving next number of defined variables
 * @param name variable name
 */
function createNewEnvironmentVar(envConsole, numOfDefinedVars, numVarsId, name) {
    function arrowCallback(e) {
        expandValueEvent(e, name);
    }


    let emptyLine = findSpaceInConsole(envConsole);
    let childArrow = createArrow();
    globalEnv.set(numVarsId, numOfDefinedVars);

    emptyLine.appendChild(childArrow);

    childArrow.onclick = arrowCallback;
    createInitialExpansion(name);

    emptyLine.appendChild(createSpanWithColor(colorCodeCommand(name), "#FFFFFF"));
}


/**
 * Creates pane dialog.
 */
function createPaneDialog() {
    let state = getStateEasy();
    let optionPane = document.getElementById("optionPane");
    let shapeNames = state.constants.getString("shapeTypes");

    let divHolder = document.createElement("div");
    let shapeSize = shapeNames.length;
    let creator = getCreatorLab(state);
    for (let c = 0; c < shapeSize; c++) {
        let newDiv = document.createElement("div");
        newDiv.id = shapeNames[c] + "Shape";
        newDiv.className = "optionLine";
        newDiv.innerHTML = "Create shape: " + shapeNames[c] + ".";
        newDiv.onclick = creator.dispatchCreateDialog;

        divHolder.appendChild(newDiv);
    }

    optionPane.appendChild(divHolder);
}


/**
 * Shows options for user click at position x, y at canvas for selecting or creating shapes.
 *
 * @param x mouse coordinate
 * @param y mouse coordinate
 */
function showOptionPane(x, y) {
    let optionPane = document.getElementById("optionPane");
    let translateStr = "translate(" + x + "px, " + y + "px)";
    optionPane.style.webkitTransform = translateStr;
    optionPane.style.webkitTransform = translateStr;
    optionPane.style.MozTransform = translateStr;
    optionPane.style.msTransform = translateStr;
    optionPane.style.OTransform = translateStr;
    optionPane.style.transform = translateStr;


    let s = getStateEasy();
    s.events.optionPane.lastX = x;
    s.events.optionPane.lastY = y;

    togglePaneVisibility(optionPane, s);
}


/**
 * Helper function to toggle pane visibility.
 *
 * @param e element to toggle.
 * @param s state object.
 */
function togglePaneVisibility(e, s) {
    let isOpen = e.style.display === "block";
    e.style.display = (isOpen) ? "none" : "block";
    s.events.optionPane.paneOpen = !isOpen;
}


function createFontChanger() {

}