function init() {
    let graphics = initCanvas("letterFRotationAroundOrigin");
    let letterF = new LetterFShape("LetterF" + createUID(112), 3, 2);
    graphics.addShape(letterF);

    setUIGadgets(graphics, letterF);
    graphics.update();
}


function setUIGadgets(state, obj) {
    let upd = new UpdateFunctions(obj, state);
    let rotationId = "rotationValue3";
    let rotateOpts = {
        functionCallback: upd.updateAngle,
        min: -360,
        max: 360,
        /*name: "RotationSlider: " Inherits from parent HTML */
    };

    state.angleDegrees = setupSlider(rotationId, rotateOpts);
}


function UpdateFunctions(obj, state) {
    function updateAngle(event, ui) {
        let angleInDegrees = ui.value;

        state.userMatrix = $m3.rotation(degToRad(angleInDegrees));
        // Set UI
        let fixedForUI = state.userMatrix.toFixed(3);
        let m = $m3.asArrayByRows(fixedForUI);
        setUIMatrix(m, "m3");
        state.update();
    }


    return {
        updateAngle: updateAngle
    };
}


init();