/**
 * Event for clicking on tabbed component headers.
 *
 * @param id tab identification
 */
function processTabClick(id) {
    let tabComp = getTabCompEasy();
    tabComp.showTabById(id);

}


/**
 * Getter for actions of shapes.
 *
 * @param state state object
 */
function getShapesEventActions(state) {
    return state.events.shapes.actions;
}


function getCreatorLab(state) {
    return state.events.optionPane.creation;
}


/**
 * Processes mouse move for webGL mouse move event.
 *
 * @param e event object
 * @param state reference to state object holding all shapes and events properties
 */
function webGLMouseMoveCallback(e, state) {
    let m = getMouse(e);
    let dx = m.x - state.events.dragX;
    let dy = m.y - state.events.dragY;

    let moveX = Math.floor(dx / state.step);
    let moveY = -Math.floor(dy / state.step);

    let creating = getCreatorLab(state);
    if (state.events.dragging && state.events.selected) {
        function translateShape(sh) {
            let initialTransform = sh.transform;
            sh.setDrawableTransforms(initialTransform.mul($m3.translation(moveX, moveY)));
        }


        let sShapes = state.events.selectedObjects;
        Object.keys(sShapes).forEach((key) => {
            state.objects.doOnShape(translateShape, sShapes[key].id, []);
        });

        state.update();
    } else if (state.events.optionPane.activeCreating !== null) {
        let workingShape = state.events.optionPane.activeShape;
        creating.dispatchOnEventType("mousemove", workingShape, state, m);
        state.update();

    }

    // Set lasts for optionPane
    state.events.optionPane.lastX = m.x;
    state.events.optionPane.lastY = m.y;

}


/**
 * Key down event.
 *
 * @param e event object
 * @param state reference to state object holding all shapes and events properties
 */
function webGLKeyDownEvent(e, state) {
    let x = e.which || e.keyCode;


    let code = state.constants.keyCodes;
    let successful = true;

    let tx = 0;
    let ty = 0;
    switch (x) {
        case code.right:
            tx = 1;
            break;
        case code.left:
            tx = -1;
            break;
        case code.up:
            ty = 1;
            break;
        case code.down:
            ty = -1;
            break;
        case code.escape:
            document.getElementById("optionPane").style.display = "none";
            break;
        default:
            successful = true;
    }


    let selectedObjects = state.events.selectedObjects;
    if ($.isEmptyObject(selectedObjects)) {
        return;
    }
    if (successful && ((tx !== 0) || (ty !== 0 ))) {
        /*Object.keys(selectedObjects).forEach((key) => {
            let sh = selectedObjects[key];
            sh.translate(tx, ty);
        });*/
        let translateM = $m3.translation(tx, ty);
        state.userMatrix = state.userMatrix.mul(translateM);
        setTransformToF(state.userMatrix);

        e.preventDefault();
        state.update();
    }


}


/**
 * Key press event.
 *
 * @param e event object
 * @param state reference to state object holding all shapes and events properties
 */
function webGLKeyPressEvent(e, state) {
    let x = e.which || e.keyCode;
    let y = String.fromCharCode(x).toUpperCase();

    let rotStep = (state.events.rotStep);
    let scaleStepX = state.events.scaleStepX;
    let scaleStepY = state.events.scaleStepY;

    let successful = true;
    let rotateDegree = 0;
    let scaleX = 1;
    let scaleY = 1;

    switch (y) {
        case '+':
            rotateDegree += rotStep;
            break;
        case '-':
            rotateDegree -= rotStep;
            break;
        case '1':
            scaleX += scaleStepX;
            break;
        case '3':
            scaleY += scaleStepY;
            break;
        case '2':
            scaleX -= scaleStepX;
            break;
        case '4':
            scaleY -= scaleStepY;
            break;
        case 'P':
        case 27:
            let pane = state.events.optionPane;
            showOptionPane(pane.lastX, pane.lastY);
            break;
        default:
            successful = false;
    }

    let selectedObjects = state.events.selectedObjects;
    if ($.isEmptyObject(selectedObjects)) {
        return;
    }

    if (successful) {
        if (((scaleX !== 0) || (scaleY !== 0) || (rotateDegree !== 0))) {
            /*Object.keys(selectedObjects).forEach((key) => {
                let sh = selectedObjects[key];
                //sh.scale(scaleX, scaleY);
                //sh.rotate(rotateDegree);


            });*/
            let scaleM = $m3.scale(scaleX, scaleY);
            let rotateM = $m3.rotation(degToRad(rotateDegree));
            state.userMatrix = state.userMatrix.mul(scaleM).mul(rotateM);
            setTransformToF(state.userMatrix);
        }
        e.preventDefault();
        state.update();

    }
}


/**
 * Event mouse up for webGL context. Sets dragging to false.
 *
 * @param e event object
 * @param state reference to state object holding all shapes and events properties
 */
function webGLMouseUpCallback(e, state) {
    let selectedObjects = state.events.selectedObjects;
    Object.keys(selectedObjects).forEach((key) => {
        selectedObjects[key].refreshTransform();
    });

    state.events.dragging = false;

}


function clearSelection(shape, state) {
    shape.transform = shape.strokeProp.transform;
    shape.strokeStyle = state.events.savedColors[shape.id];
    shape.selected = false;
}


function checkForShapeCreation(m, s) {
    if (s.events.optionPane.activeCreating !== null) {
        let creating = getCreatorLab(s);
        let pane = s.events.optionPane;
        let workingShape = pane.activeShape;
        creating.dispatchOnEventType("click", workingShape, s, m);
    }
}


function clearShapeCreation(pane) {
    let state = getStateEasy();
    let creating = getCreatorLab(state);
    creating.dispatchOnEventType("rightClick", pane.activeShape, state);

    pane.activeCreating = null;
    pane.activeShape = null;

}


/**
 * Event mouse down for webGL context. Sets dragging to true. Dispatches selections
 * @param e event object
 * @param state reference to state object holding all shapes and events properties
 */
function webGLMouseDownCallback(e, state) {
    let m = getMouse(e);
    let actCreate = state.events.optionPane.activeCreating;

    let type = getMouseButtonType(e);
    if (type === "left") {
        if ((actCreate === null) && state.events.optionPane.paneOpen === false) {
            setSelection(e, state, m);
        }

        if (state.events.selected === false) {
            checkForShapeCreation(m, state);
        }
    } else if (type === "right") {
        if ((state.events.selected === false) && (actCreate === null)) {
            showOptionPane(m.x, m.y);
        }

        if (state.events.selected) {
            let selectedObjects = state.events.selectedObjects;
            Object.keys(selectedObjects).forEach((key) => {
                clearSelection(selectedObjects[key], state);

            });

            state.events.selectedObjects = {};
            state.events.selected = false;
        }

        if (actCreate !== null) {
            clearShapeCreation(state.events.optionPane);
        }
    }

    state.update();

}


function transformSinglePoint(x, y) {
    let state = getStateEasy();
    let origin = calcOrigin(state.canvas, state.step);
    let originTranslate = ($m3.translation(origin.x, origin.y));

    let inverseMatrix = $m3.inv(originTranslate);


    let invertedP = $m3.postMul(inverseMatrix, [x, y, 1]);
    let sp = {
        x: Math.floor(invertedP[0]),
        y: -Math.floor(invertedP[1])
    };
    sp.x = (sp.x / state.step);
    sp.y = (sp.y / state.step);

    return sp;


}


function transformToShapePoint(p, sh) {
    let userMatrix = getStateEasy().userMatrix;
    let vec = $m3.postMul($m3.inv(sh.transform.mul(sh.centerTransform).mul(userMatrix)), [p.x, p.y, 1]);
    let centerX = sh.x || 0;
    let centerY = sh.y || 0;
    return {x: vec[0] + centerX, y: vec[1] + centerY};
}


function setSelection(e, state, mouse) {
    let size = state.objects.shapesToDraw.length;
    for (let c = size - 1; c >= 0; c--) {
        let sh = state.objects.shapesToDraw[c];
        let temp = transformSinglePoint(mouse.x, mouse.y);
        let tp = transformToShapePoint(temp, sh);

        if (sh.contains(tp.x, tp.y)) {
            if (!sh.selected) {
                state.events.selected = true;
                state.events.selectedObjects[sh.id] = sh;
                state.events.savedColors[sh.id] = sh.strokeStyle;

                sh.strokeStyle = getGLColorFromHex(state.constants.getColor("selectedShapeColor"));
                sh.selected = true;

            } else if (e.ctrlKey) {
                clearSelection(sh, state);
                sh.selected = false;
                delete state.events.selectedObjects[sh.id];
            }

            break;
        }


    }

    state.events.dragging = true;
    state.events.dragX = mouse.x;
    state.events.dragY = mouse.y;
}


/**
 * Gets type of a mouse button pressed.
 *
 * @param ev event object
 * @returns {*} ["right", "middle" or "left"]
 */
function getMouseButtonType(ev) {
    let isRightClick;
    let isMiddleClick;
    let e = ev || window.event;

    if ("which" in e) {
        isRightClick = e.which === 3;
        isMiddleClick = e.which === 2;
    } else if ("button" in e) {
        isRightClick = e.button === 2;
        isMiddleClick = e.button === 1;
    }

    if (isRightClick) {
        return "right";
    } else if (isMiddleClick) {
        return "middle";
    } else {
        return "left";
    }
}


/**
 * Helper function to get mouse coordinate from target element.
 *
 * @param e event object
 * @returns {{x: Number, y: Number}} mouse coordinates in respect to targeted bounding box
 */
function getMouse(e) {
    let rect = e.target.getBoundingClientRect();
    let pos = {
        x: e.clientX - rect.left,
        y: e.clientY - rect.top
    };

    return {x: parseInt(pos.x), y: parseInt(pos.y)}
}


/**
 * Helper function to prevent right clicking at canvas.
 *
 * @param e event object
 */
function webGLContextMenuCallback(e) {
    e.preventDefault();
    return false;
}


/**
 * Helper function to prevent right clicking at canvas.
 *
 * @param e event object
 */
function webGLSelectStartCallback(e) {
    e.preventDefault();
    return false;
}

