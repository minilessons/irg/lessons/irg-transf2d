/**
 * Builds webGL buffer with given parameters.
 * @param gl webGL context
 * @param type type of buffer
 * @param data data for buffer
 * @param itemSize dimension of a vertex (2, 3 or 4)
 * @returns {WebGLBuffer} webGL buffer
 */
function buildBuffer(gl, type, data, itemSize) {
    let buffer = gl.createBuffer();
    let arrayView = type === gl.ARRAY_BUFFER ? Float32Array : Uint16Array;

    gl.bindBuffer(type, buffer);
    gl.bufferData(type, new arrayView(data), gl.STATIC_DRAW);

    buffer.itemSize = itemSize;
    buffer.numItems = data.length / itemSize;
    return buffer;
}


function WebGLUtils() {

    function createUniformSetter(gl, program, uniformInfo) {
        let location = gl.getUniformLocation(program, uniformInfo.name);
        let type = uniformInfo.type;

        // Check if this uniform is an array
        let isArray = (uniformInfo.size > 1 && uniformInfo.name.substr(-3) === "[0]");
        if (type === gl.FLOAT && isArray) {
            return function (v) {
                gl.uniform1fv(location, v);
            };
        }
        if (type === gl.FLOAT) {
            return function (v) {
                gl.uniform1f(location, v);
            };
        }
        if (type === gl.FLOAT_VEC2) {
            return function (v) {
                gl.uniform2fv(location, v);
            };
        }
        if (type === gl.FLOAT_VEC3) {
            return function (v) {
                gl.uniform3fv(location, v);
            };
        }
        if (type === gl.FLOAT_VEC4) {
            return function (v) {
                gl.uniform4fv(location, v);
            };
        }
        if (type === gl.INT && isArray) {
            return function (v) {
                gl.uniform1iv(location, v);
            };
        }
        if (type === gl.INT) {
            return function (v) {
                gl.uniform1i(location, v);
            };
        }
        if (type === gl.INT_VEC2) {
            return function (v) {
                gl.uniform2iv(location, v);
            };
        }
        if (type === gl.INT_VEC3) {
            return function (v) {
                gl.uniform3iv(location, v);
            };
        }
        if (type === gl.INT_VEC4) {
            return function (v) {
                gl.uniform4iv(location, v);
            };
        }
        if (type === gl.BOOL) {
            return function (v) {
                gl.uniform1iv(location, v);
            };
        }
        if (type === gl.BOOL_VEC2) {
            return function (v) {
                gl.uniform2iv(location, v);
            };
        }
        if (type === gl.BOOL_VEC3) {
            return function (v) {
                gl.uniform3iv(location, v);
            };
        }
        if (type === gl.BOOL_VEC4) {
            return function (v) {
                gl.uniform4iv(location, v);
            };
        }
        if (type === gl.FLOAT_MAT2) {
            return function (v) {
                gl.uniformMatrix2fv(location, false, v);
            };
        }
        if (type === gl.FLOAT_MAT3) {
            return function (v) {
                gl.uniformMatrix3fv(location, false, v);
            };
        }
        if (type === gl.FLOAT_MAT4) {
            return function (v) {
                gl.uniformMatrix4fv(location, false, v);
            };
        }

        // we should never get here.
        throw new Error("Unknown type for uniform SETTER: 0x" + type.toString());
    }


    function createUniformSetters(gl, program) {
        let uniformSetters = {};
        let numUniforms = gl.getProgramParameter(program, gl.ACTIVE_UNIFORMS);

        for (let c = 0; c < numUniforms; c++) {
            let uniformInfo = gl.getActiveUniform(program, c);
            if (!uniformInfo) {
                break;
            }
            let name = uniformInfo.name;
            // remove the array suffix.
            if (name.substr(-3) === "[0]") {
                name = name.substr(0, name.length - 3);
            }
            uniformSetters[name] = createUniformSetter(gl, program, uniformInfo);
        }
        return uniformSetters;
    }


    function createAttribSetters(gl, program) {
        let attribSetters = {};


        function createAttribSetter(index) {
            return function (b) {
                gl.bindBuffer(gl.ARRAY_BUFFER, b.buffer);
                gl.enableVertexAttribArray(index);
                gl.vertexAttribPointer(
                    index, b.numComponents || b.size, b.type || gl.FLOAT, b.normalize || false, b.stride || 0, b.offset || 0);
            };
        }


        let attributesCount = gl.getProgramParameter(program, gl.ACTIVE_ATTRIBUTES);
        for (let c = 0; c < attributesCount; c++) {
            let attribInfo = gl.getActiveAttrib(program, c);
            if (!attribInfo) {
                break;
            }
            let index = gl.getAttribLocation(program, attribInfo.name);
            attribSetters[attribInfo.name] = createAttribSetter(index);
        }

        return attribSetters;
    }


    function allButIndices(name) {
        return name !== "indices";
    }


    function createMapping(obj) {
        let mapping = {};
        Object.keys(obj).filter(allButIndices).forEach(function (key) {
            mapping["a_" + key] = key;
        });
        return mapping;
    }


    function getGLTypeForTypedArray(gl, typedArray) {
        if (typedArray instanceof Int8Array) {
            return gl.BYTE;
        }
        if (typedArray instanceof Uint8Array) {
            return gl.UNSIGNED_BYTE;
        }
        if (typedArray instanceof Int16Array) {
            return gl.SHORT;
        }
        if (typedArray instanceof Uint16Array) {
            return gl.UNSIGNED_SHORT;
        }
        if (typedArray instanceof Int32Array) {
            return gl.INT;
        }
        if (typedArray instanceof Uint32Array) {
            return gl.UNSIGNED_INT;
        }
        if (typedArray instanceof Float32Array) {
            return gl.FLOAT;
        }
        throw new Error("Unsupported typed array type");
    }


    function isArrayBuffer(a) {
        return a.buffer && a.buffer instanceof ArrayBuffer;
    }


    function makeTypedArray(array, name) {

        function augmentTypedArray(typedArray, numComponents) {
            let cursor = 0;
            typedArray.push = function () {
                for (let ii = 0; ii < arguments.length; ++ii) {
                    let value = arguments[ii];
                    if (value instanceof Array || (value.buffer && value.buffer instanceof ArrayBuffer)) {
                        for (let jj = 0; jj < value.length; ++jj) {
                            typedArray[cursor++] = value[jj];
                        }
                    } else {
                        typedArray[cursor++] = value;
                    }
                }
            };
            typedArray.reset = function (opt_index) {
                cursor = opt_index || 0;
            };
            typedArray.numComponents = numComponents;
            Object.defineProperty(typedArray, 'numElements', {
                get: function () {
                    return this.length / this.numComponents | 0;
                },
            });
            return typedArray;
        }


        function createAugmentedTypedArray(numComponents, numElements, opt_type) {
            let Type = opt_type || Float32Array;
            return augmentTypedArray(new Type(numComponents * numElements), numComponents);
        }


        if (isArrayBuffer(array)) {
            return array;
        }

        if (Array.isArray(array)) {
            array = {
                data: array,
            };
        }


        let type = array.type;
        if (!type) {
            if (name === "indices") {
                type = Uint16Array;
            }
        }

        let typedArray = createAugmentedTypedArray(array.numComponents, array.data.length / array.numComponents | 0, type);
        typedArray.push(array.data);
        return typedArray;
    }


    function createBufferFromTypedArray(gl, array, type, drawType) {
        type = type || gl.ARRAY_BUFFER;
        let buffer = gl.createBuffer();
        gl.bindBuffer(type, buffer);
        gl.bufferData(type, array, drawType || gl.STATIC_DRAW);
        return buffer;
    }


    function createAttribsFromArrays(gl, arrays, opt_mapping) {
        let mapping = opt_mapping || createMapping(arrays);
        let attribs = {};
        Object.keys(mapping).forEach(function (attribName) {
            let bufferName = mapping[attribName];
            let array = makeTypedArray(arrays[bufferName], bufferName);
            attribs[attribName] = {
                buffer: createBufferFromTypedArray(gl, array),
                numComponents: array.numComponents,
                type: getGLTypeForTypedArray(gl, array),
                normalize: arrays.normalize || false,
            };
        });
        return attribs;
    }


    function getNumElementsFromNonIndexedArrays(arrays) {
        let key = Object.keys(arrays)[0];
        let array = arrays[key];
        if (isArrayBuffer(array)) {
            return array.numElements;
        } else {
            return array.data.length / array.numComponents;
        }
    }


    function createBufferInfoFromArrays(gl, arrays, opt_mapping) {
        let bufferInfo = {
            attribs: createAttribsFromArrays(gl, arrays, opt_mapping),
        };

        let indices = arrays.indices;
        if (indices) {
            indices = makeTypedArray(indices, "indices");
            bufferInfo.indices = createBufferFromTypedArray(gl, indices, gl.ELEMENT_ARRAY_BUFFER);
            bufferInfo.numElements = indices.length;
        } else {
            bufferInfo.numElements = getNumElementsFromNonIndexedArrays(arrays);
        }

        return bufferInfo;
    }


    function setAttributes(setters, attributes) {
        setters = setters.attribSetters || setters;
        Object.keys(attributes).forEach(function (name) {
            let setter = setters[name];
            if (setter) {
                setter(attributes[name]);
            }
        });
    }


    function setUniforms(setters, values) {
        setters = setters.uniformSetters || setters;
        Object.keys(values).forEach(function (name) {
            let setter = setters[name];
            if (setter) {
                setter(values[name]);
            }
        });
    }


    function setBuffersAndAttributes(gl, setters, buffers) {
        setAttributes(setters, buffers.attribs);
        if (buffers.indices) {
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffers.indices);
        }
    }


    function createProgramInfo(gl, arr) {
        let shaderProgram = initShaders(gl, arr[0], arr[1]);

        return {
            program: shaderProgram,
            uniformSetters: createUniformSetters(gl, shaderProgram),
            attribSetters: createAttribSetters(gl, shaderProgram)
        }
    }


    return {
        createProgramInfo: createProgramInfo,
        createBufferInfoFromArrays: createBufferInfoFromArrays,
        setBuffersAndAttributes: setBuffersAndAttributes,
        setUniforms: setUniforms
    };
}