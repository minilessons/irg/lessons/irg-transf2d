/**
 * Initializes shaders for gl context
 *
 * @param gl context to attach shaders to
 * @param vertId id of a vertex shader
 * @param fragId id of a fragment shader
 * @returns {WebGLProgram} shader program
 */
function initShaders(gl, vertId, fragId) {
    let fragmentID = fragId;
    let vertexID = vertId;
    let fragmentShader = getShader(gl, fragmentID);
    let vertexShader = getShader(gl, vertexID);

    // Create shader program
    let shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);
    gl.linkProgram(shaderProgram);

    // Alert if failed
    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
        console.log("Unable to initialize the shader program> " + gl.getProgramInfoLog(shaderProgram));
    }

    return shaderProgram;
}


/**
 * Gets shader from document, compiles it
 * @param gl openGL context
 * @param id shader script identification in document
 * @param type type of a shader to compile
 * @returns {*}
 */
function getShader(gl, id, type) {
    let shaderScript, theSource, shader;
    shaderScript = document.getElementById(id);
    if (!shaderScript) {
        return null;
    }

    theSource = shaderScript.text;
    if (!type) {
        if (shaderScript.type === "x-shader/x-fragment") {
            type = gl.FRAGMENT_SHADER;
        } else if (shaderScript.type === "x-shader/x-vertex") {
            type = gl.VERTEX_SHADER;
        } else {
            // Unknown shader type
            return null;
        }
    }

    shader = gl.createShader(type);
    gl.shaderSource(shader, theSource);

    gl.compileShader(shader);

    // See for compilation errors
    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        console.log("An error occurred during shader compiling: " + gl.getShaderInfoLog(shader));
        gl.deleteShader(shader);
        return null;
    }

    return shader;
}