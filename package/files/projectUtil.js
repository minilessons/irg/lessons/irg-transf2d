function initCanvas(canvasID) {
    let canvas2D = document.getElementById(canvasID);
    let renderState;
    if (canvas2D) {
        renderState = new RenderState(canvas2D);
        if (renderState === null) {
            throw "Cannot initialize webGL canvas state for 2D app.";
        }
    } else {
        throw "Cannot initialize webGL canvas for 2D app.";
    }

    return renderState;
}


/**
 * Function for manipulating greatest common divider and reducing fractions.
 *
 * @returns {{getGCD: getGCD, reduce: reduce}}
 */
function FractionReduce() {
    let getGCD = function (n, d) {

        if (n === undefined || d === undefined) {
            throw "Cannot find greatest common divisor. Not enough parameters";
        }
        let numerator = (n < d) ? n : d;
        let denominator = (n < d) ? d : n;
        let remainder = numerator;
        let lastRemainder = numerator;

        while (remainder !== 0) {
            lastRemainder = remainder;
            remainder = denominator % numerator;
            denominator = numerator;
            numerator = remainder;
        }

        if (lastRemainder) {
            return lastRemainder;
        }
    };

    let reduce = function (n, d) {
        let gcd = getGCD(n, d);

        return [n / gcd, d / gcd];
    };

    return {
        getGCD: getGCD,
        reduce: reduce
    };

}


function calculateFraction(d, n, reducer) {
    if (d === 0) {
        return 0;
    }
    let string = reducer.reduce(d, n);

    let outShow;
    let absN = Math.abs(string[0]);
    let absD = Math.abs(string[1]);
    if (absN === absD) {
        outShow = "1";
    } else if (absD === 1) {
        outShow = absN;
    }
    else {
        outShow = absN + "/" + absD;
    }

    let sign = (d < 0) ? "-" : "";
    return ("".concat(sign, outShow));
}


/**
 * Helper function to retrieve state.
 *
 * @returns {object} state object
 */
function getStateEasy() {
    return globalEnv.get("myState");
}


/**
 * Renders given matrix to UI.
 *
 * @param matrix matrix to set UI to
 * @param matrixIdPrefix matrix ID
 */
function setUIMatrix(matrix, matrixIdPrefix) {
    let id = matrixIdPrefix === undefined ? "m" : matrixIdPrefix;
    let size = 3;
    for (let c = 0; c < size; c++) {
        for (let k = 0; k < size; k++) {
            let constructedString = id + c + k;
            let div = document.getElementById(constructedString);
            div.innerHTML = (matrix[k + c * size]);
        }
    }
}



