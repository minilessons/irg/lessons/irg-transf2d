/**
 * Function for constructing coordinate array to use for webGL buffer data.
 *
 * @param gl reference to webGL context
 * @param s step used for line distance
 * @returns {{values: Array, colors: Array}}
 */

function getCoordinateArray(gl, s) {
    let arr = [];
    let colorArr = [];

    let step = s;
    let verStart = 0;
    let horStart = 0;
    let verEnd = gl.canvas.clientHeight;
    let horEnd = gl.canvas.clientWidth;


    let o = calcOrigin(gl.canvas, step);
    let xAxis = o.x;

    let gridColor = convertHexToRGBA("#333840").normalize();
    //let gridColor = convertHexToRGBA("#8acb6c").normalize();
    let axisColor = convertHexToRGBA("#4bbbbb").normalize();
    //let axisColor = convertHexToRGBA("#030201").normalize();

    let axisX = [];
    for (let c = 0; c < (horEnd + step); c += step) {
        if (c === xAxis) {
            axisX = [c, verStart, c, verEnd];

        } else {
            colorArr.push(gridColor.r, gridColor.g, gridColor.b, gridColor.a);
            colorArr.push(gridColor.r, gridColor.g, gridColor.b, gridColor.a);

            arr.push(c);
            arr.push(verStart);
            arr.push(c);
            arr.push(verEnd);
        }

    }

    let yAxis = o.y;
    let axisY = [];
    for (let y = 0; y < (step + verEnd); y += step) {
        if (y === yAxis) {
            axisY = [horStart, y, horEnd, y];
        } else {
            colorArr.push(gridColor.r, gridColor.g, gridColor.b, gridColor.a);
            colorArr.push(gridColor.r, gridColor.g, gridColor.b, gridColor.a);

            arr.push(horStart);
            arr.push(y);
            arr.push(horEnd);
            arr.push(y);
        }
    }


    arr.push(axisX[0], axisX[1], axisX[2], axisX[3]);
    colorArr.push(axisColor.r, axisColor.g, axisColor.b, axisColor.a);
    colorArr.push(axisColor.r, axisColor.g, axisColor.b, axisColor.a);
    arr.push(axisY[0], axisY[1], axisY[2], axisY[3]);
    colorArr.push(axisColor.r, axisColor.g, axisColor.b, axisColor.a);
    colorArr.push(axisColor.r, axisColor.g, axisColor.b, axisColor.a);

    return {values: arr, colors: colorArr};
}

