function init() {
    let graphics = initCanvas("commonRasterCanvas");
    let letterF = new LetterFShape("LetterF" + createUID(112), 0, 0);
    graphics.addShape(letterF);

    setUIGadgets(graphics, letterF);
    graphics.update();

    setTransformMatrix(graphics, 0);
}


function setUIGadgets(state, obj) {
    let upd = new UpdateFunctions(obj, state);
    let rotationId = "rotationValue";
    let rotateOpts = {
        functionCallback: upd.updateAngle,
        min: -360,
        max: 360,
        /*name: "RotationSlider: " Inherits from parent HTML */
    };

    state.angleDegrees = setupSlider(rotationId, rotateOpts);
}


function UpdateFunctions(obj, state) {
    function updateAngle(event, ui) {
        let angleInDegrees = ui.value;

        obj.setTransform($m3.rotation(degToRad(angleInDegrees)));
        setTransformMatrix(state, angleInDegrees);
        state.update();
    }


    return {
        updateAngle: updateAngle
    };
}


function setTransformMatrix(state, angle) {
    let cosineFirst = document.getElementById("m00");
    let sineFirst = document.getElementById("m01");
    let negSine = document.getElementById("m10");
    let cosineSecond = document.getElementById("m11");

    let closingString = angle + state.constants.strings.degreeSymbol + ")&nbsp;";
    cosineFirst.innerHTML = "cos(" + closingString;
    sineFirst.innerHTML = "sin(" + closingString;
    negSine.innerHTML = "-sin(" + closingString;
    cosineSecond.innerHTML = "cos(" + closingString;
}


init();