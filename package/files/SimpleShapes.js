function Shape() {
}


function inheritFromShape(child) {
    child.prototype = Object.create(Shape.prototype);
}


Shape.prototype.refreshTransform = function (t) {
    this.transform = t || this.strokeProp.transform;
};

Shape.prototype.setDrawableTransforms = function (m) {
    if (this.fillProp !== undefined) {
        this.fillProp.transform = (m);
    }
    this.strokeProp.transform = (m);
};

Shape.prototype.multiplyWithTransform = function (m) {
    this.transform = this.transform.mul(m);
    this.setDrawableTransforms(this.transform);
};

Shape.prototype.setTransform = function (m) {
    this.transform = m;
};

Shape.prototype.translate = function (tx, ty) {
    this.transform = this.transform.mul($m3.translation(tx, ty));
    this.setDrawableTransforms(this.transform);
};

Shape.prototype.rotate = function (angleInDegrees) {
    this.transform = this.transform.mul($m3.rotation(degToRad(angleInDegrees)));
    this.setDrawableTransforms(this.transform);
};

Shape.prototype.scale = function (sx, sy) {
    this.transform = this.transform.mul($m3.scale(sx, sy));
    this.setDrawableTransforms(this.transform);
};


/**
 * SimplePoint function representing few types of points.
 *
 * @param id point identification number
 * @param x point x coordinate
 * @param y point y coordinate
 * @param color point color in hexadecimal
 * @param type type of point to construct, can be circle (default), rhombus, cross and square
 * @constructor
 */
function SimplePoint(id, x, y, color, type) {
    this.id = id || "UndefinedPointID";
    this.type = type || "circle";


    this.primitiveStrokeType = getPrimitiveType(this.type);
    this.primitiveFillType = this.primitiveStrokeType;

    this.x = x || 0;
    this.y = y || 0;
    this.radius = getConstants().numeric.circleRadius || 0.33;
    this.hexColor = color || "#FF0000";
    this.fillStyle = getGLColorFromHex(color || "#FF0000");
    this.strokeStyle = getGLColorFromHex("#00FF00");


    this.fillData = getVerticesForSimplePoint(this.type);
    this.strokeData = getStrokeDataForSimplePoint(this.type);

    // Transform
    this.transform = getTransformForSimplePoint(this.x, this.y, this.type);
    this.centerTransform = $m3.identity();

    // Selection
    this.selected = false;

    this.refreshShapeData = function () {
        this.transform = getTransformForSimplePoint(this.x, this.y, this.type);
    };

    this.moveOrigin = function (x, y) {
        this.x = Math.floor(x);
        this.y = Math.floor(y);
        this.refreshShapeData();
    };

}


function SquareShape(id, x, y, sideLength, fillStyle, strokeStyle) {
    this.id = id || "UndefinedSquareID";
    this.x = x;
    this.y = y;
    this.side = sideLength || 1;

    this.primitiveStrokeType = "LINE_LOOP";
    this.primitiveFillType = "TRIANGLES";

    this.fillStyle = fillStyle || getGLColorFromHex("#FF0000");
    this.strokeStyle = strokeStyle || getGLColorFromHex("#00FF00");

    // Selection
    this.selected = false;

    this.refreshShapeData = function () {
        this.fillData = constructSquare(0, 0, this.side);
        this.centerTransform = $m3.translation(-this.side / 2, -this.side / 2);
        this.strokeData = constructSquareVertices(-this.side / 2, -this.side / 2, this.side, this.side);
        this.transform = $m3.translation(this.side / 2, this.side / 2).mul($m3.translation(this.x, this.y));
    };

    this.refreshShapeData();

    this.changeSide = function (x, y) {
        let dx = Math.floor(x - this.x);
        this.side = Math.abs(dx) || 1;

        this.refreshShapeData();
    };


}


function House(id, x, y) {
    this.x = x || 0;
    this.y = y || 0;

    this.id = id || "UndefinedHouseID";
    this.primitiveStrokeType = "LINE_STRIP";
    this.primitiveFillType = "TRIANGLES";

    this.fillStyle = getGLColorFromHex("#5181ca");
    this.strokeStyle = getGLColorFromHex("#f24950");

    this.vertUnits = 4;
    this.horzUnits = 5;
    this.numOfTriangles = 6;
    this.colors = createHouseColors(this.numOfTriangles * 3);
    this.fillData = constructHouse(this.vertUnits);
    this.strokeData = constructHouseVertices(this.vertUnits);
    this.centerTransform = $m3.identity();
    this.transform = $m3.identity().mul($m3.translation(this.x, this.y));

    // Selection
    this.selected = false;

    this.refreshShapeData = function () {
        this.transform = $m3.identity().mul($m3.translation(this.x, this.y));
    };

    this.moveOrigin = function (x, y) {
        this.x = Math.floor(x);
        this.y = Math.floor(y);
        this.refreshShapeData();
    }
}


function LetterFShape(id, x, y) {
    this.x = x || 0;
    this.y = y || 0;

    this.id = id || "UndefinedLetterFID";
    this.primitiveStrokeType = "LINE_STRIP";
    this.primitiveFillType = "TRIANGLES";

    this.fillStyle = getGLColorFromHex("#53cab7");
    this.strokeStyle = getGLColorFromHex("#000000");

    this.vertUnits = 5;
    this.horzUnits = 3;
    this.numOfTriangles = 6;
    this.colors = createLetterColors(this.numOfTriangles * 3);
    this.fillData = constructLetterF(this.vertUnits);
    this.strokeData = constructLetterFVertices(this.vertUnits);
    this.centerTransform = $m3.identity();
    this.transform = $m3.identity().mul($m3.translation(this.x, this.y));

    // Selection
    this.selected = false;

    this.refreshShapeData = function () {
        this.transform = $m3.identity().mul($m3.translation(this.x, this.y));
    };

    this.moveOrigin = function (x, y) {
        this.x = Math.floor(x);
        this.y = Math.floor(y);
        this.refreshShapeData();
    }
}


function RectangleShape(id, x, y, w, h) {
    this.id = id || "UndefinedRectangleID";
    this.x = x;
    this.y = y;
    this.w = w || 1;
    this.h = h || 1;

    this.primitiveStrokeType = "LINE_LOOP";
    this.primitiveFillType = "TRIANGLES";

    this.fillStyle = getGLColorFromHex("#FF0000");
    this.strokeStyle = getGLColorFromHex("#00FF00");

    // Selection
    this.selected = false;

    this.refreshShapeData = function () {
        this.fillData = constructRectangle(0, 0, this.w, this.h, "horizontal");
        this.strokeData = constructSquareVertices(-this.w / 2, -this.h / 2, this.w, this.h);
        this.centerTransform = $m3.translation(-this.w / 2, -this.h / 2);
        this.transform = $m3.translation(this.w / 2, this.h / 2).mul($m3.translation(this.x, this.y));
    };


    this.refreshShapeData();

    this.changeSides = function (x, y) {
        let dx = Math.floor(x - this.x);
        let dy = Math.floor(y - this.y);
        this.w = Math.abs(dx) || 1;
        this.h = Math.abs(dy) || 1;

        this.refreshShapeData();
    };


}


function LineShape(id, x, y, x1, y1) {
    this.id = id || "UndefinedLineID";

    this.x = x;
    this.y = y;
    this.x1 = x1;
    this.y1 = y1;

    this.vertices = [x, y, x1, y1];

    this.primitiveStrokeType = "LINES";
    this.strokeStyle = getGLColorFromHex("#00FF00");

    this.strokeData = [this.x, this.y, this.x1, this.y1];
    this.centerTransform = $m3.identity();
    this.transform = $m3.identity();

    // Selection
    this.selected = false;

    this.refreshShapeData = function () {
        this.strokeData = this.vertices.map((p) => {
            return Math.floor(p);
        });

        this.x = this.strokeData[0];
        this.y = this.strokeData[1];
        this.x1 = this.strokeData[2];
        this.y1 = this.strokeData[3];

    };
}


function PolygonShape(id, ...arguments) {
    this.id = id || "UndefinedPolygonID";
    this.vertices = Array.prototype.slice.call(arguments)[0];

    this.primitiveStrokeType = "LINE_LOOP";
    this.primitiveFillType = "TRIANGLES";

    this.fillStyle = getGLColorFromHex("#FF0000");
    this.strokeStyle = getGLColorFromHex("#00FF00");

    this.fillData = constructPointVertices(this.vertices);
    this.strokeData = constructPolylineVertices(this.vertices);
    this.centerTransform = $m3.identity();
    this.transform = $m3.identity();

    // Selection
    this.selected = false;

    this.refreshShapeData = function () {
        this.strokeData = constructPolylineVertices(this.vertices).map((p) => {
            return Math.floor(p);
        });
        this.fillData = constructPointVertices(this.vertices).map((p) => {
            return Math.floor(p);
        });
    };

}


function PolylineShape(id, ...arguments) {
    this.id = id || "UndefinedPolylineID";
    this.vertices = Array.prototype.slice.call(arguments)[0];


    this.primitiveStrokeType = "LINE_STRIP";
    this.strokeStyle = getGLColorFromHex("#00FF00");

    this.strokeData = constructPolylineVertices(this.vertices);

    this.centerTransform = $m3.identity();
    this.transform = $m3.identity();

    // Selection
    this.selected = false;

    this.refreshShapeData = function () {
        this.strokeData = constructPolylineVertices(this.vertices).map((p) => {
            return Math.floor(p);
        });
    }
}


/**
 * Constructs string representation of SimplePoint object.
 *
 * @returns {string}
 */
SimplePoint.prototype.toString = function () {
    return SimplePoint.toString(this);
};

SimplePoint.toString = function (p) {
    let aligned = getAlignedNumbers("    ", p);
    return "ID:" + aligned.id + ",&nbsp;&nbsp;x:" + aligned.x + ",&nbsp;&nbsp;y:" + aligned.y + ",&nbsp;&nbsp;color:" + p.hexColor.toUpperCase();

};


inheritFromShape(SimplePoint);
inheritFromShape(SquareShape);
inheritFromShape(LetterFShape);
inheritFromShape(RectangleShape);
inheritFromShape(PolylineShape);
inheritFromShape(PolygonShape);
inheritFromShape(LineShape);
inheritFromShape(House);