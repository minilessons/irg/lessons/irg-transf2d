function getStrokeDataForSimplePoint(type) {
    let c = getConstants();
    let sideLength = c.getNumeric(type + "Length");

    switch (type) {
        case "square":
        case "rhombus":
            return constructSquareVertices(-sideLength / 2, -sideLength / 2, sideLength, sideLength);
            break;
        case "cross":
            let rw = c.getNumeric("crossRectW");
            let rh = c.getNumeric("crossRectH");
            let data = constructSquareVertices(-rw / 2, -rh / 2, rw, rh);
            data = data.concat(constructSquareVertices - rw / 2, -rh / 2, rw, rh);

            return data;
            break;
        case "circle":
            return constructCircle(c.getNumeric("circleRadius"));
            break;
        default:
            throw new Error("Unsupported Simple Point type for type: '" + type + "'.");
    }
}


function getPrimitiveType(type) {
    let triangles = "TRIANGLES";
    switch (type) {
        case "square":
            return triangles;
        case "cross":
            return triangles;
        case "rhombus":
            return triangles;
        case "circle":
            return "TRIANGLE_FAN";
        default:
            throw new Error("Unsupported Simple Point type for type: '" + type + "'.");
    }
}


function getTransformForSimplePoint(x, y, type) {
    let mTranslate = $m3.translation(x, y);
    switch (type) {
        case "square":
            return mTranslate;
            break;
        case "cross":
            return $m3.rotation(degToRad(45)).mul(mTranslate);
            break;
        case "rhombus":
            return $m3.rotation(degToRad(45)).mul(mTranslate);
            break;
        case "circle":
            return mTranslate;
            break;
        default:
            throw new Error("Unsupported Simple Point type for type: '" + type + "'.");
    }
}


function getVerticesForSimplePoint(type) {
    let c = getConstants();
    let sideLength = c.getNumeric(type + "Length");

    switch (type) {
        case "square":
            return constructSquare(0, 0, sideLength);
            break;
        case "cross":
            let rectangleWidth = c.getNumeric("crossRectW");
            let rectangleHeight = c.getNumeric("crossRectH");
            let data = constructRectangle(0, 0, rectangleWidth, rectangleHeight, "horizontal");
            data = data.concat(constructRectangle(0, 0, rectangleWidth, rectangleHeight, "vertical"));

            return data;
            break;
        case "rhombus":
            return constructSquare(0, 0, sideLength);
            break;
        case "circle":
            return constructCircle(c.getNumeric("circleRadius"));
            break;
        default:
            throw new Error("Unsupported Simple Point type for type: '" + type + "'.");
    }
}


/**
 * Aligns numbers for SimplePoint toString printing.
 *
 * @param spaces number of spaces to align numbers to
 * @param p  point reference
 * @returns {{id, x, y}} object with aligned values
 */
function getAlignedNumbers(spaces, p) {
    let spacesL = -1 * spaces.length;
    let res = {
        id: (spaces + p.id).slice(spacesL),
        x: (spaces + p.x).slice(spacesL),
        y: (spaces + p.y).slice(spacesL)
    };

    res.id = res.id.replace(/ /g, "&nbsp;");
    res.x = res.x.replace(/ /g, "&nbsp;");
    res.y = res.y.replace(/ /g, "&nbsp;");

    return res;
}


function constructHouseVertices(tall) {
    return [
        0, 0,
        0, 1,
        0.2, 1.5,
        0.4, 1,
        0.4, 0,
        0.2, 0,
    ].map((e) => {
        return (e * tall);
    });
}


function constructHouse(tall) {
    return [
        // left side of house
        0, 0,
        0.2, 0,
        0, 1,

        0, 1,
        0.2, 0,
        0.2, 1,

        // right side of house
        0.2, 0,
        0.4, 0,
        0.2, 1,

        0.2, 1,
        0.4, 0,
        0.4, 1,

        // Roof left part
        0, 1,
        0.2, 1,
        0.2, 1.5,

        0.2, 1,
        0.2, 1.5,
        0.4, 1,
    ].map((e) => {
        return (e * tall);
    });
}


/* Utility functions for constructing Simple Shapes. */
function constructLetterF(tall) {

    return [
        // left column
        0, 0,
        0.2, 0,
        0, 1,
        0, 1,
        0.2, 0,
        0.2, 1,

        // top rung
        0.2, 0.8,
        0.6, 0.8,
        0.6, 1,

        0.2, 1,
        0.6, 1,
        0.2, 0.8,

        // middle rung
        0.2, 0.4,
        0.4, 0.4,
        0.2, 0.6,

        0.2, 0.6,
        0.4, 0.4,
        0.4, 0.6,
    ].map((e) => {
        return (e * tall);
    });

}


function constructPolylineVertices(sentArgs) {
    return sentArgs;
}


function constructPointVertices(args) {
    let vertices = [];
    let numOfTriangles = ((args.length) / 2) - 2;
    for (let c = 0; c < numOfTriangles; c++) {
        vertices.push(args[0]);
        vertices.push(args[1]);

        vertices.push(args[2 * c + 2]);
        vertices.push(args[2 * c + 3]);

        vertices.push(args[2 * c + 4]);
        vertices.push(args[2 * c + 5]);
    }
    return vertices;
}


function constructCircle(radius) {
    let vertices = [];
    for (let c = 0; c < 361; c++) {
        let radians = degToRad(c);

        let vertexOne = [
            radius * Math.sin(radians),
            radius * Math.cos(radians)
        ];

        let vertexTwo = [
            0, 0
        ];

        vertices = vertices.concat(vertexOne);
        vertices = vertices.concat(vertexTwo);
    }
    return vertices;
}


function constructSquare(x, y, sideLength) {
    let halfLength = sideLength / 2 || 0.33;
    return [
        // First triangle
        x - halfLength, y + halfLength,
        x + halfLength, y + halfLength,
        x + halfLength, y - halfLength,

        // Second triangle
        x - halfLength, y + halfLength,
        x - halfLength, y - halfLength,
        x + halfLength, y - halfLength
    ];
}


function array2DTo1D(arr) {
    let res = [];
    arr.forEach((x) => {
        x.forEach((y) => {
            res.push(y);
        })
    });

    return res;
}


function constructRectangle(cx, cy, w, h, type) {
    let halfW = w / 2;
    let halfH = h / 2;
    let rectVertices = [
        // First triangle
        [cx - halfW, cy - halfH],
        [cx - halfW, cy + halfH],
        [cx + halfW, cy + halfH],

        // Second triangle
        [cx + halfW, cy - halfH],
        [cx + halfW, cy + halfH],
        [cx - halfW, cy - halfH]
    ];

    if (type === "vertical") {
        rectVertices = rectVertices.map((elements) => {
            let v = $vec(elements.concat(1));
            let transformedPoint = $m3.postMul($m3.rotation(degToRad(90)), v);

            return [transformedPoint[0], transformedPoint[1]];
        });

        return array2DTo1D(rectVertices);
    } else {
        return array2DTo1D(rectVertices);
    }
}


function constructLetterFLines() {
    return [
        new LineShape("noId", 0, 0, 0.2, 0),
        new LineShape("noId", 0.2, 0, 0.2, 0.4),
        new LineShape("noId", 0.2, 0.4, 0.4, 0.4),
        new LineShape("noId", 0.4, 0.4, 0.4, 0.6),
        new LineShape("noId", 0.4, 0.6, 0.2, 0.6),
        new LineShape("noId", 0.2, 0.6, 0.2, 0.8),
        new LineShape("noId", 0.2, 0.8, 0.6, 0.8),
        new LineShape("noId", 0.6, 0.8, 0.6, 1),
        new LineShape("noId", 0.6, 1, 0.0, 1),
        new LineShape("noId", 0.0, 1, 0.0, 0.0)
    ];

}


function constructLetterFVertices(tall) {
    return [
        0, 0,
        0.2, 0,
        0.2, 0.4,
        0.4, 0.4,
        0.4, 0.6,
        0.2, 0.6,

        0.2, 0.8,
        0.6, 0.8,
        0.6, 1,

        0.0, 1,
        0.0, 0.0,

    ].map((e) => {
        return (e * tall);
    });
}


function createHouseColors(numOfVertices) {
    let fColor = {r: Math.random(), g: Math.random(), b: Math.random(), a: 1};
    let fColorData = [];
    for (let c = 0; c < numOfVertices; c++) {
        /*fColorData.push(fColor.r);
        fColorData.push(Math.random());
        fColorData.push(fColor.b);
        fColorData.push(fColor.a);*/
        fColorData.push(fColor.r);
        fColorData.push(fColor.g);
        fColorData.push(fColor.b);
        fColorData.push(fColor.a);
    }

    return fColorData;
}


function createLetterColors(numOfVertices) {
    let fColor = {r: Math.random(), g: Math.random(), b: Math.random(), a: 1};
    let fColorData = [];
    for (let c = 0; c < numOfVertices; c++) {
        fColorData.push(fColor.r);
        fColorData.push(Math.random());
        fColorData.push(fColor.b);
        fColorData.push(fColor.a);
    }

    return fColorData;
}


function constructSquareVertices(x, y, w, h) {
    let vertices = [];

    vertices.push(x, y);
    vertices.push(x + w, y);
    vertices.push(x + w, y + h);
    vertices.push(x, y + h);

    return vertices;
}