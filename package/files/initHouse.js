function init() {
    let scaledThenTraslated = $m3.identity().mul($m3.scale(2, 1)).mul($m3.translation(5, 4));
    let translatedThenScaled = $m3.identity().mul($m3.translation(5, 4)).mul($m3.scale(2, 1));
    createHouseView("houseScaledAndTranslated", scaledThenTraslated, "m");
    createHouseView("houseTranslatedThenScaled", translatedThenScaled, "m2");
}


function createHouseView(canvasID, transformation, matrixPrefix) {
    let graphics = initCanvas(canvasID);
    let letterF = new LetterFShape("LetterF" + createUID(112), 0, 0);

    letterF.setTransform(transformation);

    graphics.addShape(letterF);

    setUIMatrix(transformation.asArrayByRows(), matrixPrefix);
    graphics.update();
}


init();