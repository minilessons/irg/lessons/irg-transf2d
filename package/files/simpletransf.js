function vect2(x, y) {
    this.x = x;
    this.y = y;
}


vect2.prototype.add = function (other) {
    return new vect2(this.x + other.x, this.y + other.y);
}
vect2.prototype.norm = function () {
    return Math.sqrt(this.x * this.x + this.y * this.y);
}
vect2.prototype.normalize = function () {
    var n = this.norm();
    return new vect2(this.x / n, this.y / n);
}
vect2.prototype.scaled = function (d) {
    return new vect2(this.x * d, this.y * d);
}


function SimpleTransfCanvas(canvasID) {
    this.canvas = document.getElementById(canvasID);
    this.context = this.canvas.getContext("2d");
    var ctx = this.context;
    this.vector = [1, 1];
    this.transformedVector = [1, 1];
    this.transform = [[-1, 0], [0, 1], [0, 0]];
    this.newPoint = null;

    this.drawArrow = function (ox, oy, ex, ey) {
        let vx = ex - ox, vy = ey - oy, n = Math.sqrt(vx * vx + vy * vy);
        vx /= n;
        vy /= n;
        let nx = -vy, ny = vx, base = 10, perp = 5;
        this.context.beginPath();
        this.context.moveTo(ox, oy);
        this.context.lineTo(ex, ey);
        this.context.moveTo(ex - base * vx + perp * nx, ey - base * vy + perp * ny);
        this.context.lineTo(ex, ey);
        this.context.moveTo(ex - base * vx - perp * nx, ey - base * vy - perp * ny);
        this.context.lineTo(ex, ey);
        this.context.stroke();
    }

    this.paint = function () {
        let ox = this.canvas.width / 2;
        let oy = this.canvas.height / 2;
        let sx = 0.5;
        let sy = -0.5;

        if (this.newPoint) {
            this.vector[0] = (this.newPoint[0] - ox) / (ox * sx);
            this.vector[1] = (this.newPoint[1] - oy) / (oy * sy);
            this.newPoint = null;
        }

        this.transformedVector[0] = this.vector[0] * this.transform[0][0] + this.vector[1] * this.transform[1][0] + this.transform[2][0];
        this.transformedVector[1] = this.vector[0] * this.transform[0][1] + this.vector[1] * this.transform[1][1] + this.transform[2][1];

        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.context.fillStyle = "#ffffee";
        this.context.strokeStyle = "#000000";
        this.context.beginPath();
        this.context.rect(0, 0, this.canvas.width, this.canvas.height);
        this.context.stroke();

        // Show axis:
        this.context.strokeStyle = "#aaaaaa";
        this.context.beginPath();
        this.context.moveTo(ox, 0);
        this.context.lineTo(ox, 2 * oy);
        this.context.moveTo(0, oy);
        this.context.lineTo(2 * ox, oy);
        this.context.stroke();

        // Show original vector:
        this.context.lineWidth = 1;
        this.context.strokeStyle = "#000000";
        this.drawArrow(ox, oy, ox + this.vector[0] * ox * sx, oy + this.vector[1] * oy * sy);
        /*    this.context.beginPath();
            this.context.moveTo(ox, oy);
            this.context.lineTo(ox+this.vector[0]*ox*sx, oy+this.vector[1]*oy*sy);
            this.context.stroke();
        */
        // Show transformed vector:
        this.context.lineWidth = 1;
        this.context.strokeStyle = "#FF0000";
        this.drawArrow(ox, oy, ox + this.transformedVector[0] * ox * sx, oy + this.transformedVector[1] * oy * sy);
        /*    this.context.beginPath();
            this.context.moveTo(ox, oy);
            this.context.lineTo(ox+this.transformedVector[0]*ox*sx, oy+this.transformedVector[1]*oy*sy);
            this.context.stroke();
        */
    }

    var me = this;
    me.trackMouse = false;

    this.canvas.addEventListener('mousemove', function (evt) {
        if (!me.trackMouse) return;
        var rect = me.canvas.getBoundingClientRect();
        var pos = {x: evt.clientX - rect.left, y: evt.clientY - rect.top};
        me.newPoint = [pos.x, pos.y];
        me.paint();
    });
    this.canvas.addEventListener('mousedown', function (evt) {
        me.trackMouse = true;
        var rect = me.canvas.getBoundingClientRect();
        var pos = {x: evt.clientX - rect.left, y: evt.clientY - rect.top};
        me.newPoint = [pos.x, pos.y];
        me.paint();
    });
    this.canvas.addEventListener('mouseup', function (evt) {
        me.trackMouse = false;
    });

    this.paint();
}

function SimpleTransf2Canvas(canvasID,matrixBaseID) {
    this.canvas = document.getElementById(canvasID);
    this.context = this.canvas.getContext("2d");
    var ctx = this.context;

    this.transformedPoints = null;

    this.drawArrow = function (ox, oy, ex, ey) {
        let vx = ex - ox, vy = ey - oy, n = Math.sqrt(vx * vx + vy * vy);
        vx /= n;
        vy /= n;
        let nx = -vy, ny = vx, base = 10, perp = 5;
        this.context.beginPath();
        this.context.moveTo(ox, oy);
        this.context.lineTo(ex, ey);
        this.context.moveTo(ex - base * vx + perp * nx, ey - base * vy + perp * ny);
        this.context.lineTo(ex, ey);
        this.context.moveTo(ex - base * vx - perp * nx, ey - base * vy - perp * ny);
        this.context.lineTo(ex, ey);
        this.context.stroke();
    }

    function affineInverse(mat) {
      let det = 1.0/(mat[0][0]*mat[1][1]-mat[1][0]*mat[0][1]);
      let A = [
       [det*mat[1][1],-det*mat[0][1]],
       [-det*mat[1][0],det*mat[0][0]]
      ];
      let res = [
       [A[0][0],A[0][1]],
       [A[1][0],A[1][1]],
       [-mat[2][0]*A[0][0]-mat[2][1]*A[1][0],-mat[2][0]*A[0][1]-mat[2][1]*A[1][1]]
      ];
      return res;
    }
    function mul3mm(m1, m2) {
      return [
       [m1[0][0]*m2[0][0]+m1[0][1]*m2[1][0], m1[0][0]*m2[0][1]+m1[0][1]*m2[1][1]],
       [m1[1][0]*m2[0][0]+m1[1][1]*m2[1][0], m1[1][0]*m2[0][1]+m1[1][1]*m2[1][1]],
       [m1[2][0]*m2[0][0]+m1[2][1]*m2[1][0]+m2[2][0], m1[2][0]*m2[0][1]+m1[2][1]*m2[1][1]+m2[2][1]]
      ];
    }

    function mulPAT(v, m) {
      return [v[0]*m[0][0]+v[1]*m[1][0]+m[2][0],v[0]*m[0][1]+v[1]*m[1][1]+m[2][1]];
    }

    this.finalTransform = [
          [this.canvas.width/5,    0],
          [0,                      -this.canvas.height / 5],
          [this.canvas.width / 2,  this.canvas.height / 2],
        ];

    this.drawKS = function(xdims, ydims, xtick, ytick, mat, strcol, fillcol, config) {
        let p1 = mulPAT([xdims[0],0], mat);
        let p2 = mulPAT([xdims[1],0], mat);
        let p3 = mulPAT([0,ydims[0]], mat);
        let p4 = mulPAT([0,ydims[1]], mat);
        let p5 = mulPAT([0,0], mat);

        // show handles
        if(config && config.xdot) {
          this.context.fillStyle = config.xcolor;
          this.context.beginPath();
          this.context.arc(p2[0], p2[1], 4, 0, 2*Math.PI);
          this.context.closePath();
          this.context.fill();
        }
        if(config && config.ydot) {
          this.context.fillStyle = config.ycolor;
          this.context.beginPath();
          this.context.arc(p4[0], p4[1], 4, 0, 2*Math.PI);
          this.context.closePath();
          this.context.fill();
        }
        if(config && config.odot) {
          this.context.fillStyle = config.ocolor;
          this.context.beginPath();
          this.context.arc(p5[0], p5[1], 4, 0, 2*Math.PI);
          this.context.closePath();
          this.context.fill();
        }

        // Show axis:
        this.context.strokeStyle = strcol;
        this.context.beginPath();
        this.context.moveTo(p1[0], p1[1]);
        this.context.lineTo(p2[0], p2[1]);
        this.context.moveTo(p3[0], p3[1]);
        this.context.lineTo(p4[0], p4[1]);
        this.context.stroke();

        // show vectors
        if(config && config.xdot) {
          let p6 = mulPAT([1,0], mat);
          this.context.strokeStyle = config.xcolor;
          this.drawArrow(p5[0],p5[1],p6[0],p6[1]);
        }
        if(config && config.ydot) {
          let p6 = mulPAT([0,1], mat);
          this.context.strokeStyle = config.ycolor;
          this.drawArrow(p5[0],p5[1],p6[0],p6[1]);
        }

        this.context.strokeStyle = strcol;
        this.context.font="12px Verdana";
        this.context.textAlign="center";
        this.context.textBaseline="top";
        this.context.fillStyle = fillcol;
        for(let i = 0; i < xtick.length; i++) {
          let p = mulPAT([xtick[i],0], mat);
          this.context.beginPath();
          this.context.moveTo(p[0], p[1]-3);
          this.context.lineTo(p[0], p[1]+3);
          this.context.stroke();
          this.context.fillText(xtick[i], p[0],p[1]+5);
        }
        this.context.textAlign="right";
        this.context.textBaseline="middle";
        for(let i = 0; i < ytick.length; i++) {
          let p = mulPAT([0,ytick[i]], mat);
          this.context.beginPath();
          this.context.moveTo(p[0]-3, p[1]);
          this.context.lineTo(p[0]+3, p[1]);
          this.context.stroke();
          this.context.fillText(ytick[i], p[0]-3,p[1]);
        }
    }

    function drawCircle(ctx,cx,cy,r,mat) {
       let divs = 100;
       let p0 = mulPAT([cx+r,cy], mat);
       ctx.beginPath();
       ctx.moveTo(p0[0], p0[1]);
       for(let i = 1; i<=divs; i++) {
         let kut = 2*Math.PI*i/divs;
         let p1 = mulPAT([cx+r*Math.cos(kut),cy+r*Math.sin(kut)], mat);
         ctx.lineTo(p1[0], p1[1]);
         p0 = p1;
       }
       ctx.stroke();
    }

    this.drawSmiley = function(mat, strcol, fillcol) {
      this.context.strokeStyle = strcol;
      drawCircle(this.context, 0.5, 0.5, 0.5, mat);
      drawCircle(this.context, 0.7, 0.7, 0.1, mat);
    }
    function distance(v1,v2) {
      return Math.sqrt((v1[0]-v2[0])*(v1[0]-v2[0])+(v1[1]-v2[1])*(v1[1]-v2[1]));
    }
    function closestPoint(points, p) {
      if(points==null) return -1;
      let index = -1;
      let md = 0;
      for(let i = 0; i < points.length; i++) {
        let d = distance(points[i],p);
        if(i==0 || d < md) {
          md = d; index=i;
        }
      }
      if(index!=-1 && md<10) return index;
      return -1;
    }

    this.ks2Matrix = [[1,0],[0.4,1],[1,1]];

    function formatiraj(x) {
      x = Math.round(x*1000)/1000;
      let s = ""+x;
      let p = s.indexOf(".");
      if(p==-1) return s+".000";
      let l = s.length;
      let m = 3-(l-p-1);
      while(m>0) {
        s += "0"; m--;
      }
      return s;
    }
    function setMatrixDoc(m) {
      if(!matrixBaseID) return;
      document.getElementById(matrixBaseID+"_0_0").innerHTML = formatiraj(m[0][0]);
      document.getElementById(matrixBaseID+"_0_1").innerHTML = formatiraj(m[0][1]);
      document.getElementById(matrixBaseID+"_1_0").innerHTML = formatiraj(m[1][0]);
      document.getElementById(matrixBaseID+"_1_1").innerHTML = formatiraj(m[1][1]);
      document.getElementById(matrixBaseID+"_2_0").innerHTML = formatiraj(m[2][0]);
      document.getElementById(matrixBaseID+"_2_1").innerHTML = formatiraj(m[2][1]);
    }

    setMatrixDoc(this.ks2Matrix);

    this.paint = function () {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.context.fillStyle = "#ffffee";
        this.context.strokeStyle = "#000000";
        this.context.beginPath();
        this.context.rect(0, 0, this.canvas.width, this.canvas.height);
        this.context.stroke();

        this.drawKS(
          [-2.5,2.5], //xdims
          [-2.5,2.5], //ydims
          [-2, -1.5, -1, -0.5, 0.5, 1, 1.5, 2], //xtick, 
          [-2, -1.5, -1, -0.5, 0.5, 1, 1.5, 2], //ytick, 
          this.finalTransform, //mat, 
          "#aaaaaa", //strcol, 
          "#aaaaaa" //fillcol
        );
        this.drawSmiley(
          this.finalTransform, //mat, 
          "#aaaaaa", //strcol, 
          "#aaaaaa" //fillcol
        );

        let t2 = mul3mm(this.ks2Matrix, this.finalTransform);
        this.drawKS(
          [-1,1], //xdims
          [-1,1], //ydims
          [-1, -0.5, 0.5, 1], //xtick, 
          [-1, -0.5, 0.5, 1], //ytick, 
          t2, //mat, 
          "#ff77ff", //strcol, 
          "#ff77ff", //fillcol
          {xdot: true, xcolor: "#ff0000", ydot: true, ycolor: "#00ff00", odot: true, ocolor: "#0000ff"}
        );
        let ovecStart = mulPAT([0,0], this.finalTransform);
        let ovecEnd = mulPAT([0,0], t2);
        this.context.strokeStyle = "#0000ff";
        this.drawArrow(ovecStart[0],ovecStart[1],ovecEnd[0], ovecEnd[1]);
        this.drawSmiley(
          t2, //mat, 
          "#ff77ff", //strcol, 
          "#ff77ff" //fillcol
        );

        this.transformedPoints = [
          mulPAT([0,0], t2),
          mulPAT([1,0], t2),
          mulPAT([0,1], t2)
        ];
/*
        let p1 = mulPAT([-2.5,0], this.finalTransform);
        let p2 = mulPAT([ 2.5,0], this.finalTransform);
        let p3 = mulPAT([0,-2.5], this.finalTransform);
        let p4 = mulPAT([0,2.5], this.finalTransform);

        let xtick = [-2, -1.5, -1, -0.5, 0.5, 1, 1.5, 2];
        let ytick = [-2, -1.5, -1, -0.5, 0.5, 1, 1.5, 2];

        // Show axis:
        this.context.strokeStyle = "#aaaaaa";
        this.context.beginPath();
        this.context.moveTo(p1[0], p1[1]);
        this.context.lineTo(p2[0], p2[1]);
        this.context.moveTo(p3[0], p3[1]);
        this.context.lineTo(p4[0], p4[1]);
        this.context.stroke();

        this.context.font="12px Verdana";
        this.context.textAlign="center";
        this.context.textBaseline="top";
        this.context.fillStyle = "#aaaaaa";
        this.context.strokeStyle = "#aaaaaa";
        for(let i = 0; i < xtick.length; i++) {
          let p = mulPAT([xtick[i],0], this.finalTransform);
          this.context.beginPath();
          this.context.moveTo(p[0], p[1]-3);
          this.context.lineTo(p[0], p[1]+3);
          this.context.stroke();
          this.context.fillText(xtick[i], p[0],p[1]+3);
        }
        this.context.textAlign="right";
        this.context.textBaseline="middle";
        for(let i = 0; i < ytick.length; i++) {
          let p = mulPAT([0,ytick[i]], this.finalTransform);
          this.context.beginPath();
          this.context.moveTo(p[0]-3, p[1]);
          this.context.lineTo(p[0]+3, p[1]);
          this.context.stroke();
          this.context.fillText(ytick[i], p[0]-3,p[1]);
        }
*/
        // Show original vector:
        //this.context.lineWidth = 1;
        //this.context.strokeStyle = "#000000";
        //this.drawArrow(ox, oy, ox + this.vector[0] * ox * sx, oy + this.vector[1] * oy * sy);
        /*    this.context.beginPath();
            this.context.moveTo(ox, oy);
            this.context.lineTo(ox+this.vector[0]*ox*sx, oy+this.vector[1]*oy*sy);
            this.context.stroke();
        */
        // Show transformed vector:
        //this.context.lineWidth = 1;
        //this.context.strokeStyle = "#FF0000";
        //this.drawArrow(ox, oy, ox + this.transformedVector[0] * ox * sx, oy + this.transformedVector[1] * oy * sy);
        /*    this.context.beginPath();
            this.context.moveTo(ox, oy);
            this.context.lineTo(ox+this.transformedVector[0]*ox*sx, oy+this.transformedVector[1]*oy*sy);
            this.context.stroke();
        */
    }

    var me = this;
    me.trackMouse = false;

    function ispisi(tekst,m) {
     console.log(tekst+": "+m[0][0]+", "+m[0][1]+", "+m[1][0]+", "+m[1][1]+", "+m[2][0]+", "+m[2][1]);
    }

    this.canvas.addEventListener('mousemove', function (evt) {
        if (!me.trackMouse) return;
        var rect = me.canvas.getBoundingClientRect();
        var pos = {x: evt.clientX - rect.left, y: evt.clientY - rect.top};
        me.newPoint = [pos.x, pos.y];
        me.currentPoint = [pos.x,pos.y];
        me.origPoint[0] = me.origStartPoint[0] + me.currentPoint[0]-me.currentStartPoint[0];
        me.origPoint[1] = me.origStartPoint[1] + me.currentPoint[1]-me.currentStartPoint[1];
        //ispisi("Matrica", me.finalTransform);
        //ispisi("Inverz", affineInverse(me.finalTransform));

        let p = mulPAT(me.origPoint, affineInverse(me.finalTransform));
        if(me.idx==0) {
          me.ks2Matrix[2][0] = p[0];
          me.ks2Matrix[2][1] = p[1];
        } else if(me.idx==1) {
          me.ks2Matrix[0][0] = p[0]-me.ks2Matrix[2][0];
          me.ks2Matrix[0][1] = p[1]-me.ks2Matrix[2][1];
        } else if(me.idx==2) {
          me.ks2Matrix[1][0] = p[0]-me.ks2Matrix[2][0];
          me.ks2Matrix[1][1] = p[1]-me.ks2Matrix[2][1];
        } 
        setMatrixDoc(me.ks2Matrix);
        me.paint();
    });
    this.canvas.addEventListener('mousedown', function (evt) {
        var rect = me.canvas.getBoundingClientRect();
        var pos = {x: evt.clientX - rect.left, y: evt.clientY - rect.top};
        me.newPoint = [pos.x, pos.y];
        let idx = closestPoint(me.transformedPoints, me.newPoint);
        if(idx != -1) {
          me.trackMouse = true;
          me.origPoint = [me.transformedPoints[idx][0],me.transformedPoints[idx][1]];
          me.origStartPoint = [me.transformedPoints[idx][0],me.transformedPoints[idx][1]];
          me.currentPoint = [pos.x,pos.y];
          me.currentStartPoint = [pos.x,pos.y];
          me.idx = idx;
        }
        me.paint();
    });
    this.canvas.addEventListener('mouseup', function (evt) {
        me.trackMouse = false;
    });

    this.paint();
}

