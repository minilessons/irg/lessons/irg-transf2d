/**
 * Stream of characters.
 */
function InputStream(input) {
    let pos = 0;
    let line = 1;
    let col = 0;

    return {
        next: next,
        peek: peek,
        eof: eof,
        error: error
    };


    /**
     * Retrieves next character from input stream.
     *
     * @returns {string} character retrieved
     */
    function next() {
        let ch = input.charAt(pos++);
        if (ch === '\n') {
            ++line;
            col = 0;
        } else {
            ++col;
        }
        return ch;
    }


    /**
     * Looks for next character without skipping it.
     *
     * @returns {string} next character
     */
    function peek() {
        return input.charAt(pos);
    }


    /**
     * Checks whether next character is end of stream without skipping it.
     *
     * @returns {boolean} true if is
     */
    function eof() {
        return peek() === "";
    }


    /**
     * Throws error and writes error message to console output.
     *
     * @param m error message
     */
    function error(m) {
        let errorMessage = m + " (" + line + ":" + col + ")";
        throw new Error(colorCodeCommand(errorMessage));
    }

}
