function matproglexer(text) {
  this.chars = Array.from(text);
  this.token = {kind: null, value: null};
  this.pos = 0;
}

matproglexer.prototype.TT_EOF = "EOF";
matproglexer.prototype.TT_NAME = "NAME";
matproglexer.prototype.TT_SYMBOL = "SYMBOL";
matproglexer.prototype.TT_NUMBER = "NUMBER";

matproglexer.prototype.isDigit = function(c) {
  if(c>='0' && c<='9') return true;
  return false;
}

matproglexer.prototype.isIdentStart = function(c) {
  if(c>='A' && c<='Z') return true;
  if(c>='a' && c<='z') return true;
  return false;
}

matproglexer.prototype.isIdentPart = function(c) {
  if(c>='A' && c<='Z') return true;
  if(c>='a' && c<='z') return true;
  if(c>='0' && c<='9') return true;
  return c=='_';
}

matproglexer.prototype.next = function() {
  this.nexti();
  //document.write("Produced token: "+this.token.kind+": '"+this.token.value+"'<br>");
}
matproglexer.prototype.nexti = function() {
  if(this.token.kind==this.TT_EOF) return;
  while(true) {
    while(this.pos < this.chars.length && this.chars[this.pos] <= ' ') this.pos++;
    if(this.pos >= this.chars.length) {
      this.token.kind = this.TT_EOF;
      this.token.value = null;
      return;
    }
    if(this.chars[this.pos]!='#') break;
    this.pos++;
    while(this.pos < this.chars.length && this.chars[this.pos] != '\r' && this.chars[this.pos] != '\n') this.pos++;
  }
  if(this.isIdentStart(this.chars[this.pos])) {
    let id = [this.chars[this.pos]];
    this.pos++;
    while(this.pos < this.chars.length && this.isIdentPart(this.chars[this.pos])) {
      id.push(this.chars[this.pos]);
      this.pos++;
    }
    this.token.kind = this.TT_NAME;
    this.token.value = id.join("");
    return;
  }
  if(this.isDigit(this.chars[this.pos])) {
    let value = this.chars[this.pos].charCodeAt(0) - 48;
    this.pos++;
    while(this.pos < this.chars.length && this.isDigit(this.chars[this.pos])) {
      value = value*10 + (this.chars[this.pos].charCodeAt(0) - 48);
      this.pos++;
    }
    if(this.pos < this.chars.length && this.chars[this.pos]=='.') {
      this.pos++;
      let decValue = 0; let scaler = 10;      
      while(this.pos < this.chars.length && this.isDigit(this.chars[this.pos])) {
        decValue += (this.chars[this.pos].charCodeAt(0) - 48)/scaler;
        scaler *= 10;
        this.pos++;
      }
      value += decValue;
    }
    this.token.kind = this.TT_NUMBER;
    this.token.value = value;
    return;
  }
  this.token.kind = this.TT_SYMBOL;
  this.token.value = this.chars[this.pos];
  this.pos++;
}

function matprogparser(text) {
  this.lexer = new matproglexer(text);
  this.lexer.next();
  this.dom = this.parse();
}

function matprogparserMFunction(name,args) {
  this.name = name;
  this.args = args;
  this.acceptVisitor=function(v) {
    v.visitMatprogparserMFunction(this);
  }
}
function matprogparserMName(name) {
  this.name = name;
  this.acceptVisitor=function(v) {
    v.visitMatprogparserMName(this);
  }
}
function matprogparserMMatrix(data) {
  this.data = data;
  this.acceptVisitor=function(v) {
    v.visitMatprogparserMMatrix(this);
  }
}
function matprogparserMInverse(expr) {
  this.expr = expr;
  this.acceptVisitor=function(v) {
    v.visitMatprogparserMInverse(this);
  }
}
function matprogparserMTranspose(expr) {
  this.expr = expr;
  this.acceptVisitor=function(v) {
    v.visitMatprogparserMTranspose(this);
  }
}
function matprogparserMAdd(expr, expr2) {
  this.expr = expr;
  this.expr2 = expr2;
  this.acceptVisitor=function(v) {
    v.visitMatprogparserMAdd(this);
  }
}
function matprogparserMSub(expr, expr2) {
  this.expr = expr;
  this.expr2 = expr2;
  this.acceptVisitor=function(v) {
    v.visitMatprogparserMSub(this);
  }
}
function matprogparserMMul(expr, expr2) {
  this.expr = expr;
  this.expr2 = expr2;
  this.acceptVisitor=function(v) {
    v.visitMatprogparserMMul(this);
  }
}
function matprogparserStmt(name, expr) {
  this.expr = expr;
  this.name = name;
  this.acceptVisitor=function(v) {
    v.visitMatprogparserStmt(this);
  }
}
function matprogparserStmts(stmts) {
  this.statements = stmts;
  this.acceptVisitor=function(v) {
    v.visitMatprogparserStmts(this);
  }
}
function matprogparserKonst(n) {
  this.n = n;
  this.acceptVisitor=function(v) {
    v.visitMatprogparserKonst(this);
  }
}
function matprogparserFunction(name,arg) {
  this.name = name;
  this.arg = arg;
  this.acceptVisitor=function(v) {
    v.visitMatprogparserFunction(this);
  }
}
function matprogparserNegative(expr) {
  this.expr = expr;
  this.acceptVisitor=function(v) {
    v.visitMatprogparserNegative(this);
  }
}
function matprogparserPow(expr,pow) {
  this.expr = expr;
  this.pow = pow;
  this.acceptVisitor=function(v) {
    v.visitMatprogparserPow(this);
  }
}
function matprogparserAdd(expr,expr2) {
  this.expr = expr;
  this.expr2 = expr2;
  this.acceptVisitor=function(v) {
    v.visitMatprogparserAdd(this);
  }
}
function matprogparserSub(expr,expr2) {
  this.expr = expr;
  this.expr2 = expr2;
  this.acceptVisitor=function(v) {
    v.visitMatprogparserSub(this);
  }
}
function matprogparserMul(expr,expr2) {
  this.expr = expr;
  this.expr2 = expr2;
  this.acceptVisitor=function(v) {
    v.visitMatprogparserMul(this);
  }
}
function matprogparserDiv(expr,expr2) {
  this.expr = expr;
  this.expr2 = expr2;
  this.acceptVisitor=function(v) {
    v.visitMatprogparserDiv(this);
  }
}

matprogparser.prototype.parse = function() {
  let statements = [];
  while(true) {
    if(this.lexer.token.kind == this.lexer.TT_EOF) break;
    let statement = this.parseStmt();
    statements.push(statement);
  }
  return new matprogparserStmts(statements);
}

matprogparser.prototype.parseStmt = function() {
  let name = null;
  if(this.lexer.token.kind == this.lexer.TT_NAME) {
    name = this.lexer.token.value;
    this.lexer.next();
    if(this.lexer.token.kind != this.lexer.TT_SYMBOL || this.lexer.token.value!="=") throw "Expected =";
    this.lexer.next();
  }
  let e = this.parseMExpr();
  return new matprogparserStmt(name, e);
}

matprogparser.prototype.parseMExpr = function() {
  let current = this.parseMP2a();
  while(this.lexer.token.kind == this.lexer.TT_SYMBOL && (this.lexer.token.value=="+"||this.lexer.token.value=="-")) {
    let oper = this.lexer.token.value;
    this.lexer.next();
    let other = this.parseMP2a();
    current = oper=='+' ? new matprogparserMAdd(current, other) : new matprogparserMSub(current, other);
  }
  return current;
}

matprogparser.prototype.parseMP2a = function() {
  let current = this.parseMP2b();
  while(this.lexer.token.kind == this.lexer.TT_SYMBOL && this.lexer.token.value=="*") {
    let oper = this.lexer.token.value;
    this.lexer.next();
    let other = this.parseMP2b();
    current = new matprogparserMMul(current, other);
  }
  return current;
}

matprogparser.prototype.parseMP2b = function() {
  let current = this.parseMP3();
  while(this.lexer.token.kind == this.lexer.TT_SYMBOL && this.lexer.token.value==".") {
    this.lexer.next();
    if(this.lexer.token.kind != this.lexer.TT_NAME) throw "Expected NAME";
    let name = this.lexer.token.value;
    if(name!="inverse" && name!="transpose" && name!="inv" && name!="tran") throw "Expected inverse or transpose; got "+name+"!";
    this.lexer.next();
    current = (name=="inverse" || name=="inv") ? new matprogparserMInverse(current) : new matprogparserMTranspose(current);
  }
  return current;
}

matprogparser.prototype.parseMP3 = function() {
  if(this.lexer.token.kind == this.lexer.TT_NAME) {
    let name = this.lexer.token.value;
    this.lexer.next();
    if(this.lexer.token.kind == this.lexer.TT_SYMBOL && this.lexer.token.value=="(") {
      this.lexer.next();
      let args = [this.parseSExpr()];
      while(true) {
        if(this.lexer.token.kind != this.lexer.TT_SYMBOL || this.lexer.token.value!=",") break;
        this.lexer.next();
        args.push(this.parseSExpr());
      }
      if(this.lexer.token.kind != this.lexer.TT_SYMBOL || this.lexer.token.value!=")") throw "Očekivao sam ')' nakon argumenata funkcije "+name+".";
      this.lexer.next();
      return new matprogparserMFunction(name,args);
    }
    return new matprogparserMName(name);
  }
  if(this.lexer.token.kind == this.lexer.TT_SYMBOL && this.lexer.token.value=="(") {
    this.lexer.next();
    let exp = this.parseMExpr();
    if(this.lexer.token.kind != this.lexer.TT_SYMBOL || this.lexer.token.value!=")") throw "Expected ')'.";
    this.lexer.next();
    return exp;
  }
  if(this.lexer.token.kind == this.lexer.TT_SYMBOL && this.lexer.token.value=="[") {
    this.lexer.next();
    let rows = [this.parseMRow()];
    let clen = -1;
    while(this.lexer.token.kind == this.lexer.TT_SYMBOL && this.lexer.token.value=="|") {
      this.lexer.next();
      let crow = this.parseMRow();
      rows.push(crow);
      if(clen==-1) {
        clen = crow.length;
      } else if(clen != crow.length) {
        throw "Pronađena definicija matrice gdje retci imaju različite brojeve stupaca.";
      }
    }
    if(this.lexer.token.kind != this.lexer.TT_SYMBOL || this.lexer.token.value!="]") throw "Expected ']'.";
    this.lexer.next();
    return new matprogparserMMatrix(rows);
  }
  throw "Neočekivani token: "+this.lexer.token.kind+": '"+this.lexer.token.value+"'";
}

matprogparser.prototype.parseMRow = function() {
  let column = [this.parseSExpr()];
  while(this.lexer.token.kind == this.lexer.TT_SYMBOL && this.lexer.token.value==",") {
    this.lexer.next();
    column.push(this.parseSExpr());
  }
  return column;
}

matprogparser.prototype.parseSExpr = function() {
  let current = this.parseSE2();
  while(this.lexer.token.kind == this.lexer.TT_SYMBOL && (this.lexer.token.value=="+"||this.lexer.token.value=="-")) {
    let oper = this.lexer.token.value;
    this.lexer.next();
    let other = this.parseSE2();
    current = oper=='+' ? new matprogparserAdd(current, other) : new matprogparserSub(current, other);
  }
  return current;
}

matprogparser.prototype.parseSE2 = function() {
  let current = this.parseSE3();
  while(this.lexer.token.kind == this.lexer.TT_SYMBOL && (this.lexer.token.value=="*"||this.lexer.token.value=="/")) {
    let oper = this.lexer.token.value;
    this.lexer.next();
    let other = this.parseSE3();
    current = oper=='*' ? new matprogparserMul(current, other) : new matprogparserDiv(current, other);
  }
  return current;
}

matprogparser.prototype.parseSE3 = function() {
  let current = this.parseSE4();
  while(this.lexer.token.kind == this.lexer.TT_SYMBOL && this.lexer.token.value=="^") {
    this.lexer.next();
    let other = this.parseSE4();
    current = new matprogparserPow(current, other);
  }
  return current;
}

matprogparser.prototype.parseSE4 = function() {
  let minus = false;
  if(this.lexer.token.kind == this.lexer.TT_SYMBOL && this.lexer.token.value=="-") {
    minus = true;
    this.lexer.next();
  }
  let current = this.parseSE5();
  return minus ? new matprogparserNegative(current) : current;
}

matprogparser.prototype.parseSE5 = function() {
  if(this.lexer.token.kind == this.lexer.TT_NUMBER) {
    let n = this.lexer.token.value;
    this.lexer.next();
    return new matprogparserKonst(n);
  }
  if(this.lexer.token.kind == this.lexer.TT_NAME) {
    let n = this.lexer.token.value;
    this.lexer.next();
    if(this.lexer.token.kind != this.lexer.TT_SYMBOL || this.lexer.token.value!="(") throw "Expected (.";
    this.lexer.next();
    let arg = this.parseSExpr();
    if(this.lexer.token.kind != this.lexer.TT_SYMBOL || this.lexer.token.value!=")") throw "Expected ).";
    this.lexer.next();
    return new matprogparserFunction(n, arg);
  }
  if(this.lexer.token.kind == this.lexer.TT_SYMBOL && this.lexer.token.value=="(") {
    this.lexer.next();
    let expr = this.parseSExpr();
    if(this.lexer.token.kind != this.lexer.TT_SYMBOL || this.lexer.token.value!=")") throw "Expected ).";
    this.lexer.next();
    return expr;
  }
  throw "Neočekivani token: "+this.lexer.token.kind+": '"+this.lexer.token.value+"'";
}

function matrixprogramEvalVisitor() {
  this.stack = [];
  this.variables = {}; // mapa ime => {vvalue: _, vtype: 'M'/'S', vdims: [_,_,...]}
  this.names = new Set();
  this.anonNamesCounter = 0;

  this.getValue = function(name) {
    let v = this.variables['vv'+name];
    if(v===undefined) throw "Varijabla "+name+" nije definirana.";
    return v;
  }

  this.visitMatprogparserMFunction = function(node) { // name, args
    let sarr = [];
    for(let i = 0; i < node.args.length; i++) {
      node.args[i].acceptVisitor(this);
      let v = this.stack.pop();
      if(v.vtype != 'S') throw "Invalid value type for scalar function argument; expected scalar.";
      sarr.push(v.vvalue);
    }
    if(node.name=="translate2d") {
      if(sarr.length!=2) throw "translate2d takes 2 arguments; "+sarr.length+" were given.";
      let m = new matrix([[1,0,0],[0,1,0],[sarr[0],sarr[1],1]]);
      this.stack.push({vvalue: m, vtype: 'M', vdims: [m.rows, m.cols]});
      return;
    }
    if(node.name=="scale2d") {
      if(sarr.length!=2) throw "scale2d takes 2 arguments; "+sarr.length+" were given.";
      let m = new matrix([[sarr[0],0,0],[0,sarr[1],0],[0,0,1]]);
      this.stack.push({vvalue: m, vtype: 'M', vdims: [m.rows, m.cols]});
      return;
    }
    if(node.name=="rotate2d") {
      if(sarr.length!=1) throw "rotate2d takes 1 argument; "+sarr.length+" were given.";
      let r = sarr[0]/180*Math.PI;
      let m = new matrix([[Math.cos(r),Math.sin(r),0],[-Math.sin(r),Math.cos(r),0],[0,0,1]]);
      this.stack.push({vvalue: m, vtype: 'M', vdims: [m.rows, m.cols]});
      return;
    }
    if(node.name=="translate3d" || node.name=="translate") {
      if(sarr.length!=3) throw "translate3d takes 3 arguments; "+sarr.length+" were given.";
      let m = new matrix([[1,0,0,0],[0,1,0,0],[0,0,1,0],[sarr[0],sarr[1],sarr[2],1]]);
      this.stack.push({vvalue: m, vtype: 'M', vdims: [m.rows, m.cols]});
      return;
    }
    if(node.name=="scale3d" || node.name=="scale") {
      if(sarr.length!=3) throw "scale3d takes 3 arguments; "+sarr.length+" were given.";
      let m = new matrix([[sarr[0],0,0,0],[0,sarr[1],0,0],[0,0,sarr[2],0],[0,0,0,1]]);
      this.stack.push({vvalue: m, vtype: 'M', vdims: [m.rows, m.cols]});
      return;
    }
    if(node.name=="rotatex") {
      if(sarr.length!=1) throw "rotatex takes 1 argument; "+sarr.length+" were given.";
      let r = sarr[0]/180*Math.PI;
      let m = new matrix([[1,0,0,0],[0,Math.cos(r),Math.sin(r),0],[0,-Math.sin(r),Math.cos(r),0],[0,0,0,1]]);
      this.stack.push({vvalue: m, vtype: 'M', vdims: [m.rows, m.cols]});
      return;
    }
    if(node.name=="rotatey") {
      if(sarr.length!=1) throw "rotatey takes 1 argument; "+sarr.length+" were given.";
      let r = sarr[0]/180*Math.PI;
      let m = new matrix([[Math.cos(r),0,-Math.sin(r),0],[0,1,0,0],[Math.sin(r),0,Math.cos(r),0],[0,0,0,1]]);
      this.stack.push({vvalue: m, vtype: 'M', vdims: [m.rows, m.cols]});
      return;
    }
    if(node.name=="rotatez") {
      if(sarr.length!=1) throw "rotatez takes 1 argument; "+sarr.length+" were given.";
      let r = sarr[0]/180*Math.PI;
      let m = new matrix([[Math.cos(r),Math.sin(r),0,0],[-Math.sin(r),Math.cos(r),0,0],[0,0,1,0],[0,0,0,1]]);
      this.stack.push({vvalue: m, vtype: 'M', vdims: [m.rows, m.cols]});
      return;
    }
    throw "Unknown function: "+node.name+".";
  };
  this.visitMatprogparserMName = function(node) { // name
    let v = this.variables['vv'+node.name];
    if(v===undefined) throw "Varijabla "+node.name+" nije definirana.";
    this.stack.push(v);
  };
  this.visitMatprogparserMMatrix = function(node) { // data
    let data = node.data;
    let arr = [];
    for(let r = 0; r < data.length; r++) {
      let row = [];
      for(let c = 0; c < data[r].length; c++) {
        data[r][c].acceptVisitor(this);
        let v1 = this.stack.pop();
        if(v1.vtype != 'S') throw "Invalid value type for matrix definition; expected scalar.";
        row.push(v1.vvalue);
      }
      arr.push(row);
    }
    let m = new matrix(arr);
    this.stack.push({vvalue: m, vtype: 'M', vdims: [m.rows, m.cols]});
  };
  this.visitMatprogparserMInverse = function(node) {  // expr
    node.expr.acceptVisitor(this);
    let v = this.stack.pop();
    if(v.vtype != 'M') throw "Can not invert non-matrix type.";
    if(v.vdims[0]!=v.vdims[1]) throw "Can not invert matrix of dimensions "+v.vdims[0]+"x"+v.vdims[1]+".";
    let m = v.vvalue.inverse();
    this.stack.push({vvalue: m, vtype: 'M', vdims: [m.rows, m.cols]});
  };
  this.visitMatprogparserMTranspose = function(node) { // expr
    node.expr.acceptVisitor(this);
    let v = this.stack.pop();
    if(v.vtype != 'M') throw "Can not transpose non-matrix type.";
    let m = v.vvalue.transpose();
    this.stack.push({vvalue: m, vtype: 'M', vdims: [m.rows, m.cols]});
  };
  this.visitMatprogparserMAdd = function(node) { // expr, expr2
    node.expr.acceptVisitor(this);
    let v1 = this.stack.pop();
    node.expr2.acceptVisitor(this);
    let v2 = this.stack.pop();
    if(v1.vtype != 'M' || v2.vtype != 'M') throw "One of arguments of matrix addition is not matrix.";
    if(v1.vdims[0] != v2.vdims[0] || v1.vdims[1] != v2.vdims[1]) throw "Incompatible matrix addition: "+v1.vdims[0]+"x"+v1.vdims[1]+" + "+v2.vdims[0]+"x"+v2.vdims[1]+".";
    let m1 = v1.vvalue;
    let m2 = v2.vvalue;
    let m = m1.add(m2);
    this.stack.push({vvalue: m, vtype: 'M', vdims: [m.rows, m.cols]});
  };
  this.visitMatprogparserMSub = function(node) { // expr, expr2
    node.expr.acceptVisitor(this);
    let v1 = this.stack.pop();
    node.expr2.acceptVisitor(this);
    let v2 = this.stack.pop();
    if(v1.vtype != 'M' || v2.vtype != 'M') throw "One of arguments of matrix subtraction is not matrix.";
    if(v1.vdims[0] != v2.vdims[0] || v1.vdims[1] != v2.vdims[1]) throw "Incompatible matrix subtraction: "+v1.vdims[0]+"x"+v1.vdims[1]+" - "+v2.vdims[0]+"x"+v2.vdims[1]+".";
    let m1 = v1.vvalue;
    let m2 = v2.vvalue;
    let m = m1.sub(m2);
    this.stack.push({vvalue: m, vtype: 'M', vdims: [m.rows, m.cols]});
  };
  this.visitMatprogparserMMul = function(node) { // expr, expr2
    node.expr.acceptVisitor(this);
    let v1 = this.stack.pop();
    node.expr2.acceptVisitor(this);
    let v2 = this.stack.pop();
    if(v1.vtype != 'M' || v2.vtype != 'M') throw "One of arguments of matrix multiplication is not matrix.";
    if(v1.vdims[1] != v2.vdims[0]) throw "Incompatible matrix multiplication: "+v1.vdims[0]+"x"+v1.vdims[1]+" + "+v2.vdims[0]+"x"+v2.vdims[1]+".";
    let m1 = v1.vvalue;
    let m2 = v2.vvalue;
    let m = m1.mul(m2);
    this.stack.push({vvalue: m, vtype: 'M', vdims: [m.rows, m.cols]});
  };
  this.visitMatprogparserStmt = function(node) { // name, expr
    node.expr.acceptVisitor(this);
    let v = this.stack.pop();
    let ename = node.name==null ? "?anon" + (++this.anonNamesCounter) : node.name;
    this.variables['vv'+ename] = v;
    this.names.add(ename);
  };
  this.visitMatprogparserStmts = function(node) { // statements
    let arr = node.statements;
    let arrl = arr.length;
    for(let i = 0; i < arrl; i++) {
      arr[i].acceptVisitor(this);
    }
  };
  this.visitMatprogparserKonst = function(node) { // n
    let s = node.n;
    this.stack.push({vvalue: s, vtype: 'S', vdims: null});
  };
  this.visitMatprogparserFunction = function(node) { // name, arg
    node.arg.acceptVisitor(this);
    let v = this.stack.pop();
    if(v.vtype != 'S') throw "Invalid value type for function "+node.name+"; expected scalar.";
    if(node.name=="sin") {
      this.stack.push({vvalue: Math.sin(v.vvalue/180*Math.PI), vtype: 'S', vdims: null});
      return;
    }
    if(node.name=="cos") {
      this.stack.push({vvalue: Math.cos(v.vvalue/180*Math.PI), vtype: 'S', vdims: null});
      return;
    }
    if(node.name=="tan") {
      this.stack.push({vvalue: Math.tan(v.vvalue/180*Math.PI), vtype: 'S', vdims: null});
      return;
    }
    if(node.name=="sqrt") {
      this.stack.push({vvalue: Math.sqrt(v.vvalue), vtype: 'S', vdims: null});
      return;
    }
    if(node.name=="torad") {
      this.stack.push({vvalue: v.vvalue/180*Math.PI, vtype: 'S', vdims: null});
      return;
    }
    if(node.name=="todeg") {
      this.stack.push({vvalue: v.vvalue/Math.PI*180, vtype: 'S', vdims: null});
      return;
    }
    throw "Unknown function: "+node.name;
  };
  this.visitMatprogparserNegative = function(node) { // expr
    node.expr.acceptVisitor(this);
    let v = this.stack.pop();
    if(v.vtype != 'S') throw "Invalid value type for scalar negation; expected scalar.";
    this.stack.push({vvalue: -v.vvalue, vtype: 'S', vdims: null});
  };
  this.visitMatprogparserPow = function(node) { // expr, pow
    node.expr.acceptVisitor(this);
    let v = this.stack.pop();
    if(v.vtype != 'S') throw "Invalid value type for scalar power base; expected scalar.";
    node.acceptVisitor(node.pow);
    let e = this.stack.pop();
    if(e.vtype != 'S') throw "Invalid value type for scalar power exponent; expected scalar.";
    this.stack.push({vvalue: Math.pow(v.vvalue,e.vvalue), vtype: 'S', vdims: null});
  };
  this.visitMatprogparserAdd = function(node) { // expr, expr2
    node.expr.acceptVisitor(this);
    let v1 = this.stack.pop();
    if(v1.vtype != 'S') throw "Invalid value type for scalar addition; expected scalar.";
    node.expr2.acceptVisitor(this);
    let v2 = this.stack.pop();
    if(v2.vtype != 'S') throw "Invalid value type for scalar addition; expected scalar.";
    this.stack.push({vvalue: v1.vvalue+v2.vvalue, vtype: 'S', vdims: null});
  };
  this.visitMatprogparserSub = function(node) { // expr, expr2
    node.expr.acceptVisitor(this);
    let v1 = this.stack.pop();
    if(v1.vtype != 'S') throw "Invalid value type for scalar subtraction; expected scalar.";
    node.expr2.acceptVisitor(this);
    let v2 = this.stack.pop();
    if(v2.vtype != 'S') throw "Invalid value type for scalar subtraction; expected scalar.";
    this.stack.push({vvalue: v1.vvalue-v2.vvalue, vtype: 'S', vdims: null});
  };
  this.visitMatprogparserMul = function(node) { // expr, expr2
    node.expr.acceptVisitor(this);
    let v1 = this.stack.pop();
    if(v1.vtype != 'S') throw "Invalid value type for scalar multiplication; expected scalar.";
    node.expr2.acceptVisitor(this);
    let v2 = this.stack.pop();
    if(v2.vtype != 'S') throw "Invalid value type for scalar multiplication; expected scalar.";
    this.stack.push({vvalue: v1.vvalue*v2.vvalue, vtype: 'S', vdims: null});
  };
  this.visitMatprogparserDiv = function(node) { // expr, expr2
    node.expr.acceptVisitor(this);
    let v1 = this.stack.pop();
    if(v1.vtype != 'S') throw "Invalid value type for scalar division; expected scalar.";
    node.expr2.acceptVisitor(this);
    let v2 = this.stack.pop();
    if(v2.vtype != 'S') throw "Invalid value type for scalar division; expected scalar.";
    this.stack.push({vvalue: v1.vvalue/v2.vvalue, vtype: 'S', vdims: null});
  };
}

