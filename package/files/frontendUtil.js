/**
 * Helper function for calculating grid origin.
 * @param can canvas to which origin is relative to
 * @param step gap between grid lines
 * @returns {{x: number, y: number}} point x, y located at 0, 0 in local grid system.
 */
function calcOrigin(can, step) {
    let xCoord = Math.floor(step * Math.floor((Math.floor((can.width / step))) / 2));
    let yCoord = Math.floor(step * Math.floor((Math.floor((can.height / step))) / 2));
    return {x: xCoord, y: yCoord}
}


/**
 * Sets html canvas properties from constants.
 *
 * @param canvas canvas for which properties are set
 * @param constants constants
 */
function setCanvasProperties(canvas, constants) {
    canvas.width = constants.getNumeric("canvasWUnits");
    canvas.height = constants.getNumeric("canvasHUnits");
}


/**
 * Checks whether provided value is power of two.
 *
 * @param value number to check
 * @returns {boolean} true if is power of two
 */
function isPowerOf2(value) {
    return (value & (value - 1)) === 0;
}


/**
 * Transforms hex number (base 16) to decimal (base 10).
 *
 * @param hexNumber hex number to convert to decimal
 * @returns {Number} converted number
 */
function hexToDecimal(hexNumber) {
    return parseInt(hexNumber, 16);
}


/**
 * Function constructor for RGBA color values.
 *
 * @param red amount of red
 * @param green amount of green
 * @param blue amount of blue
 * @param alpha amount of transparency
 * @constructor
 */
function RGBA(red, green, blue, alpha) {
    this.r = red;
    this.g = green;
    this.b = blue;
    this.a = alpha;

    this.cssString = function () {
        return "rgba(" + this.r + "," + this.g + "," + this.b + "," + this.a + ")"
    };
    this.normalize = function () {
        let maxC = 255;
        return new RGBA(this.r / maxC, this.g / maxC, this.b / maxC, this.a);
    };

    RGBA.prototype.asArray = function (color) {
        return [color.r, color.g, color.b, color.a];
    };

    this.asArray = function () {
        return RGBA.prototype.asArray(this);
    };

    this.asNormalizedArray = function () {
        return RGBA.prototype.asArray(this.normalize());
    }
}


/**
 * Converts hex color to rgba.
 *
 * @param hexColor color to convert
 * @param alpha alpha value for new rgba color
 * @returns {RGBA} rgba representation of hex color
 */
function convertHexToRGBA(hexColor, alpha) {
    let a = (alpha === undefined) ? 1 : alpha;
    let red = hexToDecimal(hexColor.slice(1, 3));
    let green = hexToDecimal(hexColor.slice(3, 5));
    let blue = hexToDecimal(hexColor.slice(5, 7));
    return new RGBA(red, green, blue, a);
}


/**
 * Creates unique ID of with a specified number.
 *
 * @param helpNum number to create ID of
 * @returns {string} unique ID
 */
function createUID(helpNum) {
    return (Math.random() * 0x10000 * 42 * 31 + (helpNum || 8)).toString(16).slice(2, 6);
}


function getGLColorFromHex(c) {
    return convertHexToRGBA(c).asNormalizedArray();
}


/**
 * Converts radians to degrees.
 * @param r angle in radians
 * @returns {number} angle in degrees
 */
function radToDeg(r) {
    return r * 180 / Math.PI;
}


/**
 * Converts degrees to radians.
 * @param d degree
 * @returns {number} angle in radians
 */
function degToRad(d) {
    return d * Math.PI / 180;
}


function closeToZero(value, tolerance) {
    return Math.abs(value) < tolerance;
}


/**
 * Toggles element specified by html ID.
 *
 * @param id id of element to toggle
 */
function toggleElement(id) {
    let elem = document.getElementById(id);
    if (elem.style.display === "none") {
        showDivByID(id);
    } else {
        closeDivByID(id);
    }
}


/**
 * Shows div tag element with specified identification.
 *
 * @param ID id of an element to show
 */
function showDivByID(ID) {
    document.getElementById(ID).style.display = "block";
}


/**
 * Hides div tag element with specified identification.
 *
 * @param ID id of an element to hide
 */
function closeDivByID(ID) {
    document.getElementById(ID).style.display = "none";
}


/**
 * Sets focus for element specified by identification.
 *
 * @param ID id of an element to set focus to
 */
function setFocusById(ID) {
    document.getElementById(ID).focus();
}