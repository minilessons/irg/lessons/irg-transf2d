/**
 * Creates string representation of a given matrix.
 * @param m matrix to transform to set representation to
 * @param always if true calculates new string representation
 */
function createStringRepresentation(m, always) {
    if (!m.stringRepresentation || always) {
        m.stringRepresentation = getMatrixString(m);
    }
}


/**
 * Prints 3x3 matrix to console in few rows.
 *
 * @param matrix matrix to print
 */
function printMatrixToConsole(matrix) {
    createStringRepresentation(matrix, true);
    matrix.stringRepresentation.forEach((row) => {
        setToConsole(row);
    });
}


/**
 * Prints whole stack to console or logs empty stack.
 *
 * @param env environment which holds matrix stack
 */
function printStackToConsole(env) {

    let stack = env.get("matrixStack");

    if (stack.length === 0) {
        setToConsole(getLogMessage("Cannot print empty stack."));
        return;
    }

    let consoleOut = getConsoleEasy();
    let parentMatrix = document.createElement("div");
    parentMatrix.className = "matrixParent";

    stack.forEach((m, idx) => {
        if (m instanceof $m3) {
            let newElem = document.createElement("div");
            newElem.className = "stackPrefix";
            newElem.innerHTML = (colorCodeCommand("Stack index") + ": " + idx);
            parentMatrix.appendChild(newElem);

            createStringRepresentation(m, true);
            m.stringRepresentation.forEach((str) => addLineToParent(str, parentMatrix));
        }
    });

    setToConsole(colorCodeCommand("Stack: "));
    let emptyLine = findSpaceInConsole(consoleOut);
    consoleOut.insertBefore(parentMatrix, emptyLine);
    consoleOut.scrollTop = consoleOut.scrollHeight;

}


/**
 * Helper function to execute printing points to console.
 *
 * @param env environment which holds needed state with defined points
 */
function printPointsToConsole(env) {
    let state = getStateEasy();
    let listOfPoints = state.listOfPoints;

    if (listOfPoints.length === 0) {
        sendToTabConsole(getConsoleEasy(), getLogMessage("No points to print."));
    }

    listOfPoints.forEach((p) => {
        printToConsole(SimplePoint.toString(p));
    });
}


/**
 * Removes a point if it is found in list of points.
 *
 * @param id id of a point to remove
 * @param env environment which holds state and list of points
 * @param removeAll specifies if function should remove all points at once
 */
function removePoint(id, env, removeAll) {
    if (removeAll) {
        getStateEasy().objects.points = [];
        setToConsole(getLogMessage("Successfully removed all points."));
        return;
    }

    let u = new ParserUtil();
    let drawablePoints = getStateEasy().objects.points;
    let listOfP = getStateEasy().listOfPoints;

    let numId = u.num(id);
    let size = listOfP.length;
    for (let c = 0; c < size; c++) {
        let p = listOfP[c];
        if (p.id === numId) {
            drawablePoints.splice(c, 1);
            listOfP.splice(c, 1);
            setToConsole(getLogMessage("Removed point with id: " + p.id));
            return;
        }
    }

    throw new Error("There is no point with ID: " + numId);
}


/**
 * Creates new point from list of parameters and adds it to list of point in environment.
 *
 * @param list list of parameters for creating SimplePoint
 * @param env environment which holds state and list of points
 */
function addNewPoint(list, env) {
    let objects = getStateEasy().objects;
    let listOfP = getStateEasy().listOfPoints;

    let pointID = getStateEasy().getNextPointIndex();
    let type = list[3] || "circle";

    if (list[0] > 99 || list[1] > 99 || list[0] < -99 || list[1] < -99) {
        throw new Error("Please keep x and y coordinates inside range: [-99, 99].");
    }

    let newPoint = new SimplePoint(pointID, list[0], list[1], list[2], type);
    objects.addPoint(newPoint);
    listOfP.push(newPoint);

    setToConsole(getLogMessage("Added point with ID: " + pointID));
}


/**
 * Helper function to execute printing to console.
 *
 * @param value value to color code and set to console
 * @param env environment for printing stack
 */
function printToConsole(value, env) {
    if (Array.isArray(value)) {
        if (value.length > 0) {
            // Print stack
            printStackToConsole(env);
        } else {
            setToConsole(getLogMessage("Cannot print empty stack."));
        }

        return;
    }

    let coloredPrint = colorCodeCommand("print");

    // Print usual types::
    if (value instanceof $m3) {
        setToConsole(coloredPrint + ": ");
        printMatrixToConsole(value);
        return;
    }

    let colored = colorCodeConstant(value);


    setToConsole(coloredPrint + ": " + colored);
}


/**
 * Helper function to execute command: pop, which removes first element from the stack.
 *
 * @param env environment which holds the stack
 */
function popFromStack(env) {

    let stack = env.get("matrixStack");

    if (stack.length <= 0) {
        setToConsole(getLogMessage("Nothing to pop. Empty stack."));
        return;
    }

    let newStack = stack.slice(1);
    env.set("matrixStack", newStack);

    let state = getStateEasy();

    state.matrixStack = state.matrixStack.slice(1);

    let msg = "Successfully removed matrix.";
    setToConsole(msg);

}


/**
 * Helper function to execute command: "<=", pushing active matrix to stack.
 * @param env environment with needed variables (stack)
 * @param matrix matrix to push to stack
 */
function pushToStack(env, matrix) {
    let stack = env.get("matrixStack");

    createStringRepresentation(matrix, true);
    env.set("matrixStack", [matrix].concat(stack));

    let state = getStateEasy();
    state.matrixStack = [matrix].concat(state.matrixStack);
}


/**
 * Helper function to set [LOG] messages.
 *
 * @param msg message to print within [LOG] delimiters.
 * @returns string span element with message
 */
function getLogMessage(msg) {
    let logColor = getStateEasy().constants.getColor("logColor");
    let logTextColor = getStateEasy().constants.getColor("logTextColor");
    let elem = createSpanWithColor("[LOG] ", logColor);
    let textElem = createSpanWithColor(msg, logTextColor);
    elem.appendChild(textElem);
    elem.innerHTML += " [LOG]";

    return elem.outerHTML;
}


/**
 * Helper function to execute command: "clear", by clearing matrix stack on given environment.
 *
 * @param env environment which holds stack
 */
function clearStack(env) {
    env.set("matrixStack", []);
    getStateEasy().matrixStack = [];

    setToConsole(getLogMessage("Stack cleared"));
}


/**
 * Helper function to send message to console in color coding.
 *
 * @param message message to color code and set to console
 */
function sendMessageToConsole(message) {
    let colored = colorCodeCommand(message);
    setToConsole(colored);
}


function updateAllVars(env) {
    Object.keys(env.vars).forEach((name) => {
        let varValue = env.get(name);
        updateEnvironmentConsole(name, varValue);
    });
}


/**
 * Updates value of a variable in environment console.
 * @param name name of variable to update
 * @param value new value to set
 */
function updateEnvironmentConsole(name, value) {
    let id = name + getStateEasy().constants.getString("uidPostfix");
    let domValueElem = document.getElementById(id);

    let createdParent = getResolvedParent(value, name);
    let envConsole = getTabEasy("environmentVars");
    if (!domValueElem) {
        let idxName = "expandedValueIdx";

        let numOfDefinedVars = globalEnv.get(idxName);
        numOfDefinedVars += 1;

        createNewEnvironmentVar(envConsole, numOfDefinedVars, idxName, name);

        return;
    }

    createdParent.style.display = domValueElem.style.display;
    domValueElem.outerHTML = createdParent.outerHTML;

    // Animate for visual effect.
    let elm = document.getElementById(envConsole.id + "Header");
    let newElem = elm.cloneNode(true);
    elm.parentNode.replaceChild(newElem, elm);

}


/**
 * Checks for any empty space in given tab and returns its node.
 * Adds new empty lines if there is no space left.
 *
 * @param c element to check for child nodes for space
 * @returns {*}
 */
function findSpaceInConsole(c) {
    return getTabCompEasy().getEmptySpace(c);
}


function sendToTabConsole(theConsole, message) {
    getTabCompEasy().setMessageToTab(theConsole, message);
}


/**
 * Helper function to set message to console.
 *
 * @param message message to set
 */
function setToConsole(message) {
    let consoleOut = document.getElementById("console");
    sendToTabConsole(consoleOut, message);
}


/**
 * Helper function which prefixes message with: "Error: " and sends it all to console.
 *
 * @param msg message to set to console
 */
function setErrorToConsole(msg) {
    let u = new ParserUtil();
    setToConsole(u.colorCodeError() + msg);
}


/**
 * Helper function to execute command: "cls", by clearing both consoles.
 */
function clearConsole() {
    let consoleOut = document.getElementById("console");
    let historyOut = document.getElementById("history");
    consoleOut.innerHTML = "";
    historyOut.innerHTML = "";
    addBlankRows("console", 10);
    addBlankRows("history", 10);

    userHistory.clear();

    // Reset index for accessing empty lines
    let tabComp = getTabCompEasy();
    tabComp.resetIndex(consoleOut);
    tabComp.resetIndex(historyOut);

    setToConsole(getLogMessage("Cleared console"));
}


