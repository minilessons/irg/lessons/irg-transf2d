function init() {
    let graphics = initCanvas("commonRasterCanvas");
    let letterF = new LetterFShape("LetterF" + createUID(112), 0, 0);
    graphics.addShape(letterF);

    setUIGadgets(graphics, letterF);
    graphics.update();
}


function setUIGadgets(state, obj) {
    let upd = new UpdateFunctions(obj, state);

    let scaleXId = "scaleXValue";
    let scaleYId = "scaleYValue";

    let scaleOptsX = {
        uiPrecision: 2,
        min: -9,
        max: 9,
        step: 1,
        /* name: "scale x: ", Inherited from parent HTML*/
        value: 1,
        functionCallback: upd.updateScale(0)
    };

    let scaleOptsY = {
        uiPrecision: 2,
        min: -3,
        max: 3,
        step: 1,
        /* name: "scale y: ", Inherited from parent HTML*/
        value: 1,
        functionCallback: upd.updateScale(1),
        sliderTickmarks: "scale_tickmarks"
    };

    state.sliderSx = setupSlider(scaleXId, scaleOptsX);
    state.sliderSy = setupSlider(scaleYId, scaleOptsY);

}


function UpdateFunctions(obj, state) {
    function updateScale(index) {
        return function (e, ui) {
            let arr = obj.transform.getElements();
            let horzUnitScaler = 1;// / obj.horzUnits;
            let vertUnitScale = 1;// / obj.vertUnits;
            arr[index][index] = (index === 0) ? ui.value * horzUnitScaler : ui.value * vertUnitScale;

            setTransformToUIScaling(arr[index][index], index);
            state.update();
        };
    }


    return {
        updateScale: updateScale
    };

}


function setTransformToUIScaling(stringFraction, index) {
    let matrixElement = document.getElementById("m" + (index + "") + (index + ""));
    matrixElement.innerHTML = stringFraction;
}


init();