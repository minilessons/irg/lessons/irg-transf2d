let constants = getConstants();
let globalEnv = new Environment();


globalEnv.set = function (name, value) {
    let scope = this.lookup(name);
    if (!scope && this.parent) {
        let error = "Undefined variable " + name;
        throw new Error(error);
    }

    return (scope || this).vars[name] = value;
};

let globalUserEnv = new Environment();

// Set global stack
globalUserEnv.def("matrixStack", []);

// Tracing matrix stack
globalUserEnv.def("traceStack", true);

// Tracing points
globalUserEnv.def("tracePoints", true);

globalUserEnv.def("printPrecision", 3);

// Default fill and outline colors
globalUserEnv.def("defaultOutlineColor", "#0ade2c");
globalUserEnv.def("defaultFillColor", "#d03664");

/*globalUserEnv.def("rotateStep", 45);
globalUserEnv.def("scaleXStep", 0.1);
globalUserEnv.def("scaleYStep", 0.1);*/

globalEnv.def("expandedValueIdx", 0);


// Set matrix to be global variable last because it uses print precision which must be defined above.
let globalMatrixM = $m3.identity();
createStringRepresentation(globalMatrixM, true);
globalUserEnv.def("matrix", globalMatrixM);


/**
 * Helper function for constructing and manipulating tab components.
 * @returns {{addTab: addTab, showTabById: showTabById, getActiveTab: getActiveTab, getTabHeader: getTabHeader, getEmptySpace: getEmptySpace, resetIndexes: resetIndexes, setMessageToTab: setMessageToTab, resetIndex: resetIndex}}
 * @constructor
 */
function TabbedComponent() {
    let tabs = [];
    let tabsObj = {};
    let activeTab = null;


    function setMessageToTab(comp, msg) {
        let emptyLine = getEmptySpace(comp);

        emptyLine.innerHTML = (msg);
        comp.scrollTop = comp.scrollHeight;

        // Animate for visual effect.
        let elm = document.getElementById(comp.id + "Header");
        let newElem = elm.cloneNode(true);
        elm.parentNode.replaceChild(newElem, elm);

    }


    function resetIndex(comp) {
        tabsObj[comp.id].emptySpaceIdx = 0;
    }


    function resetIndexes() {
        Object.keys(tabsObj).forEach((key) => {
            tabsObj[key].emptySpaceIdx = 0;
        });
    }


    function getEmptySpace(component) {
        let tab = tabsObj[component.id];
        let nodes = component.childNodes;
        let idx = tab.emptySpaceIdx;

        let retVal;
        if (idx < nodes.length) {
            retVal = nodes[idx];
        } else {
            // No empty lines, add new spaces
            let numOfSpacesToAdd = getStateEasy().constants.getNumeric("expandTextAreaRows");
            if (component.id === "history") {
                userHistory.addElements(numOfSpacesToAdd);
            } else {
                addBlankRows(component.id, numOfSpacesToAdd);
            }

            retVal = component.childNodes[idx];
        }

        tab.emptySpaceIdx += 1;
        return retVal;
    }


    function addTab(component) {
        if (!component) {
            throw new Error("Undefined tab component for component:" + component);
        }

        // Set active tab to first tab given.
        if (activeTab === null) {
            activeTab = component;
        }

        tabs.push(component);
        tabsObj[component.id] = {comp: component, emptySpaceIdx: 0};
    }


    function getActiveTab() {
        if (activeTab === null) {
            throw new Error("No active tab.");
        }
        return activeTab;
    }


    function scrollTabDown(tab) {
        tab.scrollTop = tab.scrollHeight;
    }


    function showTabById(id) {
        tabs.forEach((item) => {
            if (item.id === id) {
                item.style.display = "block";
                activeTab = item;
                getTabHeader(activeTab).style.backgroundColor = getStateEasy().constants.getColor("activeTab");
                scrollTabDown(activeTab);
            } else {
                item.style.display = "none";
                getTabHeader(item).style.backgroundColor = getStateEasy().constants.getColor("normalTab");
            }
        });
    }


    function getTabHeader(elem) {
        let id = elem.id + "Header";
        return document.getElementById(id);
    }


    return {
        addTab: addTab,
        showTabById: showTabById,
        getActiveTab: getActiveTab,
        getTabHeader: getTabHeader,
        getEmptySpace: getEmptySpace,
        resetIndexes: resetIndexes,
        setMessageToTab: setMessageToTab,
        resetIndex: resetIndex
    };

}


/**
 * History utilities for command history console.
 *
 * @constructor
 */
function UserInputHistory() {
    this.commandInterface = document.getElementById("commandRow");
    this.historyConsole = document.getElementById("history");
    this.current = 1;
    this.numOfDefinedId = 0;
    this.historyList = [""];

    let userHistoryInside = this;

    this.selectHistoryElem = function (elem) {
        let normalColor = getConstants().getColor("passiveHistoryElement");
        elem.parentNode.childNodes.forEach(e => {
            if (e.id.startsWith("history;")) {
                e.style.backgroundColor = normalColor;
            }
        });
        elem.style.backgroundColor = getConstants().getColor("activeHistoryElement");
    };

    this.dispatchClick = function (e) {
        let id = e.target.id;

        for (let c = 0; c < 3; c++) {
            if (!id.startsWith("history")) {
                id = e.target.parentNode.id;
            } else {
                break;
            }
        }


        let clickedId;
        if (id.toLowerCase().startsWith("history")) {

            // Select it:

            let elem = document.getElementById(id);

            if (elem) {
                userHistory.selectHistoryElem(elem);
            }

            clickedId = Number(id.split(";")[1]);
            userHistoryInside.setToCommandInput(clickedId);
            this.current = ((clickedId === 0) ? 1 : clickedId);
        } else {
            throw new Error("Could not find history element for that click!");
        }
    };


    this.addElements = function (num) {
        for (let c = 0; c < num; c++) {
            let newDiv = createEmptyAreaElement();
            newDiv.id = "history" + ";" + this.numOfDefinedId;
            this.numOfDefinedId += 1;
            newDiv.onclick = this.dispatchClick;

            this.historyConsole.appendChild(newDiv);

        }

    };


    this.setToCommandInput = function (idx) {
        let text = (idx !== undefined) ? this.historyList[idx + 1] : this.getEvent();
        this.commandInterface.value = text || "";
    };

    this.getEvent = function () {
        if (this.historyList[this.current] === undefined) {
            return "";
        }

        return this.historyList[this.current];
    };

    this.addEvent = function (event) {
        this.historyList.push(event);
    };

    this.scrollUp = function () {
        this.current -= 1;

        if (this.current <= 0) {
            this.current = 1;
        }


        let e = document.getElementById("history;" + (this.current - 1));
        if (e) {
            userHistoryInside.selectHistoryElem(e);
        }
    };

    this.scrollDown = function () {
        this.current += 1;

        if (this.current >= this.historyList.length) {
            this.current = this.historyList.length;
        }

        let e = document.getElementById("history;" + (this.current - 1));
        if (e) {
            userHistoryInside.selectHistoryElem(e);
        }
    };

    this.clear = function () {
        this.current = 1;
        this.historyList = [""];
    };

    this.setToLast = function () {
        this.current = this.historyList.length;
    };

    this.addToTabComponent = function (elem, cmd) {
        // Color the command and save it to empty line in history.
        let colored = colorCodeCommand(cmd);
        sendToTabConsole(elem, colored);

        this.setToLast();
    }
}


let userHistory = new UserInputHistory();
globalEnv.def("userHistory", userHistory);

let tabComp = new TabbedComponent();
globalEnv.def("tabbedComponent", tabComp);


/**
 * Returns tabbed component which is defined in global environment.
 *
 * @returns {*} tabbed component
 */
function getTabCompEasy() {
    return globalEnv.get("tabbedComponent");
}


/**
 * Parses user input as single or multiline command.
 * @param command command to parse
 * @param e event object
 */
function parseCommand(command, e) {
    let userHistory = globalEnv.get("userHistory");
    let keyCode = e.keyCode || e.which;

    // Set user input history
    let userHistoryElem = document.getElementById("history");
    let commandInterface = document.getElementById("commandRow");

    if (keyCode === constants.keyCodes.up) {
        userHistory.scrollUp();
        userHistory.setToCommandInput();

    } else if (keyCode === constants.keyCodes.down) {
        userHistory.scrollDown();
        userHistory.setToCommandInput();
    }

    if (keyCode !== constants.keyCodes.enter) {
        return;
    }


    if (command.trim() === "") {
        return;
    }

    // Got submit command, parse it
    let is = new InputStream(command);
    let ts = new TokenStream(is);
    let parser = new Parser(ts);

    try {
        //console.time("$::Parser start::$");
        let ast = parser.parseTopLevel();
        //console.timeEnd("$::Parser start::$");

        //console.time("$::Evaluator start::$");
        evaluate(ast, globalUserEnv);
        //console.timeEnd("$::Evaluator start::$");
        getStateEasy().update();
    } catch (err) {
        setErrorToConsole(err.message);
        //console.log("Error", err);
        tabComp.showTabById("console");
        return;
    }


    commandInterface.value = "";
    addMultipleCommandsToHistory(userHistory, command, userHistoryElem)


}


/**
 * Helper function to split command at ";" character and prints all parts to separate lines into command history.
 *
 * @param userHistory history to print commands to
 * @param command command to split and print its parts
 * @param historyElem DOM element which holds history tab component
 */
function addMultipleCommandsToHistory(userHistory, command, historyElem) {
    let commentSplit = command.split("//");
    if (commentSplit.length > 1) {
        userHistory.addEvent(command);
        userHistory.addToTabComponent(historyElem, command);
        return;
    }
    let commandsArr = command.split(";");

    let size = commandsArr.length;
    if (size === 1) {
        userHistory.addEvent(commandsArr[0]);
        userHistory.addToTabComponent(historyElem, commandsArr[0]);
        return;
    }

    let lastSemicolon = false;
    if (command.endsWith(";")) {
        lastSemicolon = true;
    }

    for (let c = 0; c < size; c++) {
        let semicolonCh = ";";
        if (c === (size - 1) && lastSemicolon === false) {
            semicolonCh = "";
        }
        let cmd = commandsArr[c].trim() + semicolonCh;
        if (cmd === ";") {
            break;
        }
        userHistory.addEvent(cmd);
        userHistory.addToTabComponent(historyElem, cmd)
    }


}


/**
 * Adds initial messages to user history.
 *
 * @param userHistory history to add message to
 * @param historyElem DOM element which holds history tab component
 */
function addInitialMessagesToHistory(userHistory, historyElem) {
    if (!historyElem) {
        throw new Error("History element not defined.");
    }

    let instr1 = "// This is a comment.";
    let instr2 = "// Use up and down keyboard arrows to navigate this history.";
    userHistory.addEvent(instr1);
    userHistory.addEvent(instr2);

    let colored1 = colorCodeCommand(instr1);
    let colored2 = colorCodeCommand(instr2);
    sendToTabConsole(historyElem, colored1);
    userHistory.setToLast();
    sendToTabConsole(historyElem, colored2);
    userHistory.setToLast();
}


/**
 * Gets parent DIV element based on value provided. Specifies it with special unique identification for later retrieving.
 *
 * @param value value to set inside parent
 * @param name name of variable whose value is being processed
 * @returns {*}
 */
function getResolvedParent(value, name) {
    let elem = null;
    let uidPrefix = name + getStateEasy().constants.getString("uidPostfix");


    // If value is a $m3 matrix
    if (value instanceof $m3) {
        let parentMatrix = document.createElement("div");
        parentMatrix.className = "matrixParent";
        parentMatrix.id = uidPrefix;

        createStringRepresentation(value, true);
        value.stringRepresentation.forEach((str) => addLineToParent(str, parentMatrix));

        elem = parentMatrix;
    }

    // If value is array of matrices
    let message = value;
    if (Array.isArray(value)) {
        if (value.length === 0) {
            message = "Stack is empty.";
        } else {

            let parentStackMatrix = document.createElement("div");
            parentStackMatrix.className = "matrixParent";
            parentStackMatrix.id = uidPrefix;

            value.forEach((x, idx) => {
                if (x instanceof $m3) {
                    let newElem = document.createElement("div");
                    newElem.className = "stackPrefix";
                    newElem.innerHTML = (colorCodeCommand("Stack index") + ": " + idx);
                    parentStackMatrix.appendChild(newElem);

                    createStringRepresentation(x, true);
                    x.stringRepresentation.forEach((str) => addLineToParent(str, parentStackMatrix));
                }
            });
            elem = parentStackMatrix;
        }
    }

    // It is a simple value, true false or constant.
    if (elem === null) {
        elem = createOuterSpanNode("envSimpleValue", message);
        elem.id = uidPrefix;
    }

    elem.style.display = "none";
    return elem;
}


/**
 * Creates initial expansion of variables and hides the parent holding values.
 *
 * @param varName name of variable to create expansion for
 */
function createInitialExpansion(varName) {
    // Expand element to hold value of its variable
    let envConsole = getTabEasy("environmentVars");
    let variableValue = globalUserEnv.get(varName);

    let createdParent = getResolvedParent(variableValue, varName);
    createdParent.style.display = "none";

    let nextEmptyLine = findSpaceInConsole(envConsole);
    envConsole.insertBefore(createdParent, nextEmptyLine);
}


/**
 * Event callback for toggling expanded values.
 *
 * @param e event object
 * @param varName name of a variable to toggle
 */
function expandValueEvent(e, varName) {
    let elem = e.target;
    let id = varName + getStateEasy().constants.getString("uidPostfix");

    let target = document.getElementById(id);
    let isHidden = (target.style.display === "none");
    if (isHidden) {
        target.style.display = "block";
        elem.innerHTML = getStateEasy().constants.getString("expandedSymbol");
    } else {
        target.style.display = "none";
        elem.innerHTML = getStateEasy().constants.getString("collapsedSymbol");
    }
}


/**
 * Adds initially defined variables into environment console tab.
 *
 * @param env environment console
 */
function addInitialVarsToEnvironment(env) {
    if (!env) {
        throw new Error("Environment element not defined.");
    }

    let initialVars = globalUserEnv.vars;

    let idxName = "expandedValueIdx";
    let numOfDefinedVars = globalEnv.get(idxName);
    Object.keys(initialVars).forEach(function (key) {
        numOfDefinedVars += 1;
        createNewEnvironmentVar(env, numOfDefinedVars, idxName, key);
    });
}


/**
 * Initializes webGL to canvas and exports state to global environment.
 */
function init() {
    let state = initCanvas("webGL2D");
    let letterF = new LetterFShape("LetterF" + createUID(112), 0, 0);
    applyOptionalArgs(letterF, globalUserEnv.get("defaultOutlineColor"), globalUserEnv.get("defaultFillColor"));


    globalEnv.def("myState", state);
    state.update();

    let numOfInitialSpaces = state.constants.getNumeric("initialTextAreaRows");
    let userHistory = globalEnv.get("userHistory");
    createDescription();
    createFontChanger();
    userHistory.addElements(numOfInitialSpaces);
    addBlankRows("console", numOfInitialSpaces);
    addBlankRows("environmentVars", numOfInitialSpaces);


    let consoleId = "console";
    let envId = getStateEasy().constants.getString("envConsole");
    let consoleElem = document.getElementById(consoleId);


    let historyElem = document.getElementById("history");
    let envElem = document.getElementById(envId);

    // Add tabs to tabbed component
    tabComp.addTab(historyElem);
    tabComp.addTab(consoleElem);
    tabComp.addTab(envElem);

    tabComp.showTabById(consoleId);

    addInitialMessagesToHistory(userHistory, historyElem);
    addInitialVarsToEnvironment(envElem);
    createPaneDialog();

    // Set description to hidden
    toggleElement("keywordsDesc");
}


init();