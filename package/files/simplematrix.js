function matrix(arr) {
  this.rows = arr.length;
  if(arr.length < 1) throw "Expected 2D array";
  this.cols = arr[0].length;
  for(let i = 1; i < this.rows; i++) {
    if(arr[i].length != this.cols) throw "Expected 2D array";
  }
  this.data = arr;
}

matrix.prototype.add = function(m2) {
  if(this.rows != m2.rows || this.cols != m2.cols) throw "Incompatible matrices for addition: "+this.rows+"x"+this.cols+" and "+m2.rows+"x"+m2.cols+".";
  let res = [];
  for(let r = 0; r < this.rows; r++) {
    let row = [];
    for(let c = 0; c < this.cols; c++) {
      row.push(this.data[r][c]+m2.data[r][c]);
    }
    res.push(row);
  }
  return new matrix(res);
}

matrix.prototype.sub = function(m2) {
  if(this.rows != m2.rows || this.cols != m2.cols) throw "Incompatible matrices for subtraction: "+this.rows+"x"+this.cols+" and "+m2.rows+"x"+m2.cols+".";
  let res = [];
  for(let r = 0; r < this.rows; r++) {
    let row = [];
    for(let c = 0; c < this.cols; c++) {
      row.push(this.data[r][c]-m2.data[r][c]);
    }
    res.push(row);
  }
  return new matrix(res);
}

matrix.prototype.mul = function(m2) {
  if(this.cols != m2.rows) throw "Incompatible matrices for multiplication: "+this.rows+"x"+this.cols+" and "+m2.rows+"x"+m2.cols+".";
  let res = [];
  for(let r = 0; r < this.rows; r++) {
    let row = [];
    for(let c = 0; c < m2.cols; c++) {
      let sum = 0;
      for(let k = 0; k < this.cols; k++) {
        sum += this.data[r][k]*m2.data[k][c];
      }
      row.push(sum);
    }
    res.push(row);
  }
  return new matrix(res);
}

matrix.prototype.transpose = function() {
  let res = [];
  for(let c = 0; c < this.cols; c++) {
    let row = [];
    for(let r = 0; r < this.rows; r++) {
      row.push(this.data[r][c]);
    }
    res.push(row);
  }
  return new matrix(res);
}

matrix.prototype.toString = function() {
  let res = "[";
  for(let r = 0; r < this.rows; r++) {
    if(r>0) res += ", ";
    res += "[";
    for(let c = 0; c < this.cols; c++) {
      if(c>0) res += ", ";
      res += this.data[r][c];
    }
    res += "]";
  }
  res += "]";
  return res;
}

matrix.prototype.inverse = function() {
  if(this.cols != this.rows) throw "Incompatible matrix for inverse: "+this.rows+"x"+this.cols+" - must be square.";
  let buf = [];
  let res = [];
  for(let r = 0; r < this.rows; r++) {
    let resrow = [];
    let bufrow = [];
    for(let c = 0; c < this.cols; c++) {
      bufrow.push(this.data[r][c]);
      resrow.push(0);
    }
    resrow[r] = 1;
    buf.push(bufrow);
    res.push(resrow);
  }

  for(let r = 0; r < this.rows; r++) {
    let mv = Math.abs(buf[r][r]); let mi = r;
    for(let i = r+1; i < this.rows; i++) {
      let v = Math.abs(buf[i][r]);
      if(v>mv) { mv = v; mi = i; }
    }
    if(mi!=r) {  // dodaj mi-ti redak r-tom retku
      if((buf[mi][r]<0 && buf[r][r]>0) || (buf[mi][r]>0 && buf[r][r]<0)) {
        // treba ih odbiti
        for(let k = r; k < this.cols; k++) {
          buf[r][k] -= buf[mi][k];
        }
        for(let k = 0; k < this.cols; k++) {
          res[r][k] -= res[mi][k];
        }
      } else {
        // treba ih zbrojiti
        for(let k = r; k < this.cols; k++) {
          buf[r][k] += buf[mi][k];
        }
        for(let k = 0; k < this.cols; k++) {
          res[r][k] += res[mi][k];
        }
      }
    }
    let div = buf[r][r];
    for(let k = r; k < this.cols; k++) {
      buf[r][k] /= div;
    }
    for(let k = 0; k < this.cols; k++) {
      res[r][k] /= div;
    }
    for(let rr = 0; rr < this.rows; rr++) {
      if(rr == r) continue;
      let f = -buf[rr][r];
      for(let k = r; k < this.cols; k++) {
        buf[rr][k] += f*buf[r][k];
      }
      for(let k = 0; k < this.cols; k++) {
        res[rr][k] += f*res[r][k];
      }
    }
  }

  return new matrix(res);
}

matrix.prototype.divByLastColumn = function() {
  let arr = [];
  for(let r = 0; r < this.rows; r++) {
    let row = [];
    let d = this.data[r][this.cols-1];
    for(let c = 0; c < this.cols; c++) {
      row.push(this.data[r][c] / d);
    }
    arr.push(row);
  }
  return new matrix(arr);
}

