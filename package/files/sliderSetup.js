function setupValuePicker(id, options) {
    let parent = document.getElementById(id);
    if (!parent) {
        return;
    }

    return createValuePicker(parent, options);
}


function createValuePicker(parent, options) {
    let uiPrecision = options.uiPrecision || 0;
    let name = options.name || "";
    let min = options.min;
    let max = options.max;
    let step = options.step || 1;
    let defaultValue = options.value || 0;
    let funcCall = options.funcCall;


    parent.innerHTML = '<div class="widget-picker-container"><div class="widgetPickerLabel">' + name + '</div><input class="widgetPicker" type="number" min="' + min + '" max="' + max + '" value="' + defaultValue + '" step="' + step + '" /></div>';


    let pickerElem = parent.querySelector(".widgetPicker");

    pickerElem.addEventListener("input", handleChange);
    pickerElem.addEventListener("change", handleChange);
    pickerElem.addEventListener("wheel", handleChange);


    function handleChange(event) {
        let value = parseInt(event.target.value);
        funcCall(event, {value: value});
    }


    return {
        elem: parent,
        updateValue: (v) => {
            pickerElem.value = v;
        }
    };
}


function setupSlider(id, options) {
    let parent = document.getElementById(id);
    if (!parent) {
        return;
    }
    return createSlider(parent, options);
}


function createSlider(parent, options) {
    let uiPrecision = options.uiPrecision || 0;
    let name = options.name || parent.innerHTML;
    let min = options.min || 0;
    let max = options.max || 1;
    let step = options.step || 1;
    let value = options.value || 0;
    let functionCallback = options.functionCallback;
    let uiMult = options.uiMult || 1;
    let sliderTickmarksId = options.sliderTickmarks || "";

    min /= step;
    max /= step;
    value /= step;

    parent.innerHTML = '<div class="widgetContainer"><div class="widgetLabel">' + name + '</div><div class="widgetValue"></div><input class="widgetSlider" type="range" list="' + sliderTickmarksId + '" min="' + min + '" max="' + max + '" value="' + value + '"/></div>';


    let valueElem = parent.querySelector(".widgetValue");
    let sliderElem = parent.querySelector(".widgetSlider");


    sliderElem.addEventListener("input", handleChange);
    sliderElem.addEventListener("change", handleChange);


    function updateValue(value) {
        valueElem.textContent = (value * step * uiMult).toFixed(uiPrecision);
    }


    updateValue(value);


    function handleChange(event) {
        let value = parseInt(event.target.value);
        updateValue(value);
        functionCallback(event, {value: value * step});
    }


    return {
        elem: parent,
        slider: sliderElem,
        updateValue: (v) => {
            v /= step;
            sliderElem.value = v;
            updateValue(v);
        },
    };
}
