function ContainsForPrimitiveTypes() {
    function line(mx, my, shape) {
        let o = shape;
        if ((mx < o.x) || (mx > o.x1)) {
            //return false;
        }


        function pointNearestMouse(line, x, y) {
            let lerp = function (a, b, x) {
                return (a + x * (b - a));
            };

            let dx = line.x1 - line.x;
            let dy = line.y1 - line.y;
            let t = ((x - line.x) * dx + (y - line.y) * dy) / (dx * dx + dy * dy);

            let lineX = lerp(line.x, line.x1, t);
            let lineY = lerp(line.y, line.y1, t);
            return ({x: lineX, y: lineY});
        }


        let linePoint = pointNearestMouse(o, mx, my);
        let dx = mx - linePoint.x;
        let dy = my - linePoint.y;
        let distance = Math.abs(Math.sqrt(dx * dx + dy * dy));

        let tolerance = 0.2;
        return distance < tolerance;
    }


    function circle(x, y, shape) {
        let dx = shape.x - x;
        let dy = shape.y - y;
        let r = shape.radius;
        return (((dx * dx) + (dy * dy)) < (r * r));
    }


    function rectangle(x, y, shape) {
        let p = shape;
        return (p.x <= x) && ((p.x + p.w) >= x) &&
            (p.y <= y) && ((p.y + p.h) >= y);
    }


    /**
     * Algorithm for convex and concave objects. Intersecting ray.
     *
     * @param p point to check
     * @param line line segment
     * @returns {boolean}
     */
    function rayIntersectAlgo(p, line) {
        let Py = p.y;
        let Px = p.x;
        let Ax = (line.y < line.y1) ? line.x : line.x1;
        let Ay = (line.y < line.y1) ? line.y : line.y1;
        let Bx = (line.y > line.y1) ? line.x : line.x1;
        let By = (line.y > line.y1) ? line.y : line.y1;
        let epsilon = 0;
        let m_blue = 0;
        let m_red = 0;


        if (Py === Ay || Py === By) {
            Py = Py + epsilon;
        }

        if ((Py < Ay) || (Py > By)) {
            return false;
        } else if (Px > Math.max(Ax, Bx)) {
            return false;
        } else {
            if (Px < Math.min(Ax, Bx)) {
                return true;
            } else {
                if (Ax !== Bx) {
                    m_red = (By - Ay) / (Bx - Ax);
                } else {
                    m_red = Math.MAX_SAFE_INTEGER;
                }
                if (Ax !== Px) {
                    m_blue = (Py - Ay) / (Px - Ax);
                }
                else {
                    m_blue = Math.MAX_SAFE_INTEGER;
                }
                return m_blue >= m_red;
            }
        }
    }


    return {
        line: line,
        circle: circle,
        rectangle: rectangle,
        rayIntersectAlgo: rayIntersectAlgo
    }
}


function addPrototypes() {
    let containsUtil = new ContainsForPrimitiveTypes();
    let u = new ParserUtil();
    let isOdd = u.isOdd;

    LineShape.prototype.contains = function (x, y) {
        return containsUtil.line(x - this.x, y - this.y, this);
    };


    SimplePoint.prototype.contains = function (x, y) {
        return containsUtil.circle(x, y, this);
    };

    SquareShape.prototype.contains = function (x, y) {
        let pretendToBeRectangle = {
            x: this.x,
            y: this.y,
            w: this.side,
            h: this.side
        };
        return containsUtil.rectangle(x, y, pretendToBeRectangle);
    };

    RectangleShape.prototype.contains = function (x, y) {
        return containsUtil.rectangle(x, y, this);
    };


    LetterFShape.prototype.contains = function (x, y) {
        let originPoint = {x: x - this.x, y: y - this.y};
        let count = 0;

        let lines = constructLetterFLines().map((line) => {
            return new LineShape(line.id, line.x * 5, line.y * 5, line.x1 * 5, line.y1 * 5);
        });
        lines.forEach((line) => {
            if (containsUtil.rayIntersectAlgo(originPoint, line)) {
                ++count;
            }
        });
        return isOdd(count);
    };

    PolygonShape.prototype.contains = function (x, y) {
        let originPoint = {x: x, y: y};
        let lines = constructLineFromStrip(this.strokeData);

        let count = 0;
        lines.forEach((line) => {
            if (containsUtil.rayIntersectAlgo(originPoint, line)) {
                ++count;
            }
        });

        return isOdd(count);

    };

    PolylineShape.prototype.contains = function (x, y) {
        let lines = constructLineFromStrip(this.strokeData);

        return lines.some((line) => {
            return containsUtil.line(x, y, line);
        });
    }

}


function constructLineFromStrip(strip) {
    let lineId = "noId";
    let size = strip.length / 2;
    let lines = [];
    lines.push(new LineShape(lineId, strip[0], strip[1], strip[2], strip[3]));
    let previousLine = lines[0];
    for (let c = 1; c < (size - 1); c++) {
        let newLine = new LineShape(lineId, previousLine.x1, previousLine.y1, strip[c * 2 + 2], strip[c * 2 + 3]);
        lines.push(newLine);
        previousLine = newLine;
    }

    lines.push(new LineShape(lineId, previousLine.x1, previousLine.y1, strip[0], strip[1]));
    return lines;
}


addPrototypes();