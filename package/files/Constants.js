/**
 * Main constants used for this project.
 * This constructor holds default values for all constants.
 *
 * Several constants object are provided: "colors", "fonts", "strings", "numeric" and "keyCodes". They offer various elements including a lot of colors, fonts, string constants, numeric constants and common key codes for keyboard presses.
 */
class Constants {
    constructor(colors = {
                    black: "black",
                    yellow: "yellow",
                    red: "red",
                    white: "white",
                    orange: "orange",
                    awesome: "#8836ba",
                    gray: "#AF1E37",
                    darkRed: "#d02b2a",
                    purple: "#8e44ad",
                    emerald: "#2ecc71",
                    midnight: "#2c3e50",
                    dodgerBlue: "#1E90FF",
                    flipD: "#1E90FF",
                    flipT: "#886F98",
                    diagramArrow: "#da0000",
                    flipSR: "#CB3A34",
                    flipJK: "#2ecc71",
                    registerColor: "#50928c",
                    clickBoxColor: "#746dc5",
                    darkBlue: "#00008B",
                    lightSkyBlue: "#87CEFA",
                    glowColor: "rgba(255, 0, 0, 0.5)",
                    fullTransparent: "rgba(255, 255, 255, 0)",
                    defaultPixel: "#b5e2cb",
                    selectedPixel: "#b65bab",
                    correctColor: "#71bf9a",
                    successBox: "#8cdf7b",
                    infoNiceBlue: "#2196F3",
                    pixelActiveInput: "#df120e",
                    startPoints: "#e23e41",
                    selectorColor: "#12ACBD",
                    gridColor: "#dda8d7",
                    letterF: "#123DBC"
                },
                fonts = {
                    pixelFont: "14px bold Schoolbook",
                    normalFont: "14px Schoolbook",
                    arialFont: "12px Arial",
                    startEndPoints: "24px Schoolbook",
                    axesFont: "12px Schoolbook"
                },
                strings = {
                    EMPTY: "",
                    letterF: "LetterF"
                },
                numeric = {
                    pixelDimension: {
                        w: 25,
                        h: 25
                    },
                    objRotation: 45

                },
                keyCodes = {
                    left: 37,
                    up: 38,
                    right: 39,
                    down: 40,
                    enter: 13,
                    E: 69
                },
                smallLetters = {
                    /* Various text subscripts and superscripts */
                    /* Subscript <sub> x </sub> numbers */
                    sup0: '\u2070',
                    sup1: '\u00B9',
                    sup2: '\u00B2',
                    sup3: '\u00B3',
                    sup4: '\u2074',
                    sup5: '\u2075',
                    sup6: '\u2076',
                    sup7: '\u2077',
                    sup8: '\u2078',
                    sup9: '\u2079',

                    /* Superscript <sup> x </sup> numbers */
                    sub0: '\u2080',
                    sub1: '\u2081',
                    sub2: '\u2082',
                    sub3: '\u2083',
                    sub4: '\u2084',
                    sub5: '\u2085',
                    sub6: '\u2086',
                    sub7: '\u2087',
                    sub8: '\u2088',
                    sub9: '\u2089',

                    // Small sub letters
                    subA: '\u2090',
                    subE: '\u2091',
                    subO: '\u2092',
                    subM: '\u2098',
                    subN: '\u2099',

                    supN: '\u207F',

                    // Other signs
                    subPlus: '\u208A',
                    subMinus: '\u208B',
                    subBracketLeft: '\u208F',
                    supBracketLeft: '\u207D',
                    subBracketRight: '\u208E',
                    supBracketRight: '\u207E',
                },
                widths = {}) {
        this.colors = colors;
        this.fonts = fonts;
        this.strings = strings;
        this.numeric = numeric;
        this.keyCodes = keyCodes;
        this.smallLetters = smallLetters;
        this.widths = widths;

        this.getRGBAColor = function (name) {
            try {
                return convertHexToRGBA(this.colors[name]);
            } catch (err) {
                throw ("Undefined color for name: " + name);
            }

        };

        this.getColor = function (name) {
            try {
                return this.colors[name];
            } catch (err) {
                throw ("Undefined color for name: " + name);
            }
        };

        this.getString = function (name) {
            try {
                return this.strings[name];
            } catch (err) {
                throw ("Undefined color for name: " + name);
            }
        };

        this.getKeyCode = function (name) {
            try {
                return this.keyCodes[name];
            } catch (err) {
                throw ("Undefined color for name: " + name);
            }
        };


        this.getFont = function (name) {
            try {
                return this.fonts[name];
            } catch (err) {
                throw ("Undefined color for name: " + name);
            }
        };


        this.getNumeric = function (name) {
            try {
                return this.numeric[name];
            } catch (err) {
                throw ("Undefined color for name: " + name);
            }
        };


    }


}


/**
 * Gets current project constants using additional specified constants in specs as well as other constants.
 *
 * @returns {Constants} Constants with specified constants
 */
function getConstants() {
    // Constants used for this question.
    let colors = {
        black: "black",
        yellow: "yellow",
        red: "red",
        white: "white",
        orange: "orange",
        awesome: "#8836ba",
        gray: "#AF1E37",
        darkRed: "#d02b2a",
        purple: "#8e44ad",
        emerald: "#2ecc71",
        midnight: "#2c3e50",
        dodgerBlue: "#1E90FF",
        darkBlue: "#00008B",
        lightSkyBlue: "#87CEFA",
        glowColor: "rgba(255, 0, 0, 0.5)",
        fullTransparent: "rgba(255, 255, 255, 0)",
        crystal: "rgba(214, 72, 74, 0.25)",
        testColor123: "#d6484a",
        gridColor: "#add9ff",
        test: "#9ad5e8",
        //gridColor: "#69b7b0",
        axisColor: "#7487dd",
        // letterF: "#418aac",
        letterF: "#3810ed",
        arrows: "#6c5eee",
        rotateCircle: "#5645bc",
        selectionColorF: "#f41929",
        arrowShadow: "#e61d14",
        transparency: {
            transparent: 0.7,
            nonTransparent: 1
        },

        logColor: "#1ca9f3",
        logTextColor: "#a85c93",
        activeTab: "#a85c93",
        normalTab: "#67a89b",

        activeHistoryElement: "rgba(126, 128, 140, 0.3)",
        passiveHistoryElement: "#3b3a3b",

        selectedShapeColor: "#fffdfd"
    };
    let fonts = {
        normalFont: "14px Schoolbook",
        arialFont: "12px Arial",
        axesFont: "12px Schoolbook",
        schoolBook: "14px Schoolbook"
    };

    let strings = {
        EMPTY: "",
        envConsole: "environmentVars",
        letterF: "LetterF",
        tabUID: "tabUID",
        //expandedSymbol: "\u22BF",
        //collapsedSymbol: "\u22B3",
        //collapsedSymbol: " &#x203A ",
        /*   collapsedSymbol: "&#129170;",
           expandedSymbol: "&#129171;",*/
        collapsedSymbol: "&#9658;",
        expandedSymbol: "&#9660;",
        uidPostfix: "!.!",
        noRetValue: "No return value.",

        shapeTypes: ["Line", "Square", "Rectangle", "LetterF", "Point", "Polyline", "Polygon"],
        degreeSymbol: "&deg;"
    };
    let numeric = {
        canvasWUnits: 600,
        canvasHUnits: 360,
        gridStep: 12,
        rotStep: 45,
        defaultFOV: (60),

        mathPrecision: 6,

        // Tab components and text areas
        initialTextAreaRows: 10,
        expandTextAreaRows: 5,


        // Point types
        squareLength: 0.66,
        rhombusLength: 0.66,
        crossRectW: 0.88,
        crossRectH: 0.143,
        rhombusSideLength: 0.66,
        squareSideLength: 0.66,
        circleRadius: 0.33,

    };
    let keyCodes = {
        escape: 27,
        left: 37,
        up: 38,
        right: 39,
        down: 40,
        enter: 13,
        E: 69,
        A: 65,
        S: 83,
        D: 68,
        W: 87
    };
    let smallLetters = {
        /* Various text subscripts and superscripts */
        /* Subscript <sub> x </sub> numbers */
        sup0: '\u2070',
        sup1: '\u00B9',
        sup2: '\u00B2',
        sup3: '\u00B3',
        sup4: '\u2074',
        sup5: '\u2075',
        sup6: '\u2076',
        sup7: '\u2077',
        sup8: '\u2078',
        sup9: '\u2079',

        /* Superscript <sup> x </sup> numbers */
        sub0: '\u2080',
        sub1: '\u2081',
        sub2: '\u2082',
        sub3: '\u2083',
        sub4: '\u2084',
        sub5: '\u2085',
        sub6: '\u2086',
        sub7: '\u2087',
        sub8: '\u2088',
        sub9: '\u2089',

        // Small sub letters
        subA: '\u2090',
        subE: '\u2091',
        subO: '\u2092',
        subM: '\u2098',
        subN: '\u2099',

        supN: '\u207F',

        // Other signs
        subPlus: '\u208A',
        subMinus: '\u208B',
        subBracketLeft: '\u208F',
        supBracketLeft: '\u207D',
        subBracketRight: '\u208E',
        supBracketRight: '\u207E',
    };

    let widths = {
        axisWidth: 2
    };

    return new Constants(colors, fonts, strings, numeric, keyCodes, smallLetters, widths);
}
