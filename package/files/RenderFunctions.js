function createAndRenderSquare(x, y, sideLength) {
    return new SquareShape("Square" + createUID(x + y), x, y, sideLength);
}


function createAndRenderPoint(x, y) {
    let pId = getStateEasy().getNextPointIndex();
    return new SimplePoint(pId, x, y);
}


function createAndRenderPolyline(...args) {
    let u = new ParserUtil();
    if (u.isOdd(args.length)) {
        throw new Error("Expected even number of arguments for function Polyline.");
    }
    return new PolylineShape("Polyline" + createUID(42), args);
}


function createAndRenderLetterFAtPos(x, y) {
    return new LetterFShape("LetterF" + createUID(x + y), x, y);
}


function createAndRenderLetterF() {
    return new LetterFShape("LetterF" + createUID(42), 0, 0);
}


function createAndRenderPolygonAnyVertices(...args) {
    let u = new ParserUtil();
    if (u.isOdd(args.length)) {
        throw new Error("Expected even number of arguments for function Polygon.");
    }
    return new PolygonShape("Polygon" + createUID(42), args);
}


function createAndRenderPolygon(...args) {
    let u = new ParserUtil();
    let vertices = args.length / 2;
    if (u.isOdd(args.length)) {
        throw new Error("Expected even number of arguments for function Polygon.");
    } else if (vertices < 3) {
        throw new Error("Expected at least 3 vertices in order to construct a Polygon.");
    }
    return new PolygonShape("Polygon" + createUID(42), args);
}


function createAndRenderRectangle(x, y, w, h) {
    return new RectangleShape("Rectangle" + createUID(x + y * w), x, y, w, h);
}


function createAndRenderLine(x, y, x1, y1) {
    return new LineShape("Line" + createUID(x - y1 + x1), x, y, x1, y1);
}