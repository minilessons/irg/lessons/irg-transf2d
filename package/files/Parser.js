/**
 * Parser for parsing simple one line commands from user.
 *
 * @param input token stream
 * @returns {{parseTopLevel: parseTopLevel}} main function to run the parser
 * @constructor
 */
function Parser(input) {
    let parserUtil = new ParserUtil();
    let isWhitespace = parserUtil.isWhitespace;

    let keywords = parserUtil.keywords;
    let pointTypes = parserUtil.pointTypes;
    let printTypes = parserUtil.printTypes;
    let selectionFunctions = ["square", "point", "polyline", "polygon",
        "rectangle", "line"];
    let functionsWithVariableArgs = ["polyline", "polygon"];
    let standaloneFunctions = parserUtil.standaloneFunctions;
    let allowedMatrixFunctions = ["sin", "cos", "tan"];
    let PRECEDENCE = {
        "=": 1,
        "||": 2,
        "&&": 3,
        "<": 7, ">": 7, "<=": 7, ">=": 7, "==": 7, "!=": 7,
        "+": 10, "-": 10,
        "*": 20, "/": 20, "%": 20,
        ".": 21
    };

    let FALSE = {type: "bool", value: false};
    let expressionCheckPoint = false;
    let initialMessage = "Expecting a statement.";
    let lastSavedMessage = initialMessage;

    return {
        parseTopLevel: parseTopLevel
    };


    function unexpectedMessage() {
        let msg = lastSavedMessage;
        lastSavedMessage = initialMessage;
        input.error(msg);
    }


    /**
     * Checks if character or token is a punctuation without skipping it.
     *
     * @param ch character to check
     * @returns {string|*|boolean}
     */
    function isPunc(ch) {
        let tok = input.peek();
        return tok && tok.type === "punc" && (!ch || tok.value === ch) && tok;
    }


    /**
     * Checks if character or token is keyword without skipping it.
     *
     * @param kw keyword token to check
     * @returns {string|*|boolean}
     */
    function isKeyword(kw) {
        let tok = input.peek();
        return tok && tok.type === "kw" && (!kw || tok.value === kw) && tok;
    }


    /**
     * Checks if character or token is function name without skipping it.
     *
     * @param func name to check
     * @returns {string|*|boolean}
     */
    function isFunc(func) {
        let tok = input.peek();
        return tok && tok.type === "func" && (!func || tok.value === func) && tok;
    }


    /**
     * Checks if character or token is operator without skipping it.
     * @param op operator to check
     * @returns {string|*|boolean}
     */
    function isOper(op) {
        let tok = input.peek();
        return tok && tok.type === "op" && (!op || tok.value === op) && tok;
    }


    /**
     * Skips punctuation characters.
     *
     * @param ch character to skip
     */
    function skipPunc(ch) {
        if (isPunc(ch)) {
            input.next();
        } else {
            input.error("Expecting punctuation: \"" + ch + "\"");

        }
    }


    /**
     * Skips keyword token.
     *
     * @param kw keyword to skip
     */
    function skipKeyword(kw) {
        if (isKeyword(kw)) {
            input.next();
        } else {
            input.error("Expecting keyword: \"" + kw + "\"");
        }
    }


    /**
     * Skips operator token.
     * @param op operator to skip
     */
    function skipOper(op) {
        if (isOper(op)) {
            input.next();
        }
        else input.error("Expecting operator: \"" + op + "\"");
    }


    /**
     * Helper function to throw errors on wrong tokens.
     */
    function unexpected() {
        input.error(lastSavedMessage);
        input.error("Unexpected token for token: " + JSON.stringify(input.peek()));
    }


    /**
     * Function which is called after each token. Checks whether there is operand binding between current and next token.
     *
     * @param left left token of possibly binary expression
     * @param myPrec current operator precedence
     * @returns {*}
     */
    function maybeBinary(left, myPrec) {
        let tok = isOper();
        if (tok) {
            expressionCheckPoint = true;
            lastSavedMessage = "Expected expression after operator: '" + tok.value + "'";
            let hisPrec = PRECEDENCE[tok.value];
            if (hisPrec > myPrec) {
                input.next();
                return maybeBinary({
                    type: (tok.value === "=" || tok.value === "<=") ? "assign" : "binary",
                    operator: tok.value,
                    left: left,
                    right: (tok.value === ".") ? parseInv() : maybeBinary(parseAtom(), hisPrec)
                }, myPrec);
            }
        }
        return left;
    }


    function skipWhitespace() {
        let peeked = input.peekChar();
        if (!isWhitespace(peeked) && isPunc(peeked)) {
            input.error("Expected space between parameters.");
        }
    }


    /**
     * Helper function to parse any expression separated with separator between start and stop tokens.
     *
     * @param parser parser to call on each expression between start and end tokens
     * @param stop end token for expression
     * @returns {Array}
     */
    function delimitedSpace(parser, stop, argCount) {
        let a = [];
        let first = true;

        let c = 0;
        while (!input.eof()) {
            if (isPunc(stop)) break;
            if (first) {
                first = false;
            } else {
                skipWhitespace();
            }
            if (isPunc(stop)) break;

            a.push(parser());
            c = c + 1;

            if (c >= argCount) {
                break;
            }

        }
        return a;
    }


    /**
     * Helper function to parse any expression separated with separator between start and stop tokens.
     *
     * @param start start token for expression
     * @param stop end token for expression
     * @param separator separator between more tokens inside expression
     * @param parser parser to call on each expression between start and end tokens
     * @returns {Array}
     */
    function delimited(start, stop, separator, parser) {
        let a = [], first = true;
        skipPunc(start);
        while (!input.eof()) {
            if (isPunc(stop)) break;
            if (first) first = false; else skipPunc(separator);
            if (isPunc(stop)) break;
            a.push(parser());
        }
        skipPunc(stop);
        return a;
    }


    /**
     * Helper function to setup call node.
     *
     * @param func function name
     * @returns {{type: string, func: *, args: Array}}
     */
    function parseCall(func) {
        return {
            type: "call",
            func: func,
            args: delimited("(", ")", ",", parseExpression),
            checkArgsCount: !isVariableArgs(func)
        };
    }


    /**
     * Helper function which is called for tokens in case of a call sequence. Checks whether there is call of type "(args)".
     *
     * @param expr expression to call before checking for call
     * @returns {{type: string, func: *, args: Array}}
     */
    function maybeCall(expr) {
        expr = expr();
        return (isPunc("(")) ? parseCall(expr) : expr;
    }


    /**
     * Parses sequence for keyword "print".
     *
     * @param kw keyword "print"
     * @returns {{type: string, value}}
     */
    function parsePrint(kw) {
        skipKeyword(kw);

        lastSavedMessage = "Missing print argument.";
        let token = parseExpression();
        if (!token) {
            input.error("Expected one argument for print.");
        }

        checkArg(token, printTypes, "Cannot print token: " + JSON.stringify(token.type));

        return {
            type: "print",
            value: token
        };
    }


    /**
     * Parses sequence for keyword "clearStack".
     *
     * @param kw keyword "clearStack"
     * @returns {{type: string}}
     */
    function parseClearStack(kw) {
        skipKeyword(kw);
        return {
            type: "clearStack"
        };
    }


    /**
     * Parses sequence for keyword "pop".
     *
     * @param kw keyword "pop"
     * @returns {{type: string}}
     */
    function parsePop(kw) {
        skipKeyword(kw);
        return {
            type: "pop"
        };
    }


    /**
     * Parses sequence for keyword "points". Continuation must be "on" or "off".
     *
     * @param kw keyword "points"
     * @returns {{type: string}}
     */
    function parsePoints(kw) {
        skipKeyword(kw);

        // Must come off or on
        let ret = {
            type: "points"

        };

        if (isKeyword(keywords.ON)) {
            skipKeyword(keywords.ON);
            ret.value = true;
        } else if (isKeyword(keywords.OFF)) {
            skipKeyword(keywords.OFF);
            ret.value = false;
        } else {
            input.error("Expecting keyword: '" + keywords.ON + "'" + " or '" + keywords.OFF + "'.");
        }

        return ret;
    }


    /**
     * Parses sequence for keyword "trace". Continuation must be "on" or "off".
     *
     * @param kw keyword "trace"
     * @returns {{type: string}}
     */
    function parseTrace(kw) {
        skipKeyword(kw);

        // Must come off or on
        let ret = {
            type: "trace"

        };

        if (isKeyword(keywords.ON)) {
            skipKeyword(keywords.ON);
            ret.value = true;
        } else if (isKeyword(keywords.OFF)) {
            skipKeyword(keywords.OFF);
            ret.value = false;
        } else {
            input.error("Expecting keyword: '" + keywords.ON + "'" + " or '" + keywords.OFF + "'.");
        }

        return ret;
    }


    /**
     * Parses sequence for unary operators.
     *
     * @param op unary operator
     * @returns {{type: string, value, operand}}
     */
    function parseUnary(op) {

        skipOper(op.value);

        return {
            type: "unary",
            value: op.value,
            operand: parseExpression()
        };
    }


    /**
     * Parses sequence needed to construct matrix in 2D.
     *
     * @returns {{type: string}}
     */
    function parseMatrix2D() {
        let ret = {
            type: "matrix"
        };

        let m = [];

        parseRow(m, 0);
        skipOper("|");
        parseRow(m, 1);
        skipOper("|");
        parseRow(m, 2);

        // Save matrix values
        ret.value = m;


        function parseRow(matrix, rowIdx) {
            let row = [];
            lastSavedMessage = "Expecting three numbers for one row";
            for (let c = 0; c < 3; c++) {
                let matrixElementDesc = " At matrix element: [" + rowIdx + ", " + c + "].";
                if (isKeyword()) {
                    input.error("Expected number but got keyword." + matrixElementDesc);
                }

                let tok = parseExpression();

                if (tok.type === "call") {
                    if (!isValidMatrixFunc(tok.func)) {
                        input.error("Expected one of " + beautifyArray(allowedMatrixFunctions) +
                            " but got function: '" + tok.func.value + "'." + matrixElementDesc);
                    }
                }

                row.push(tok);
            }

            matrix.push(row);
        }


        return ret;
    }


    /**
     * Returns boolean node for boolean token.
     *
     * @returns {{type: string, value: boolean}}
     */
    function parseBool() {
        return {
            type: "bool",
            value: input.next().value === "true"
        };
    }


    /**
     * Parses sequence of "inv" calls. Possibly not needed because of new dot "." operator.
     */
    function parseInv() {
        let numOfInverses = 1;

        skipKeyword(keywords.INV);
        while (isOper(".")) {
            skipOper(".");

            // Must be inverse
            skipKeyword(keywords.INV);

            numOfInverses += 1;
        }

        let ret = {
            type: "inv",
            value: "inv"
        };
        if (parserUtil.isOdd(numOfInverses)) {
            ret.apply = true;
            return ret;
        } else {
            ret.apply = false;
            return ret;
        }
    }


    /**
     * Helper function to check if value is among array of given types.
     * @param value value to check
     * @param types allowed types in array
     * @returns {boolean}
     */
    function isAllowedType(value, types) {
        return types.indexOf(value) > -1;
    }


    /**
     * Helper function to check types of given value and throw error with provided description.
     * @param value value to check
     * @param types allowed types for given value
     * @param errDesc error message in case of error
     */
    function checkTypes(value, types, errDesc) {
        if (!isAllowedType(value, types)) {
            input.error(errDesc);
        }
    }


    function isVariableArgs(func) {
        return isAllowedType(func.value, functionsWithVariableArgs)
    }


    /**
     * Helper function to check if token for creating matrix elements is valid. Proper function.
     *
     * @param tok token to check
     */
    function isValidMatrixFunc(tok) {
        return isAllowedType(tok.value, allowedMatrixFunctions);
    }


    /**
     * Helper function to check arguments.
     *
     * @param arg argument token to check
     * @param types allowed types
     * @param argDesc error description
     */
    function checkArg(arg, types, argDesc) {
        if (!arg) {
            input.error(argDesc);
        }

        checkTypes(arg.type, types, argDesc);
    }


    function isStandaloneFunc(value) {
        return isAllowedType(value, standaloneFunctions);
    }


    function isHexColor(hexColor) {
        checkArg(hexColor, ["hexColor"], "Expected hexadecimal color. Start color with '#' character.");
    }


    function isSelectionFunc(tok) {
        return isAllowedType(tok.value, selectionFunctions);
    }


    /**
     * Parses sequence of "addPoint" statement.
     *
     * @param kw keyword to skip
     */
    function parseAddPoint(kw) {
        skipKeyword(kw);

        let maxArgs = 3;
        let args = delimitedSpace(parseExpression, ";", maxArgs);
        if (args.length > 4) {
            input.error("Expected 3 or 4 parameters but got: " + args.length);
        }

        let xCoord = args[0];
        checkArg(xCoord, ["var", "binary", "num"], "Expected x point coordinate. (variable, expression or number)");
        let yCoord = args[1];
        checkArg(yCoord, ["var", "binary", "num"], "After x coordinate y coordinate is expected.");
        let hexColor = args[2];
        isHexColor(hexColor);


        // Optional argument
        let pointType = input.peek();
        if (pointType) {
            if (!isAllowedType(pointType.value, pointTypes)) {
                input.error("Got type: '" + pointType.value + "' but expected one of: " + beautifyArray(pointTypes));
            }

            pointType = input.next();
            pointType.type = "pointType";
            args.push(pointType);
        }

        return {
            type: "newPoint",
            args: args
        }
    }


    /**
     * Parses sequence of optional arguments for select statement. fill color and stroke Color
     * @returns {Array}
     */
    function parseOptionalColors() {
        let optionalArgs = [];
        if (isKeyword(keywords.WITH)) {
            skipKeyword(keywords.WITH);

            // Read another color for stroke style
            let strokeColor = input.next();
            isHexColor(strokeColor);

            optionalArgs.push(strokeColor);
        }

        // Fill style hex color
        if (isKeyword(keywords.AND)) {
            skipKeyword(keywords.AND);

            // Read color for fill style
            let fillColor = input.next();
            isHexColor(fillColor);

            optionalArgs.push(fillColor);
        }

        return optionalArgs;

    }


    /**
     * Parses sequence of selection list for select statement.
     *
     * @param kw keywords to skip
     */
    function parseSelection(kw) {
        expressionCheckPoint = true;
        skipKeyword(kw);

        let errMessage = "Expected one of: " + beautifyArray(selectionFunctions) + " but found: ";


        function selectionParser() {
            // Return value is one token
            // A function with 2 optional parameters
            let tok = input.peek();
            if (!tok) {
                lastSavedMessage = errMessage + "nothing after comma (',').";
                unexpectedMessage();
            }
            if (isSelectionFunc(input.peek())) {
                let token = input.next();
                // It is indeed a function
                let retTok = parseCall(token);
                retTok.optionalArgs = parseOptionalColors();
                return retTok;
            } else if (isKeyword(keywords.LETTER_F)) {
                skipKeyword(keywords.LETTER_F);
                return {
                    type: "letterF",
                    value: null,
                    optionalArgs: parseOptionalColors(),
                };
            } else {
                input.error(errMessage + "'" + input.peek().value + "'.");
            }
        }


        let selectionList = [];
        let stop = ";";
        let first = true;
        while (!input.eof()) {
            if (!input.peek()) {

            }
            if (isPunc(stop)) break;
            if (first) {
                first = false;
            } else {
                skipPunc(",");
            }
            if (isPunc(stop)) break;
            selectionList.push(selectionParser());
        }

        return {
            type: "selectionList",
            shapes: selectionList
        };
    }


    /**
     * Parses sequence of "removePoint" statement.
     *
     * @param kw keyword to skip
     * @returns {*} parsed token
     */
    function parseRemovePoint(kw) {
        skipKeyword(kw);

        let ret = {
            type: "removePoint"
        };


        if (isKeyword(keywords.ALL)) {
            skipKeyword(keywords.ALL);
            ret.removeAll = true;
            ret.value = null;
            return ret;
        }


        let tokenId = input.peek();
        if (!tokenId) {
            input.error("Expected point identification (id) or keyword 'all'.");
        }

        return {
            type: "removePoint",
            value: input.next(),
            removeAll: false
        };
    }


    /**
     * Helper function to parse setter for scale X value.
     *
     * @returns {{type: string, value: *}}
     */
    function parseSetScaleY() {
        skipKeyword(keywords.setScaleStepY);
        expressionCheckPoint = true;
        lastSavedMessage = "After keyword '" + keywords.setScaleStepY + "'" + ", scale step value is expected.";
        let valueTok = maybeBinary(input.next(), 0);
        if (!valueTok || !isAllowedType(valueTok.type, ["num", "binary", "var"])) {
            unexpectedMessage();
        }
        return {
            type: "setScaleYStep",
            value: valueTok
        };
    }


    /**
     * Helper function to parse setter for rotate value.
     *
     * @returns {{type: string, value: *}}
     */
    function parseSetRotate() {
        skipKeyword(keywords.SET_ROTATE_STEP);
        expressionCheckPoint = true;
        lastSavedMessage = "After keyword '" + keywords.SET_ROTATE_STEP + "'" + ", rotate value is expected.";
        let valueTok = maybeBinary(input.next(), 0);
        if (!valueTok || !isAllowedType(valueTok.type, ["num", "binary", "var"])) {
            unexpectedMessage();
        }
        return {
            type: "setRotateStep",
            value: valueTok
        };
    }


    /**
     * Helper function to parse setter for scale Y value.
     *
     * @returns {{type: string, value: *}}
     */
    function parseSetScaleX() {
        skipKeyword(keywords.setScaleStepX);
        expressionCheckPoint = true;
        lastSavedMessage = "After keyword '" + keywords.setScaleStepX + "'" + ", scale step value is expected.";
        let valueTok = maybeBinary(input.next(), 0);
        if (!valueTok || !isAllowedType(valueTok.type, ["num", "binary", "var"])) {
            unexpectedMessage();
        }
        return {
            type: "setScaleXStep",
            value: valueTok
        };
    }


    function parseAtomAndCall() {
        return maybeCall(parseAtom);
    }


    /**
     * Main function to parse different expressions and tokens. Token dispatcher.
     * @returns {*}
     */
    function parseAtom() {
        if (isPunc("(")) {
            input.next();
            let exp = parseExpression();
            skipPunc(")");
            return exp;
        }

        if (isPunc("{")) return parseProgram();

        if (isPunc("[")) {
            input.next();
            let exp = parseMatrix2D();
            skipPunc("]");
            return exp;
        }


        // Keywords check
        if (isKeyword(keywords.PRINT)) {
            expressionCheckPoint = true;
            return parsePrint(keywords.PRINT);
        }

        if (isKeyword(keywords.CLEAR_STACK)) {
            return parseClearStack(keywords.CLEAR_STACK);
        }

        if (isKeyword(keywords.POP)) {
            return parsePop(keywords.POP);
        }

        if (isKeyword(keywords.TRACE)) {
            return parseTrace();
        }

        if (isKeyword(keywords.POINTS)) {
            return parsePoints();
        }

        if (isKeyword(keywords.PRINT_POINTS)) {
            skipKeyword(keywords.PRINT_POINTS);
            return {
                type: "printPoints",
                value: null
            };
        }

        if (isKeyword(keywords.ADD_POINT)) {
            expressionCheckPoint = true;
            return parseAddPoint(keywords.ADD_POINT);
        }

        if (isKeyword(keywords.REMOVE_POINT)) {
            return parseRemovePoint(keywords.REMOVE_POINT);
        }

        if (isKeyword(keywords.DEFAULT_FILL_COLOR)) {
            skipKeyword(keywords.DEFAULT_FILL_COLOR);
            let tok = input.next();
            isHexColor(tok);
            return {
                type: "defaultFillColor",
                value: tok.value
            };
        }


        if (isKeyword(keywords.DEFAULT_OUTLINE_COLOR)) {
            skipKeyword(keywords.DEFAULT_OUTLINE_COLOR);
            let tok = input.next();
            isHexColor(tok);
            return {
                type: "defaultOutlineColor",
                value: tok.value
            };
        }


        if (isKeyword(keywords.PRINT_FILL_COLOR)) {
            skipKeyword(keywords.PRINT_FILL_COLOR);

            return {
                type: "print",
                value: {type: "var", value: "defaultFillColor"}
            };
        }

        if (isKeyword(keywords.PRINT_OUTLINE_COLOR)) {
            skipKeyword(keywords.PRINT_OUTLINE_COLOR);

            return {
                type: "print",
                value: {type: "var", value: "defaultOutlineColor"}
            };
        }


        if (isKeyword(keywords.PRINT_STACK)) {
            skipKeyword(keywords.PRINT_STACK);
            return {
                type: "printStack",
                value: null
            };
        }

        if (isKeyword(keywords.CLEAR_SCREEN)) {
            skipKeyword(keywords.CLEAR_SCREEN);
            return {
                type: "clearScreen",
                value: null
            };
        }

        if (isKeyword(keywords.PUSH)) {
            skipKeyword(keywords.PUSH);
            expressionCheckPoint = true;
            return {
                type: "pushToStack",
                value: parseExpression()
            };
        }

        if (isKeyword(keywords.TRUE) || isKeyword(keywords.FALSE)) {
            if (!expressionCheckPoint) {
                lastSavedMessage = "Expected a statement.";
                unexpectedMessage();
            }
            return parseBool();
        }

        if (isKeyword(keywords.SELECT)) {
            return parseSelection(keywords.SELECT);
        }

        if (isKeyword(keywords.SET_ROTATE_STEP)) {
            return parseSetRotate();
        }

        if (isKeyword(keywords.setScaleStepX)) {
            return parseSetScaleX();
        }

        if (isKeyword(keywords.setScaleStepY)) {
            return parseSetScaleY();

        }


        // Unary operator check
        let maybeUnary = isOper();
        if (maybeUnary) {
            if (maybeUnary.value === ".") {
                throw new Error("Expected matrix but found nothing.");
            }

            // Unary operator, return unary type and just parse next token.
            return parseUnary(maybeUnary);
        }


        // Functions to call
        if (isFunc()) {
            let tok = input.next();
            if (isStandaloneFunc(tok.value)) {
                expressionCheckPoint = true;
            }
            return parseCall(tok);
        }

        // Reading next token
        let tok = input.next();

        if (!tok) {
            unexpectedMessage();
        }


        if (tok.type === "var") {
            if (!isOper() && !expressionCheckPoint) {
                input.error("Expected assign operator for variable: '" + tok.value + "'.");
            }
            return tok;
        }

        // Return any non dividable tokens
        if (tok.type === "num" || tok.type === "str" || tok.type === "hexColor" || tok.type === "pointType") {
            if (!expressionCheckPoint) {
                lastSavedMessage = "Expected a statement.";
                unexpectedMessage();
            }
            return tok;
        }


        // Nothing worked, throw error
        unexpectedMessage();

    }


    /**
     * Function to parse whole program.
     * Currently set to optional syntax sugar ";".
     * Can be used to write more statements on one line using ";" separator.
     *
     * @returns {{type: string, prog: Array}}
     */
    function parseTopLevel() {
        let prog = [];
        while (!input.eof()) {
            prog.push(parseExpression());
            if (!input.eof()) {
                if (isPunc(";")) {
                    skipPunc(";");
                } else {
                    input.error("Expecting punctuation ';' for multiple statements.")
                }
                expressionCheckPoint = false;
                lastSavedMessage = initialMessage;
            }
        }
        return {type: "prog", prog: prog};
    }


    /**
     * Parses block scoped program inside top level program.
     * @returns {*}
     */
    function parseProgram() {
        let prog = delimited("{", "}", ";", parseExpression);
        if (prog.length === 0) return FALSE;
        if (prog.length === 1) return prog[0];
        return {type: "prog", prog: prog};
    }


    /**
     * Parses expression by calling main dispatcher and checks whether it could be binary expression or a call expression.
     *
     */
    function parseExpression() {
        return maybeCall(function () {
            return maybeBinary(parseAtomAndCall(), 0);
        });
    }
}
