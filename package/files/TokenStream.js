/**
 * Main token stream. Given character stream returns functions to deal with tokens.
 *
 * @param input character stream
 * @returns {{next: next, peek: peek, eof: eof, error: (error|*|Error|MediaError)}}
 * @constructor
 */
function TokenStream(input) {
    let current = null;
    let u = new ParserUtil();

    return {
        next: next,
        peek: peek,
        peekChar: peekChar,
        eof: eof,
        error: input.error,
    };


    /**
     *  Reads from input stream while predicate is true
     *
     * @param predicate predicate to call for characters in stream
     * @returns {string}
     */
    function readWhile(predicate) {
        let str = "";
        while (!input.eof() && predicate(input.peek())) {
            str += input.next();
        }
        return str;
    }


    /**
     * Reads a number from stream and returns its token.
     *
     * @returns {{type: string, value: Number}}
     */
    function readNumber() {
        // We got to have float number.
        let hasDot = false;
        let number = readWhile(function (ch) {
            if (ch === ".") {
                if (hasDot) {
                    // To many dots
                    return false;
                }

                hasDot = true;
                return true;
            }
            return u.isDigit(ch);
        });
        return {type: "num", value: parseFloat(number)};
    }


    /**
     * Reads identification from input stream and returns its token.
     * Can be keyword token, function token and simple label (variable) token.
     *
     * @returns {{type: string, value: string}}
     */
    function readIdent() {
        let id = readWhile(u.isId);
        return {
            type: u.isKeyword(id) ? "kw" : u.isFunc(id) ? "func" : "var",
            value: id
        };
    }


    /**
     *  Reads escaped character stream of a string from input.
     *
     * @param end end of string stream
     * @returns {string}
     */
    function readEscaped(end) {
        let escaped = false, str = "";
        let gotEnd = false;
        input.next();
        while (!input.eof()) {
            let ch = input.next();
            if (escaped) {
                str += ch;
                escaped = false;
            } else if (ch === "\\") {
                escaped = true;
            } else if (ch === end) {
                gotEnd = true;
                break;
            } else {
                str += ch;
            }
        }

        if (gotEnd === false) {
            input.error("Expected closing \" at end of string.");
        } else {
            return str;
        }
    }


    /**
     * Reads string and returns its token.
     *
     * @returns {{type: string, value: string}}
     */
    function readString() {
        return {type: "str", value: readEscaped('"')};
    }


    /**
     * Skips comment from input stream.
     */
    function skipComment() {
        readWhile(function (ch) {
            return ch !== "\n"
        });
        input.next();
    }


    /**
     * Reads number with unary character in front.
     *
     * @param ch operator character to apply to operand
     * @returns {{type: string, value: Number}}
     */
    function readUnaryWithNumber(ch) {
        let numToken = readNumber();
        switch (ch) {
            case "-":
                numToken.value *= -1;
        }

        return numToken;
    }


    /**
     * Reads string as hex color.
     */
    function readHexColor() {
        let hexColor = readWhile(u.isHexChar);

        let maxHexValues = 6;
        if (hexColor.length > maxHexValues) {
            input.error("Expected maximum of 6 characters for color.");
        }

        if (hexColor.length === 0) {
            input.error("Expected at least 1 hex character for hexadecimal color.");
        }

        // Check hex values
        let size = hexColor.length;
        for (let c = 0; c < maxHexValues; c++) {
            if (c >= size) {
                hexColor = hexColor + "0";
            }
        }

        // All ok, return hex token
        return {
            type: "hexColor",
            value: "#" + hexColor
        };
    }


    /**
     * Main dispatcher which reads characters and returns different token in respect to given input stream.
     *
     * @returns {*}
     */
    function readNext() {
        readWhile(u.isWhitespace);

        if (input.eof()) {
            // Nothing more to read
            return null;
        }


        let ch = input.peek();
        if (ch === "/") {
            let oper = input.next();
            // could be another "/"
            let another = input.peek();
            if (another === "/") {
                // A comment
                skipComment();
                return readNext();
            } else {
                // Found operational character, return object token from it
                return {
                    type: "op",
                    value: oper
                };
            }
        }

        if (ch === "#") {
            // Should be hex color
            input.next();
            return readHexColor();
        }


        if (ch === '"') {
            // It is start of the string, read the token string and return it
            return readString();
        }

        if (u.isDigit(ch)) {
            // Character is a number, read it and return it as token
            return readNumber();
        }

        if (u.isIdStart(ch)) {
            // Some kind of identification (variable label)
            return readIdent();
        }

        if (u.isPunc(ch)) {
            // Some kind of punctuation
            return {
                type: "punc",
                value: input.next()
            };
        }

        if (u.isOperChar(ch)) {
            let operator = readWhile(u.isOperChar);
            if (operator === "-" || operator === "+") {
                if (u.isDigit(input.peek())) {
                    // Got unary with number
                    return readUnaryWithNumber(operator);
                }
            }

            // Found operational character, return object token from it
            return {
                type: "op",
                value: operator
            };
        }

        // Nothing worked, throw error.
        input.error("Can't handle character: " + ch);
    }


    function peekChar() {
        return input.peek();
    }


    /**
     * Checks the current token without skipping it, or reads next if there is no one.
     *
     * @returns {*|{type, value}}
     */
    function peek() {
        return current || (current = readNext());
    }


    /**
     * Gets next token.
     *
     * @returns {*|{type, value}}
     */
    function next() {
        let tok = current;
        current = null;
        return tok || readNext();
    }


    /**
     * Checks whether there is more tokens in token stream.
     *
     * @returns {boolean}
     */
    function eof() {
        return peek() === null;
    }

}
