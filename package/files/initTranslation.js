/**
 * Initializes webGL to canvas for 2D translation.
 */
function init() {
    let graphics = initCanvas("commonRasterCanvas");
    let letterF = new LetterFShape("LetterF" + createUID(112), 0, 0);
    graphics.addShape(letterF);

    setUIGadgets(graphics, letterF);
    graphics.update();
}


function setUIGadgets(state, obj) {
    let upd = new UpdateFunctions(obj, state);
    let step = state.constants.numeric.gridStep;

    let maxYPixel = (state.constants.numeric.canvasHUnits / 2) / step;
    let maxX = (state.constants.numeric.canvasWUnits / 2) / step;
    let maxY = maxYPixel;

    let minX = -maxX;
    let minY = -maxYPixel;

    let translationXId = "translationXValue";
    let translationYId = "translationYValue";

    let translateXOpts = {
        uiPrecision: 1,
        min: minX,
        max: maxX,
        step: 1,
        // name: "translation x: ", Inherit from html parent
        value: 0,
        functionCallback: upd.updatePosition(0)

    };

    let translateYOpts = {
        uiPrecision: 1,
        min: minY,
        max: maxY,
        step: 1,
        //name: "translation y: ", Inherit from html parent
        value: 0,
        functionCallback: upd.updatePosition(1)
    };

    let sliderX = setupSlider(translationXId, translateXOpts);
    let sliderY = setupSlider(translationYId, translateYOpts);
    state.sliderTx = sliderX.slider;
    state.sliderTy = sliderY.slider;

}


function UpdateFunctions(obj, state) {
    function updatePosition(index) {
        return function (event, ui) {
            let x = state.sliderTx.value;
            let y = state.sliderTy.value;
            obj.setTransform($m3.identity().mul($m3.translation(x, y)));

            // Set UI
            let fixedForUI = obj.transform.toFixed(3);
            let m = $m3.asArrayByRows(fixedForUI);
            setUIMatrix(m);
            state.update();
        }
    }


    return {
        updatePosition: updatePosition
    };
}


init();
