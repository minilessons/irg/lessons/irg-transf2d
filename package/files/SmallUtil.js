/**
 * Parser utilities used by few program segments.
 * Holds keyword list and supported function call names.
 *
 * @constructor
 */
function ParserUtil() {
    let pointTypes = ["circle", "rhombus", "cross", "square"];
    let printTypes = ["var", "str", "num", "matrix", "matrixStack", "binary", "unary", "call", "bool", "hexColor"];
    let standaloneFunctions = ["precision"];
    let keywords = {
        INV: "inv",
        TRACE: "trace",
        POINTS: "points",

        ON: "on",
        OFF: "off",

        POP: "pop",
        CLEAR_STACK: "clearStack",

        // Print
        PRINT: "print",

        CLEAR_SCREEN: "clearScreen",

        TRUE: "true",
        FALSE: "false",

        PRINT_POINTS: "printPoints",

        ADD_POINT: "addPoint",
        REMOVE_POINT: "removePoint",
        PRINT_STACK: "printStack",

        ALL: "all",

        PUSH: "push",

        LETTER_F: "letterF",
        SELECT: "select",
        WITH: "with",
        AND: "and",

        DEFAULT_OUTLINE_COLOR: "defaultOutlineColor",
        DEFAULT_FILL_COLOR: "defaultFillColor",
        PRINT_FILL_COLOR: "printFillColor",
        PRINT_OUTLINE_COLOR: "printOutlineColor",

        SET_ROTATE_STEP: "setRotateStep",
        setScaleStepX: "setScaleStepX",
        setScaleStepY: "setScaleStepY"
    };

    let functionNames = [
        "cos", "sin", "tan",
        "stack", "rotate", "scale", "shear",
        "translate", "precision", "identity",
        "square", "point", "polyline", "polygon",
        "rectangle", "line"
    ];
    addCapitalsSupport();


    /**
     * Adds possibility to call provided functions with all capitals or all small letters.
     */
    function addCapitalsSupport() {
        functionNames.forEach((name) => {
            functionNames.push(name.toUpperCase());

        });
    }


    /**
     * Checks whether number is odd or even.
     *
     * @param num
     * @returns {boolean} true if is
     */
    function isOdd(num) {
        return (num % 2) !== 0;
    }


    /**
     * Checks whether input is really of number type.
     *
     * @param x
     * @returns {boolean} true if is
     */
    function isNum(x) {
        return (typeof x === "number");
    }


    /**
     * Helper function to discard anything not of type number.
     *
     * @param x input to check
     * @returns {boolean} true if is
     */
    function num(x) {
        if (typeof x !== "number") {
            let msg = "Expected number but got: " + x;
            throw new Error(msg);
        }
        return x;
    }


    /**
     * Helper function to check whether there is such function name in supported function names list.
     *
     * @param x function name to search for
     * @returns {boolean} true if is
     */
    function isFunc(x) {
        return functionNames.indexOf(x) > -1;
    }


    /**
     * Helper function to check whether there is provided keyword in supported point types list.
     *
     * @param x point name to search for
     * @returns {boolean} true if is
     */
    function isPointType(x) {
        return pointTypes.indexOf(x) > -1;
    }


    function toUnderscore(str) {
        let underScoreStr = str;
        let listOfSplits = [];

        for (let c = 0, size = str.length; c < size; c++) {
            let ch = str[c];
            if (ch === ch.toUpperCase() && isLetter(ch)) {
                listOfSplits.push(ch);
            }
        }

        listOfSplits.forEach((ch) => {
            let arr = underScoreStr.split(ch);
            underScoreStr = arr[0] + "_" + ch + arr[1];
        });

        return underScoreStr;

    }


    /**
     * Helper function to check whether there is provided keyword in supported keywords list.
     *
     * @param x keyword token to search for
     * @returns {boolean} true if is
     */
    function isKeyword(x) {
        return keywords.hasOwnProperty(x.toUpperCase()) ||
            keywords.hasOwnProperty(toUnderscore(x).toUpperCase()) ||
            keywords.hasOwnProperty(x);
    }


    /**
     * Helper function to check whether provided character is valid for constructing hex colors.
     * @param ch character to check
     * @returns {boolean}
     */
    function isHexChar(ch) {
        return (/[0-9]/i.test(ch) || /[a-f]/i.test(ch) || /[A-F]/i.test(ch));
    }


    /**
     * Helper function to check whether provided character is a digit from 0 to 9.
     *
     * @param ch character to check
     * @returns {boolean} true if is
     */
    function isDigit(ch) {
        return /[0-9]/i.test(ch);
    }


    /**
     * Helper function to check for a character letter.
     *
     * @param ch character to check
     * @returns {boolean}
     */
    function isLetter(ch) {
        return /[a-z]/i.test(ch) || /[A-Z]/i.test(ch);
    }


    /**
     * Helper function to check whether provided character
     * is start of identification with support for letters a-z and underscore.
     *
     * @param ch character to check
     * @returns {boolean} true if is
     */
    function isIdStart(ch) {
        return /[a-z_]/i.test(ch);
    }


    /**
     * Helper function to check whether provided character
     * is start of identification with support for letters a-z and underscore.
     *
     * @param ch
     * @returns {boolean} true if is
     */
    function isId(ch) {
        return isIdStart(ch) || "0123456789".indexOf(ch) >= 0;
    }


    /**
     * Checks whether provided character is any of operators supported.
     *
     * @param ch character to check
     * @returns {boolean} true if is
     */
    function isOperChar(ch) {
        return "+-*/%=&|<>!<=.".indexOf(ch) >= 0;
    }


    /**
     * Checks whether provided character is any of punctuation provided.
     *
     * @param ch character to check
     * @returns {boolean} true if is
     */
    function isPunc(ch) {
        return ",;(){}[]".indexOf(ch) >= 0;
    }


    /**
     * Checks whether character is whitespace, tab or newline.
     *
     * @param ch character to check
     * @returns {boolean} true if is
     */
    function isWhitespace(ch) {
        return " \t\n".indexOf(ch) >= 0;
    }


    /**
     * Returns colored error code in span tag.
     *
     * @returns {string}
     */
    function colorCodeError() {
        return createOuterSpan("errorClass", "Error: ");
    }


    /**
     * Checks whether value is $m3 matrix instance.
     * @param m value to check
     * @returns {boolean} true if is
     */
    function isMatrix(m) {
        return m instanceof $m3;
    }


    // Other predicates
    function or(p1, p2) {
        return function (x) {
            return p1(x) || p2(x);
        }
    }


    function negative(x) {
        return x < 0;
    }


    function positive(x) {
        return x > 0;
    }


    function and(p1, p2) {
        return function (x) {
            return p1(x) && p2(x);
        }
    }


    function not(p) {
        return function (x) {
            return !p(x);
        }
    }


    function less(x) {
        return function (y) {
            return y < x;
        }
    }


    function greater(x) {
        return function (y) {
            return y > x;
        }
    }


    function InRange(min, max) {
        this.min = min;
        this.max = max;
    }


    InRange.prototype.toString = function () {
        return "&lt;" + this.min + ", " + this.max + "&gt;";
    };

    InRange.prototype.checkRange = function (x) {
        let func = and(greater(this.min), less(this.max));
        return func.apply(null, [x]);
    };


    return {
        keywords: keywords,
        pointTypes: pointTypes,
        functionNames: functionNames,
        printTypes: printTypes,
        standaloneFunctions: standaloneFunctions,
        isOdd: isOdd,
        isOperChar: isOperChar,
        isPunc: isPunc,
        isDigit: isDigit,
        isKeyword: isKeyword,
        isId: isId,
        isIdStart: isIdStart,
        isNum: isNum,
        isWhitespace: isWhitespace,
        isFunc: isFunc,
        isHexChar: isHexChar,
        isPointType: isPointType,
        num: num,
        colorCodeError: colorCodeError,
        isMatrix: isMatrix,
        InRange: InRange

    };
}

